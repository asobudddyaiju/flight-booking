# flightbooking

A new Flutter application.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

hive

Key                           Value               box name
///////For Sign In ////////////////////////////////////////////////////
photourl                     string                "box"
email                        string                "box"
displayname                  string                "box"

///////For Bottom sheet ///////////////////////////////////////////////
selectedClass                 int                   "box"
adultCount                    int                   "box"
childrenCount                 int                   "box"
infantCount                   int                   "box"

///////For Round Trip Status //////////////////////////////////////////
roundTripStatus               String                "box"

///////For Place ///////////////////////////////////////////////////////
destinationFromName           String
destinationFrom               String
destinationToName             String
destinationTo                 String

///////For Date ///////////////////////////////////////////////////////
checkOutDate                  String
checkOutMonth                 String
checkOutYear                  String
checkInDate                   String
checkInMonth                  String
checkInYear                   String



