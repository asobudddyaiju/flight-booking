import 'dart:async';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:hive/hive.dart';

import '../src/utils/object_factory.dart';


class Auth {

  Stream<User> get user => FirebaseAuth.instance.authStateChanges();

  Future<String> signInWithGoogle() async {
    try {
      // Trigger the authentication flow
      final GoogleSignInAccount googleUser = await GoogleSignIn().signIn();

      // Obtain the auth details from the request
      final GoogleSignInAuthentication googleAuth =
          await googleUser.authentication;

      // Create a new credential
      final GoogleAuthCredential credential = GoogleAuthProvider.credential(
        accessToken: googleAuth.accessToken,
        idToken: googleAuth.idToken,
      );
      await FirebaseAuth.instance.signInWithCredential(credential);
      if (googleUser != null) {
        ObjectFactory().hiveBox.hivePut(key: 'photourl',value: googleUser.photoUrl);
        ObjectFactory().hiveBox.hivePut(key: 'displayname',value: googleUser.displayName);
        ObjectFactory().hiveBox.hivePut(key: 'email',value: googleUser.email);
      }
      String status = "Success";
      return status.trim();
    } on FirebaseAuthException catch (e) {
      return e.message;
    } catch (e) {
      rethrow;
    }
  }

  Future<String> Signout() async {
    try {
      await FirebaseAuth.instance.signOut();
      await GoogleSignIn().signOut();
      await FirebaseAuth.instance.signOut();
      String status = "Success";
      return status.trim();
    } on FirebaseAuthException catch (e) {
      return e.message;
    } catch (e) {
      rethrow;
    }
  }

}
