import 'dart:async';
import 'package:flightbooking/src/models/destination_from_response.dart';
import 'package:flightbooking/src/models/nearby_airport_request.dart';
import 'package:flightbooking/src/models/nearby_airport_response.dart';
import 'package:flightbooking/src/models/search_request.dart';
import 'package:flightbooking/src/models/notification_list_response.dart';
import 'package:flightbooking/src/models/search_response.dart';
import 'package:flightbooking/src/models/state.dart';
import 'package:flightbooking/src/utils/constants.dart';
import 'package:flightbooking/src/utils/object_factory.dart';
import 'base_bloc.dart';

/// api response of login is managed by AuthBloc
/// stream data is handled by StreamControllers

class AppBloc extends Object implements BaseBloc {
  StreamController<bool> _loading = new StreamController<bool>.broadcast();

  StreamController<SampleResponseModel> _sample =
      new StreamController<SampleResponseModel>.broadcast();

  StreamController<DestinationFromResponse> _destinationFrom =
      new StreamController<DestinationFromResponse>.broadcast();

  StreamController<NearByAirportsResponse> _nearbySearch =
      new StreamController<NearByAirportsResponse>.broadcast();

  StreamController<SearchResponse> _flightSearch =
      new StreamController<SearchResponse>.broadcast();

//stream controller is broadcasting the  details

  Stream<SampleResponseModel> get sampleResponse => _sample.stream;

  Stream<DestinationFromResponse> get destinationResponse =>
      _destinationFrom.stream;

  Stream<NearByAirportsResponse> get nearbySearchResponse =>
      _nearbySearch.stream;

  Stream<SearchResponse> get flightSearch => _flightSearch.stream;

  /// stream for progress bar
  Stream<bool> get loadingListener => _loading.stream;

  StreamSink<bool> get loadingSink => _loading.sink;

  sampleCall() async {
    loadingSink.add(true);

    State state = await ObjectFactory().repository.sampleCall();

    if (state is SuccessState) {
      loadingSink.add(false);
      _sample.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _sample.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  destinationFromCall({String searchKey}) async {
    loadingSink.add(true);
    State state =
        await ObjectFactory().repository.destination(searchKey: searchKey);
    if (state is SuccessState) {
      loadingSink.add(false);
      _destinationFrom.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _destinationFrom.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }
searchNearbyAirports({NearByAirportsRequest nearByAirportsRequest}) async {
    loadingSink.add(true);
    State state =
        await ObjectFactory().repository.searchNearbyAirports(nearByAirportsRequest: nearByAirportsRequest);
    if (state is SuccessState) {
      loadingSink.add(false);
      _nearbySearch.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _nearbySearch.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  searchFlights({SearchRequest searchReq}) async {
    loadingSink.add(true);
    State state =
        await ObjectFactory().repository.searchFlights(searchReq: searchReq);
    if (state is SuccessState) {
      loadingSink.add(false);
      _flightSearch.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _flightSearch.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  ///disposing the stream if it is not using
  @override
  void dispose() {
    _loading?.close();
    _sample?.close();
    _destinationFrom?.close();
    _flightSearch?.close();
    _nearbySearch.close();
  }
}

AppBloc appBlocSingle = AppBloc();
