import 'package:flightbooking/src/screens/home_screen.dart';
import 'package:flightbooking/src/screens/result.dart';
import 'package:flightbooking/src/utils/constants.dart';
import 'package:flightbooking/src/utils/util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class DetailsAppBarWidget extends StatefulWidget {
  String title;
  DetailsAppBarWidget({this.title});
  @override
  _DetailsAppBarWidgetState createState() => _DetailsAppBarWidgetState();
}

class _DetailsAppBarWidgetState extends State<DetailsAppBarWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth(context, dividedBy: 1),
      height: screenHeight(context, dividedBy: 8),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            GestureDetector(
              child: Container(
                  width: screenWidth(context,dividedBy: 8),
                  height: screenHeight(context,dividedBy: 25),
                  color: Colors.transparent,
                  child: Padding(
                    padding: const EdgeInsets.all(2.0),
                    child: SvgPicture.asset('assets/icons/back.svg', color:  Constants.kitGradients[28],),
                  )
              ),
              onTap: (){
                Navigator.pop(context);
              },
            ),
            Text(
              widget.title,
              style: TextStyle(
                fontFamily: 'ProximaNova',
                color: Constants.kitGradients[32],
                fontSize: 20,
                fontWeight: FontWeight.w700,
                fontStyle: FontStyle.normal,
              ),
            ),
            Container(width: screenWidth(context,dividedBy: 9),),
          ],
        ),
      ),
    );
  }
}
