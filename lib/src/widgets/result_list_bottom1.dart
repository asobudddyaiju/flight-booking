import 'package:flightbooking/src/utils/constants.dart';
import 'package:flightbooking/src/utils/util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class ResultListBottom1 extends StatefulWidget {
  String dateTime, stops, route1, tripDuration, airline;
  bool callFrom2;
  ResultListBottom1({this.dateTime, this.route1, this.stops, this.tripDuration,this.callFrom2, this.airline});
  @override
  _ResultListBottom1State createState() => _ResultListBottom1State();
}

class _ResultListBottom1State extends State<ResultListBottom1> {
  @override
  Widget build(BuildContext context) {
    return  Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          //color: Colors.greenAccent,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child:
            widget.airline==null?
            SvgPicture.asset('assets/images/airplane_green.svg')
                :
           Image.network("https://images.kiwi.com/airlines/64/${widget.airline}.png"),
          ),
          width: screenWidth(context, dividedBy: 5),
          height: screenHeight(context, dividedBy: 12),
        ),
        Container(
          width: screenWidth(context, dividedBy: 1.4),
          //height: screenHeight(context, dividedBy: 8),
         // color: Colors.orange,
          child: Column(
            children: [
              SizedBox(
                height: screenHeight(context, dividedBy: 80),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    widget.dateTime,
                    style: TextStyle(
                      fontFamily: 'ProximaNova',
                      color: Constants.kitGradients[31],
                      fontSize: 14,
                      fontWeight: FontWeight.w700,
                      fontStyle: FontStyle.normal,
                    ),
                  ),
                  widget.stops == "0 Stop"?Text(
                    "Direct",
                    style: TextStyle(
                      fontFamily: 'ProximaNova',
                      color: Constants.kitGradients[14],
                      fontSize: 14,
                      fontWeight: FontWeight.w700,
                      fontStyle: FontStyle.normal,
                    ),
                  ):Text(
                    widget.stops,
                    style: TextStyle(
                      fontFamily: 'ProximaNova',
                      color: Constants.kitGradients[31],
                      fontSize: 14,
                      fontWeight: FontWeight.w700,
                      fontStyle: FontStyle.normal,
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 240),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    width: screenWidth(context,dividedBy: 2),
                    child: Text(
                      widget.route1,
                      style: TextStyle(
                        fontFamily: 'ProximaNova',
                        color: Constants.kitGradients[29],
                        fontSize: 12,
                        fontWeight: FontWeight.w400,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                  ),
                  Text(
                   widget.tripDuration,
                    style: TextStyle(
                      fontFamily: 'ProximaNova',
                      color: Constants.kitGradients[29],
                      fontSize: 12,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal,
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 80),
              ),

            ],
          ),
        )
      ],
    );
  }
}
