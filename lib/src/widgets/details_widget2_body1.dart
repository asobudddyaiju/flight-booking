import 'package:flightbooking/src/utils/constants.dart';
import 'package:flightbooking/src/utils/util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class DetailsWidget2Body1 extends StatefulWidget {
  String title1, title2, title3, iconPath, airline;
  DetailsWidget2Body1({this.title3, this.title2, this.title1, this.iconPath,this.airline});
  @override
  _DetailsWidget2Body1State createState() => _DetailsWidget2Body1State();
}
class _DetailsWidget2Body1State extends State<DetailsWidget2Body1> {
  @override
  void initState() {
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return   Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          width: screenWidth(context,dividedBy: 2.0),
          height: screenHeight(context,dividedBy: 8),
         // color: Colors.greenAccent,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                widget.title1,
                overflow: TextOverflow.visible,
                style: TextStyle(
                    color: Constants.kitGradients[32],
                    fontFamily: 'ProximaNova',
                    fontSize: 18,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal),
              ),
              Text(
                widget.title2,
                overflow: TextOverflow.visible,
                style: TextStyle(
                    color: Constants.kitGradients[31],
                    fontFamily: 'ProximaNova',
                    fontSize: 13,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal),
              ),
              Text(
                widget.title3,
                overflow: TextOverflow.visible,
                style: TextStyle(
                    color: Constants.kitGradients[39],
                    fontFamily: 'ProximaNova',
                    fontSize: 15,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal),
              ),
            ],
          ),
        ),
        Container(
          width: screenWidth(context,dividedBy: 4.5),
          height: screenHeight(context,dividedBy: 10),
         // color: Colors.greenAccent,
          child: Padding(
            padding: const EdgeInsets.all(4.0),
            child: widget.airline==null?SvgPicture.asset(widget.iconPath):Image.network("https://images.kiwi.com/airlines/64/${widget.airline}.png"),
          ),
        ),
      ],
    );
  }
}
