import 'package:flightbooking/src/utils/constants.dart';
import 'package:flightbooking/src/utils/util.dart';
import 'package:flutter/material.dart';

class DetailsWidget2Body2A extends StatefulWidget {
  String date, time, place, Airport, airportCode;

  DetailsWidget2Body2A(
      {this.date, this.time, this.place, this.Airport, this.airportCode});

  @override
  _DetailsWidget2Body2AState createState() => _DetailsWidget2Body2AState();
}

class _DetailsWidget2Body2AState extends State<DetailsWidget2Body2A> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth(context, dividedBy: 1.5),
      // color: Colors.red,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                width: screenWidth(context, dividedBy: 4.2),
                //  color: Colors.blue,
                child: Text(
                  widget.time,
                  overflow: TextOverflow.visible,
                  style: TextStyle(
                      color: Constants.kitGradients[28],
                      fontFamily: 'ProximaNova',
                      fontSize: 20,
                      fontWeight: FontWeight.w700,
                      fontStyle: FontStyle.normal),
                ),
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 280),
              ),
              Container(
                width: screenWidth(context, dividedBy: 4.2),
                // color: Colors.blue,
                child: Text(
                  widget.date,
                  overflow: TextOverflow.visible,
                  style: TextStyle(
                      color: Constants.kitGradients[31],
                      fontFamily: 'ProximaNova',
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal),
                ),
              ),
            ],
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                width: screenWidth(context, dividedBy: 2.5),
                // color: Colors.blue,
                child: Text(
                  widget.place,
                  style: TextStyle(
                      color: Constants.kitGradients[28],
                      fontFamily: 'ProximaNova',
                      fontSize: 20,
                      fontWeight: FontWeight.w700,
                      fontStyle: FontStyle.normal),
                ),
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 280),
              ),
              Container(
                width: screenWidth(context, dividedBy: 2.5),
                // color: Colors.blue,
                child: RichText(
                  text: TextSpan(
                      text: widget.Airport,
                      style: TextStyle(
                          color: Constants.kitGradients[31],
                          fontFamily: 'ProximaNova',
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal),
                      children: <TextSpan>[
                        TextSpan(
                          text: " (",
                          style: TextStyle(
                              color: Constants.kitGradients[31],
                              fontFamily: 'ProximaNova',
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.normal),

                        ),
                        TextSpan(
                          text: widget.airportCode,
                          style: TextStyle(
                              color: Constants.kitGradients[14],
                              fontFamily: 'ProximaNova',
                              fontSize: 14,
                              fontWeight: FontWeight.w700,
                              fontStyle: FontStyle.normal),
                        ),
                        TextSpan(
                          text: ")",
                          style: TextStyle(
                              color: Constants.kitGradients[31],
                              fontFamily: 'ProximaNova',
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.normal),

                        )
                      ]),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
