import 'package:flightbooking/localization/locale.dart';
import 'package:flightbooking/src/utils/constants.dart';
import 'package:flightbooking/src/utils/util.dart';
import 'package:flightbooking/src/widgets/result_appbar_widget.dart';
import 'package:flightbooking/src/widgets/result_shimmer_body.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shimmer/shimmer.dart';

class ResultShrimmer extends StatefulWidget {
  bool roundTripStatus;
  String route, dateTimePassengers;

  ResultShrimmer({this.roundTripStatus, this.dateTimePassengers, this.route});

  @override
  _ResultShrimmerState createState() => _ResultShrimmerState();
}

class _ResultShrimmerState extends State<ResultShrimmer> {
  ScrollController _controller = ScrollController();
  bool _enabled = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Constants.kitGradients[27],
      // appBar: AppBar(
      //   backgroundColor: Constants.kitGradients[3],
      //   leading: Container(),
      //   actions: [
      //     ResultAppBarWidget(
      //       route: widget.route,
      //       dateTimePassengers: widget.dateTimePassengers,
      //     ),
      //   ],
      // ),
      body: Column(
        children: [
          // Container(
          //   width: screenWidth(context, dividedBy: 1),
          //   height: 0.3,
          //   color: Constants.kitGradients[4],
          // ),
          // Container(
          //   height: screenHeight(context, dividedBy: 17),
          //   color: Constants.kitGradients[3],
          //   child: Shimmer.fromColors(
          //     baseColor: Colors.grey[300],
          //     highlightColor: Colors.grey[100],
          //     enabled: _enabled,
          //     child: ListView.builder(
          //         controller: _controller,
          //         scrollDirection: Axis.horizontal,
          //         shrinkWrap: true,
          //         itemCount: 5,
          //         itemBuilder: (BuildContext context, index) {
          //           return Padding(
          //             padding: const EdgeInsets.all(8.0),
          //             child: Container(
          //               width: screenWidth(context, dividedBy: 4),
          //               decoration: BoxDecoration(
          //                 color: Constants.kitGradients[33],
          //                 borderRadius: BorderRadius.circular(20.0),
          //               ),
          //             ),
          //           );
          //         }),
          //   ),
          // ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  getTranslated(context, "Searching"),
                  style: TextStyle(
                    fontFamily: 'ProximaNova',
                    color: Constants.kitGradients[34],
                    fontSize: 12,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: ListView.builder(
                controller: _controller,
                shrinkWrap: true,
                scrollDirection: Axis.vertical,
                itemCount: 10,
                itemBuilder: (BuildContext context, index) {
                  return Padding(
                    padding: const EdgeInsets.all(3.0),
                    child: Container(
                      width: screenWidth(context, dividedBy: 1),
                      decoration: BoxDecoration(
                          color: Constants.kitGradients[3],
                          borderRadius: BorderRadius.circular(15.0)),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            ResultShimmerBody(),
                            SizedBox(
                              height: screenHeight(context, dividedBy: 30),
                            ),
                            widget.roundTripStatus == true
                                ? ResultShimmerBody()
                                : Container(),
                          ],
                        ),
                      ),
                    ),
                  );
                }),
          ),
        ],
      ),
    );
  }
}
