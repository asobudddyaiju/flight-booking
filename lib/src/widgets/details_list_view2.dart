import 'package:flightbooking/src/utils/flight_names.dart';
import 'package:flightbooking/src/utils/util.dart';
import 'package:flutter/material.dart';

import 'details_widget3.dart';
import 'details_widget_2.dart';

class DetailsListView2 extends StatefulWidget {
  bool roundTripStatus, showWidget;
  final String title1, title2, title3, toTime,toAirport,toAirportCode, toDate,toPlace, fromTime, fromAirport,fromAirportCode, fromDate,fromPlace, layOverDuration,layOverPlace, airline;
  DetailsListView2({this.roundTripStatus,this.toAirport,this.toPlace,this.toTime,this.toDate,this.fromAirport,this.fromDate,this.fromPlace,this.fromTime,this.title2,this.title1,this.title3,this.layOverDuration,this.layOverPlace, this.airline, this.toAirportCode, this.fromAirportCode});
  @override
  _DetailsListView2State createState() => _DetailsListView2State();
}

class _DetailsListView2State extends State<DetailsListView2> {
  @override
  void initState() {
    if(widget.layOverDuration.trim()== "dontShowWidget") {
      setState(() {
        widget.showWidget = false;
      });
    }else{
      setState(() {
        widget.showWidget = true;
      });
    }
    print( widget.showWidget.toString());
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        widget.roundTripStatus == true
            ? DetailsWidget2(
          title1: widget.title1,
          title2: widget.title2,
          title3: widget.title3,
          toTime: widget.toTime,
          fromAirport:widget.fromAirport,
          fromAirportCode: widget.fromAirportCode,
          toAirport: widget.toAirport,
          toAirportCode: widget.toAirportCode,
          toDate: widget.toDate,
          toPlace: widget.toPlace,
          fromTime: widget.fromTime,
          fromDate: widget.fromDate,
          fromPlace: widget.fromPlace,
          airline: widget.airline,
        )
            : Container(),
        SizedBox(
          height: screenHeight(context, dividedBy: 105),
        ),
        widget.showWidget == true?DetailsWidget3(
          layOverPlace: 'Layover in '+widget.layOverPlace,
          layOverDuration: widget.layOverDuration,
        ):Container(),
        SizedBox(
          height: screenHeight(context, dividedBy: 105),
        ),
      ],
    );
  }
}
