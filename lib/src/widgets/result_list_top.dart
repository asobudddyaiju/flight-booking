import 'dart:ui';

import 'package:flightbooking/localization/locale.dart';
import 'package:flightbooking/src/utils/constants.dart';
import 'package:flutter/material.dart';

class ResultListTop extends StatefulWidget {
  String title;
  Function onTap;
  Color textColor, containerColor;
  int itemIndex;
  ResultListTop({this.title, this.onTap, this.containerColor, this.textColor, this.itemIndex});
  @override
  _ResultListTopState createState() => _ResultListTopState();
}

class _ResultListTopState extends State<ResultListTop> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: GestureDetector(
        child: Container(
          decoration: BoxDecoration(
            color: widget.containerColor,
            borderRadius: BorderRadius.circular(20.0),
          ),
          child: Center(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 8),
              child: Text(
               widget.title,
                style: TextStyle(
                  fontFamily: 'ProximaNova',
                  color: widget.textColor,
                  fontSize: 14,
                  fontWeight: FontWeight.w700,
                  fontStyle: FontStyle.normal,
                ),
              ),
            ),
          ),
        ),
        onTap: widget.onTap,
      ),
    );
  }
}
