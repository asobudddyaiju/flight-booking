import 'package:flightbooking/src/screens/result.dart';
import 'package:flightbooking/src/utils/constants.dart';
import 'package:flightbooking/src/utils/util.dart';
import 'package:flutter/material.dart';

class HomeScreenSubmitButton extends StatefulWidget {
  String buttonName;
  Function onTap;
  HomeScreenSubmitButton({this.buttonName, this.onTap});
  @override
  _HomeScreenSubmitButtonState createState() => _HomeScreenSubmitButtonState();
}

class _HomeScreenSubmitButtonState extends State<HomeScreenSubmitButton> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
        width: screenWidth(context, dividedBy: 1.2),
        height: screenHeight(context, dividedBy: 13.0),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(35),
            color: Constants.kitGradients[14]),
        child: Center(
          child: Text(
            widget.buttonName,
            style: TextStyle(
                fontFamily: 'ProximaNova',
                fontSize: 24,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
                color: Constants.kitGradients[3]),
          ),
        ),
      ),
      onTap: widget.onTap
    );
  }
}
