import 'package:flightbooking/src/utils/constants.dart';
import 'package:flightbooking/src/utils/util.dart';
import 'package:flightbooking/src/widgets/details_widget2_body1.dart';
import 'package:flightbooking/src/widgets/details_widget2_body2.dart';
import 'package:flutter/material.dart';


class DetailsWidget2 extends StatefulWidget {
  String title1, title2, title3, airline;
  String fromTime, toTime, fromDate, toDate, fromAirport, fromAirportCode, fromPlace,toPlace, toAirport, toAirportCode;

  DetailsWidget2({this.title2, this.title1, this.title3,this.fromAirport, this.toAirport, this.toTime,this.toDate, this.fromDate, this.fromTime, this.fromPlace, this.toPlace, this.airline, this.fromAirportCode, this.toAirportCode});

  @override
  _DetailsWidget2State createState() => _DetailsWidget2State();
}

class _DetailsWidget2State extends State<DetailsWidget2> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth(context, dividedBy: 1),
      //height: screenHeight(context,dividedBy: 3),
      decoration: BoxDecoration(
          color: Constants.kitGradients[38],
          borderRadius: BorderRadius.circular(15.0)),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            DetailsWidget2Body1(
              title1: widget.title1,
              title2: widget.title2,
              title3: widget.title3,
              iconPath: 'assets/images/airplane_green.svg',
              airline: widget.airline,
            ),
            DetailsWidget2Body2(fromTime: widget.fromTime,fromPlace: widget.fromPlace,fromDate: widget.fromDate,fromAirport: widget.fromAirport,toDate: widget.toDate,toTime: widget.toTime,toPlace: widget.toPlace,toAirport: widget.toAirport,fromAirportCode: widget.fromAirportCode,toAirportCode: widget.toAirportCode,),
          ],
        ),
      ),
    );
  }
}
