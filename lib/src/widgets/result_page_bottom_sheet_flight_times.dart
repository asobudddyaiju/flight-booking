import 'package:flightbooking/src/utils/constants.dart';
import 'package:flightbooking/src/utils/object_factory.dart';
import 'package:flightbooking/src/utils/util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_range_slider/flutter_range_slider.dart' as frs;

class ResultPageBottomSheetFlightTime extends StatefulWidget {
  // List<int> durationDepartureArray = [];
  // List<int> durationReturnArray = [];
  final ValueChanged showSaveButtonValueChanged;
  final ValueChanged departureFromTime;
  final ValueChanged departureToTime;
  final ValueChanged returnFromTime;
  final ValueChanged returnToTime;
  final String destinationFromName;
  final String destinationToName;
  // final ValueChanged stops;
  // ValueChanged returnRangeValue;
  bool roundTripStatus;

  ResultPageBottomSheetFlightTime(
      {
      // {this.durationDepartureArray,
      //   this.durationReturnArray,
      this.showSaveButtonValueChanged,
      this.departureFromTime,
      this.departureToTime,
      this.returnFromTime,
      this.returnToTime,
      this.destinationFromName,
      this.destinationToName,
      // this.returnRangeValue,
      this.roundTripStatus});

  @override
  _ResultPageBottomSheetFlightTimeState createState() =>
      _ResultPageBottomSheetFlightTimeState();
}

class _ResultPageBottomSheetFlightTimeState
    extends State<ResultPageBottomSheetFlightTime> {
  // double _lowerValue;
  double departureFromTime = 0;
  double departureToTime = 24;
  double returnFromTime = 0;
  double returnToTime = 24;

  // double _upperValue;

  // double departRangeStartsAt;
  // double returnRangeEndsAt;
  // bool checkDepartureRangeIsLowerThanReturn;
  // double variableForObjectFactoryDeparture;
  // double variableForObjectFactoryReturn;
  // double x;
  // double y;

  @override
  void initState() {
    // if (widget.roundTripStatus == true) {
    // setState(() {
    // checkDepartureRangeIsLowerThanReturn = (ObjectFactory()
    //     .hiveBox
    //     .getDepartureArrayMax()
    //     .roundToDouble()) <=
    //     (ObjectFactory().hiveBox.getReturnArrayMax().roundToDouble())
    //     ? true
    //     : false;

    // departRangeStartsAt = (ObjectFactory()
    //     .hiveBox
    //     .getDepartureArrayMin()
    //     .roundToDouble()) <=
    //     (ObjectFactory().hiveBox.getReturnArrayMax().roundToDouble())
    //     ? (ObjectFactory().hiveBox.getDepartureArrayMin().roundToDouble())
    //     : (ObjectFactory().hiveBox.getReturnArrayMax().roundToDouble());

    //   returnRangeEndsAt = (ObjectFactory()
    //       .hiveBox
    //       .getReturnArrayMax()
    //       .roundToDouble()) >=
    //       (ObjectFactory().hiveBox.getDepartureArrayMin().roundToDouble())
    //       ? (ObjectFactory().hiveBox.getReturnArrayMax().roundToDouble())
    //       : (ObjectFactory().hiveBox.getDepartureArrayMin().roundToDouble());
    //
    //   x = (departRangeStartsAt) / 100000;
    //   y = (returnRangeEndsAt) / 100000;
    // });

    // if (ObjectFactory().hiveBox.getDepartureRangeStart() != null) {
    //   if (ObjectFactory().hiveBox.getDepartureRangeLowerThanReturnRange() ==
    //       true) {
    //     setState(() {
    //       variableForObjectFactoryDeparture =
    //           ObjectFactory().hiveBox.getDepartureRangeStart();
    //       variableForObjectFactoryReturn =
    //           ObjectFactory().hiveBox.getReturnRangeEnd();
    //       _lowerValue = variableForObjectFactoryDeparture;
    //       _upperValue = variableForObjectFactoryReturn;
    //     });
    //   } else {
    //     setState(() {
    //       variableForObjectFactoryDeparture =
    //           ObjectFactory().hiveBox.getDepartureRangeStart();
    //       variableForObjectFactoryReturn =
    //           ObjectFactory().hiveBox.getReturnRangeEnd();
    //       _upperValue = variableForObjectFactoryDeparture;
    //       _lowerValue = variableForObjectFactoryReturn;
    //     });
    //   }

    // print("data is from object factory "+_lowerValue.toString());
    // print("data is from object factory "+_upperValue.toString());
    //   } else {
    //     setState(() {
    //       _lowerValue = (departRangeStartsAt);
    //       _upperValue = (returnRangeEndsAt);
    //     });
    //
    //     // print("data is not from object factory "+_lowerValue.toString());
    //     // print("data is not from object factory "+_upperValue.toString());
    //   }
    // } else {
    //   setState(() {
    //     checkDepartureRangeIsLowerThanReturn = true;
    //     departRangeStartsAt =
    //         ObjectFactory().hiveBox.getDepartureArrayMin().roundToDouble();
    //     returnRangeEndsAt =
    //         ObjectFactory().hiveBox.getDepartureArrayMax().roundToDouble();
    //     x = (departRangeStartsAt) / 100000;
    //     y = (returnRangeEndsAt) / 100000;
    //   });
    //   if (ObjectFactory().hiveBox.getDepartureRangeStart() != null) {
    //     setState(() {
    //       variableForObjectFactoryDeparture =
    //           ObjectFactory().hiveBox.getDepartureRangeStart();
    //       variableForObjectFactoryReturn =
    //           ObjectFactory().hiveBox.getReturnRangeEnd();
    //       _lowerValue = variableForObjectFactoryDeparture;
    //       _upperValue = variableForObjectFactoryReturn;
    //     });
    //     // print("data is from object factory "+_lowerValue.toString());
    //     // print("data is from object factory "+_upperValue.toString());
    //   } else {
    //     setState(() {
    //       _lowerValue = (departRangeStartsAt + x);
    //       _upperValue = (returnRangeEndsAt - y);
    //     });

    // print("data is not from object factory "+_lowerValue.toString());
    // print("data is not from object factory "+_upperValue.toString());
    //   }
    // }
    departureFromTime = ObjectFactory().hiveBox.getDepartureFromTime() != null
        ? ObjectFactory().hiveBox.getDepartureFromTime() != ""
            ? int.parse(ObjectFactory()
                    .hiveBox
                    .getDepartureFromTime()
                    .toString()
                    .split(":")
                    .first)
                .toDouble()
            : 0
        : 0;
    departureToTime = ObjectFactory().hiveBox.getDepartureToTime() != null
        ? ObjectFactory().hiveBox.getDepartureToTime() != ""
            ? int.parse(ObjectFactory()
                    .hiveBox
                    .getDepartureToTime()
                    .toString()
                    .split(":")
                    .first)
                .toDouble()
            : 24
        : 24;
    returnToTime = ObjectFactory().hiveBox.getReturnToTime() != null
        ? ObjectFactory().hiveBox.getReturnToTime() != ""
            ? int.parse(ObjectFactory()
                    .hiveBox
                    .getReturnToTime()
                    .toString()
                    .split(":")
                    .first)
                .toDouble()
            : 24
        : 24;
    returnFromTime = ObjectFactory().hiveBox.getReturnFromTime() != null
        ? ObjectFactory().hiveBox.getReturnFromTime() != ""
            ? int.parse(ObjectFactory()
                    .hiveBox
                    .getReturnFromTime()
                    .toString()
                    .split(":")
                    .first)
                .toDouble()
            : 0
        : 0;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          width: screenWidth(context, dividedBy: 1.1),
          child: Text(
            'Departure flight from ' + widget.destinationFromName,
            overflow: TextOverflow.visible,
            style: TextStyle(
                fontSize: 16.0,
                fontFamily: 'AirbnbCerealAppRegular',
                fontWeight: FontWeight.w700,
                color: Constants.kitGradients[13]),
          ),
        ),
        SizedBox(
          height: screenHeight(context, dividedBy: 30.0),
        ),
        Container(
          width: screenWidth(context, dividedBy: 1),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "${departureFromTime.toStringAsFixed(0).length == 1 ? "0" + departureFromTime.toStringAsFixed(0) : departureFromTime.toStringAsFixed(0) == "24" ? "00" : departureFromTime.toStringAsFixed(0)}:00",
                  style: TextStyle(
                      fontSize: 16.0,
                      fontFamily: 'AirbnbCerealAppRegular',
                      fontWeight: FontWeight.w700,
                      color: Constants.kitGradients[13]),
                ),
                Text(
                  "${departureToTime.toStringAsFixed(0).length == 1 ? "0" + departureToTime.toStringAsFixed(0) : departureToTime.toStringAsFixed(0) == "24" ? "00" : departureToTime.toStringAsFixed(0)}:00",
                  style: TextStyle(
                      fontSize: 16.0,
                      fontFamily: 'AirbnbCerealAppRegular',
                      fontWeight: FontWeight.w700,
                      color: Constants.kitGradients[13]),
                ),
              ],
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0),
          child: SliderTheme(
            data: SliderTheme.of(context).copyWith(
              overlayColor: Constants.kitGradients[0],
              activeTickMarkColor: Constants.kitGradients[0],
              activeTrackColor: Constants.kitGradients[0],
              inactiveTrackColor: Colors.grey,
              //trackHeight: 8.0,
              thumbColor: Constants.kitGradients[0],
              valueIndicatorColor: Constants.kitGradients[0],
              showValueIndicator: false
                  ? ShowValueIndicator.always
                  : ShowValueIndicator.onlyForDiscrete,
            ),
            child: frs.RangeSlider(
              min: 0,
              max: 24,
              lowerValue: departureFromTime,
              upperValue: departureToTime,
              showValueIndicator: true,
              onChanged: (double newLowerValue, double newUpperValue) {
                setState(() {
                  departureFromTime = newLowerValue;
                  departureToTime = newUpperValue;
                });
              },
              onChangeStart: (double startLowerValue, double startUpperValue) {
                widget.showSaveButtonValueChanged(true);
              },
              onChangeEnd: (double newLowerValue, double newUpperValue) {
                widget.departureToTime(newUpperValue);
                widget.departureFromTime(newLowerValue);
                // widget.showSaveButtonValueChanged(true);
                // if (widget.roundTripStatus == true) {
                //   if (checkDepartureRangeIsLowerThanReturn == true) {
                //     widget.departRangeValue(newLowerValue);
                //     widget.returnRangeValue(newUpperValue);
                //     ObjectFactory()
                //         .hiveBox
                //         .putDepartureRangeLowerThanReturnRange(value: true);
                //   } else {
                //     widget.departRangeValue(newUpperValue);
                //     widget.returnRangeValue(newLowerValue);
                //     ObjectFactory()
                //         .hiveBox
                //         .putDepartureRangeLowerThanReturnRange(value: false);
                //   }
                // } else {
                //   widget.departRangeValue(newLowerValue);
                //   widget.returnRangeValue(newUpperValue);
                //   ObjectFactory()
                //       .hiveBox
                //       .putDepartureRangeLowerThanReturnRange(value: true);
                // }
              },
            ),
          ),
        ),
        SizedBox(
          height: screenHeight(context, dividedBy: 10.0),
        ),
        if (widget.roundTripStatus == true)
          Container(
            width: screenWidth(context, dividedBy: 1.1),
            child: Text(
              'Return flight from ' + widget.destinationToName,
              overflow: TextOverflow.visible,
              style: TextStyle(
                  fontSize: 16.0,
                  fontFamily: 'AirbnbCerealAppRegular',
                  fontWeight: FontWeight.w700,
                  color: Constants.kitGradients[13]),
            ),
          ),
        if (widget.roundTripStatus == true)
          SizedBox(
            height: screenHeight(context, dividedBy: 30.0),
          ),
        if (widget.roundTripStatus == true)
          Container(
            width: screenWidth(context, dividedBy: 1),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "${returnFromTime.toStringAsFixed(0).length == 1 ? "0" + returnFromTime.toStringAsFixed(0) : returnFromTime.toStringAsFixed(0) == "24" ? "00" : returnFromTime.toStringAsFixed(0)}:00",
                    style: TextStyle(
                        fontSize: 16.0,
                        fontFamily: 'AirbnbCerealAppRegular',
                        fontWeight: FontWeight.w700,
                        color: Constants.kitGradients[13]),
                  ),
                  Text(
                    "${returnToTime.toStringAsFixed(0).length == 1 ? "0" + returnToTime.toStringAsFixed(0) : returnToTime.toStringAsFixed(0) == "24" ? "00" : returnToTime.toStringAsFixed(0)}:00",
                    style: TextStyle(
                        fontSize: 16.0,
                        fontFamily: 'AirbnbCerealAppRegular',
                        fontWeight: FontWeight.w700,
                        color: Constants.kitGradients[13]),
                  ),
                ],
              ),
            ),
          ),
        if (widget.roundTripStatus == true)
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            child: SliderTheme(
              data: SliderTheme.of(context).copyWith(
                overlayColor: Constants.kitGradients[0],
                activeTickMarkColor: Constants.kitGradients[0],
                activeTrackColor: Constants.kitGradients[0],
                inactiveTrackColor: Colors.grey,
                //trackHeight: 8.0,
                thumbColor: Constants.kitGradients[0],
                valueIndicatorColor: Constants.kitGradients[0],
                showValueIndicator: false
                    ? ShowValueIndicator.always
                    : ShowValueIndicator.onlyForDiscrete,
              ),
              child: frs.RangeSlider(
                min: 0,
                max: 24,
                lowerValue: returnFromTime,
                upperValue: returnToTime,
                showValueIndicator: true,
                onChanged: (double newLowerValue, double newUpperValue) {
                  setState(() {
                    returnToTime = newUpperValue;
                    returnFromTime = newLowerValue;
                  });
                },
                onChangeStart:
                    (double startLowerValue, double startUpperValue) {
                  widget.showSaveButtonValueChanged(true);
                },
                onChangeEnd: (double newLowerValue, double newUpperValue) {
                  widget.returnToTime(newUpperValue);
                  widget.returnFromTime(newLowerValue);
                  // widget.showSaveButtonValueChanged(true);
                  // if (widget.roundTripStatus == true) {
                  //   if (checkDepartureRangeIsLowerThanReturn == true) {
                  //     widget.departRangeValue(newLowerValue);
                  //     widget.returnRangeValue(newUpperValue);
                  //     ObjectFactory()
                  //         .hiveBox
                  //         .putDepartureRangeLowerThanReturnRange(value: true);
                  //   } else {
                  //     widget.departRangeValue(newUpperValue);
                  //     widget.returnRangeValue(newLowerValue);
                  //     ObjectFactory()
                  //         .hiveBox
                  //         .putDepartureRangeLowerThanReturnRange(value: false);
                  //   }
                  // } else {
                  //   widget.departRangeValue(newLowerValue);
                  //   widget.returnRangeValue(newUpperValue);
                  //   ObjectFactory()
                  //       .hiveBox
                  //       .putDepartureRangeLowerThanReturnRange(value: true);
                  // }
                },
              ),
            ),
          ),
        if (widget.roundTripStatus == true)
          SizedBox(
            height: screenHeight(context, dividedBy: 10.0),
          ),
      ],
    );
  }
}
