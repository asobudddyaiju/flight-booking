import 'package:flightbooking/src/utils/constants.dart';
import 'package:flightbooking/src/utils/object_factory.dart';
import 'package:flightbooking/src/utils/util.dart';
import 'package:flutter/material.dart';

class ResultPageBottomSheetStopsFilter extends StatefulWidget {
  ValueChanged showSaveButtonValueChanged;
  ValueChanged stops;

  ResultPageBottomSheetStopsFilter({
    this.showSaveButtonValueChanged,
    this.stops,
  });

  @override
  _ResultPageBottomSheetStopsFilterState createState() =>
      _ResultPageBottomSheetStopsFilterState();
}

class _ResultPageBottomSheetStopsFilterState
    extends State<ResultPageBottomSheetStopsFilter> {
  // double _lowerValue;
  double stops = 5;
  double dummyStop = 0;

  // double _upperValue;

  // double departRangeStartsAt;
  // double returnRangeEndsAt;
  // bool checkDepartureRangeIsLowerThanReturn;
  // double variableForObjectFactoryDeparture;
  // double variableForObjectFactoryReturn;
  // double x;
  // double y;

  @override
  void initState() {
    if (ObjectFactory().hiveBox.getStopsNumbers() != null)
      stops = int.parse(ObjectFactory().hiveBox.getStopsNumbers()).toDouble();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          width: screenWidth(context, dividedBy: 1),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "0",
                  style: TextStyle(
                      fontSize: 16.0,
                      fontFamily: 'AirbnbCerealAppRegular',
                      fontWeight: FontWeight.w700,
                      color: Constants.kitGradients[13]),
                ),
                Text(
                  stops.toStringAsFixed(0),
                  style: TextStyle(
                      fontSize: 16.0,
                      fontFamily: 'AirbnbCerealAppRegular',
                      fontWeight: FontWeight.w700,
                      color: Constants.kitGradients[13]),
                ),
              ],
            ),
          ),
        ),
        Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            child: SliderTheme(
              data: SliderTheme.of(context).copyWith(
                overlayColor: Constants.kitGradients[0],
                activeTickMarkColor: Constants.kitGradients[0],
                activeTrackColor: Constants.kitGradients[0],
                valueIndicatorTextStyle: TextStyle(color: Colors.red),

                inactiveTrackColor: Colors.grey,
                //trackHeight: 8.0,
                thumbColor: Constants.kitGradients[0],
                valueIndicatorColor: Constants.kitGradients[4],
                showValueIndicator: false
                    ? ShowValueIndicator.always
                    : ShowValueIndicator.onlyForDiscrete,
              ),
              child: Slider(
                min: 0,
                max: 5,
                value: stops,
                onChanged: (value) {
                  widget.showSaveButtonValueChanged(true);
                  setState(() {
                    stops = value;
                  });
                  widget.stops(value);
                },
              ),
            )),
        SizedBox(
          height: screenHeight(context, dividedBy: 10.0),
        ),
      ],
    );
  }
}
