import 'package:flightbooking/localization/locale.dart';
import 'package:flightbooking/src/screens/home_screen.dart';
import 'package:flightbooking/src/utils/constants.dart';
import 'package:flightbooking/src/utils/object_factory.dart';
import 'package:flightbooking/src/utils/util.dart';
import 'package:flightbooking/src/widgets/home_bottom_sheet_widget.dart';
import 'package:flutter/material.dart';
import 'home_bottom_sheet_widget2.dart';
import 'home_screen_submit_button.dart';
class HomeBottomSheet extends StatefulWidget {
  @override
  _HomeBottomSheetState createState() => _HomeBottomSheetState();
}

class _HomeBottomSheetState extends State<HomeBottomSheet> {
  bool economy;
  bool premiumEconomy;
  bool business;
  bool firstClass;
  int adultCount = 0;
  int childrenCount = 0;
  int infantsCount = 0;

  @override
  void initState() {


    ObjectFactory().hiveBox.hiveGetInt(key:"adultCount")==null?setState((){
      adultCount = 1;
    }):setState((){
      adultCount = ObjectFactory().hiveBox.hiveGetInt(key:"adultCount");
    });
    ObjectFactory().hiveBox.hiveGetInt(key:"childrenCount")==null?setState((){
      childrenCount = 0;
    }):setState((){
      childrenCount = ObjectFactory().hiveBox.hiveGetInt(key:"childrenCount");
    });
    ObjectFactory().hiveBox.hiveGetInt(key:"infantCount")==null?setState((){
      infantsCount = 0;
    }):setState((){
      infantsCount = ObjectFactory().hiveBox.hiveGetInt(key:"infantCount");
    });
    ObjectFactory().hiveBox.hiveGetBool(key:"economy")==null?setState((){
      economy = true;
    }):setState((){
      economy = ObjectFactory().hiveBox.hiveGetBool(key:"economy");
    });
    ObjectFactory().hiveBox.hiveGetBool(key:"premiumEconomy")==null?setState((){
      premiumEconomy = false;
    }):setState((){
      premiumEconomy = ObjectFactory().hiveBox.hiveGetBool(key:"premiumEconomy");
    });
    ObjectFactory().hiveBox.hiveGetBool(key:"business")==null?setState((){
      business = false;
    }):setState((){
      business = ObjectFactory().hiveBox.hiveGetBool(key:"business");
    });
    ObjectFactory().hiveBox.hiveGetBool(key:"firstClass")==null?setState((){
      firstClass = false;
    }):setState((){
      firstClass = ObjectFactory().hiveBox.hiveGetBool(key:"firstClass");
    });

    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth(context, dividedBy: 1),
      height: screenHeight(context, dividedBy: 1.2),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(25.0), topRight: Radius.circular(25.0)),
        color: Constants.kitGradients[3],
      ),
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 15.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              getTranslated(context, 'Passengers'),
              style: TextStyle(
                  fontFamily: 'ProximaNova',
                  fontStyle: FontStyle.normal,
                  fontWeight: FontWeight.w700,
                  fontSize: 16,
                  color: Constants.kitGradients[36]),
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 86),
            ),
            HomeBottomSheetWidget(
              title: getTranslated(context, "Adults"),
              subTitle: getTranslated(context, "Over_11"),
              count: adultCount,
              adultCount: adultCount,
              childrenCount: childrenCount,
              infantCount: infantsCount,
              countValue: (value){
                setState(() {
                  adultCount = value;
                });
              },
            ),
            Container(
              width: screenWidth(context, dividedBy: 1),
              height: 0.5,
              color: Constants.kitGradients[37],
            ),
            HomeBottomSheetWidget(
              title: getTranslated(context, "Children"),
              subTitle: getTranslated(context, "2-11"),
              count: childrenCount,
              adultCount: adultCount,
              childrenCount: childrenCount,
              infantCount: infantsCount,
              countValue: (value){
                setState(() {
                  childrenCount = value;
                });
              },
            ),
            Container(
              width: screenWidth(context, dividedBy: 1),
              height: 0.5,
              color: Constants.kitGradients[37],
            ),
            HomeBottomSheetWidget(
              title: getTranslated(context, "Infants"),
              subTitle: getTranslated(context, "Under_2"),
              count: infantsCount,
              adultCount: adultCount,
              childrenCount: childrenCount,
              infantCount: infantsCount,
              countValue: (value){
                setState(() {
                  infantsCount = value;
                });
              },
            ),
            Container(
              width: screenWidth(context, dividedBy: 1),
              height: 0.5,
              color: Constants.kitGradients[37],
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 26),
            ),
            Text(
              getTranslated(context, 'Classes'),
              style: TextStyle(
                  fontFamily: 'ProximaNova',
                  fontStyle: FontStyle.normal,
                  fontWeight: FontWeight.w700,
                  fontSize: 16,
                  color: Constants.kitGradients[36]),
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 100),
            ),
            HomeScreenBottomWidget2(
              classes: getTranslated(context, "Economy"),
              selectionStatus: economy,
              selectionStatusValue: (value) {
                value == true
                    ? setState(() {
                  economy = value;
                  premiumEconomy = false;
                  business = false;
                  firstClass = false;
                })
                    : setState(() {
                  economy = value;
                });
              },
            ),
            HomeScreenBottomWidget2(
              classes: getTranslated(context, "Premium Economy"),
              selectionStatus: premiumEconomy,
              selectionStatusValue: (value) {
                value == true
                    ? setState(() {
                  economy = false;
                  premiumEconomy = value;
                  business = false;
                  firstClass = false;
                })
                    : setState(() {
                  premiumEconomy = value;
                });
              },
            ),
            HomeScreenBottomWidget2(
              classes: getTranslated(context, "Business"),
              selectionStatus: business,
              selectionStatusValue: (value) {
                value == true
                    ? setState(() {
                  economy = false;
                  premiumEconomy = false;
                  business = value;
                  firstClass = false;
                })
                    : setState(() {
                  business = value;
                });
              },
            ),
            HomeScreenBottomWidget2(
              classes: getTranslated(context, "First class"),
              selectionStatus: firstClass,
              selectionStatusValue: (value) {
                value == true
                    ? setState(() {
                  economy = false;
                  premiumEconomy = false;
                  business = false;
                  firstClass = value;
                })
                    : setState(() {
                  firstClass = value;
                });
              },
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 30),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                HomeScreenSubmitButton(
                  buttonName: getTranslated(context, "Apply"),
                  onTap: () {
                    ObjectFactory().hiveBox.hivePutInt(key: "adultCount", value: adultCount);
                    ObjectFactory().hiveBox.hivePutInt(key: "childrenCount", value: childrenCount);
                    ObjectFactory().hiveBox.hivePutInt(key: "infantCount", value: infantsCount);
                    ObjectFactory().hiveBox.hivePutBool(key: "economy", value: economy);
                    ObjectFactory().hiveBox.hivePutBool(key: "premiumEconomy", value: premiumEconomy);
                    ObjectFactory().hiveBox.hivePutBool(key: "business", value: business);
                    ObjectFactory().hiveBox.hivePutBool(key: "firstClass", value: firstClass);


                    // print('Adult count is '+ ObjectFactory().hiveBox.hiveGetInt(key:"adultCount").toString());
                    // print('Children count is '+ ObjectFactory().hiveBox.hiveGetInt(key:"childrenCount").toString());
                    // print('Infant count is '+ ObjectFactory().hiveBox.hiveGetInt(key:"infantCount").toString());
                    //  print('economy is '+ ObjectFactory().hiveBox.hiveGetBool(key:"economy").toString());
                    //  print('premiumEconomy is '+ ObjectFactory().hiveBox.hiveGetBool(key:"premiumEconomy").toString());
                    //  print('business is '+ ObjectFactory().hiveBox.hiveGetBool(key:"business").toString());
                    // print('firstClass is '+ ObjectFactory().hiveBox.hiveGetBool(key:"firstClass").toString());


                    Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context)=>HomeScreen()), (route) => false);
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
