import 'package:flightbooking/src/utils/constants.dart';
import 'package:flightbooking/src/utils/util.dart';
import 'package:flutter/material.dart';

class HomeScreenBottomWidget2 extends StatefulWidget {
  String classes;
  bool selectionStatus;
  ValueChanged selectionStatusValue;
  HomeScreenBottomWidget2({this.classes, this.selectionStatusValue, this.selectionStatus});
  @override
  _HomeScreenBottomWidget2State createState() => _HomeScreenBottomWidget2State();
}

class _HomeScreenBottomWidget2State extends State<HomeScreenBottomWidget2> {
  @override
  Widget build(BuildContext context) {
    return  GestureDetector(
      onTap: (){
        setState(() {
          widget.selectionStatus = true;
        });
        widget.selectionStatusValue(widget.selectionStatus);
      },
      child: Container(
        width: screenWidth(context,dividedBy: 1.08),
        height: screenHeight(context,dividedBy: 13),
        color: Colors.transparent,
        child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(widget.classes,style: TextStyle(
                  fontFamily:
                  'ProximaNova',
                  fontStyle:
                  FontStyle.normal,
                  fontWeight:
                  FontWeight.w400,
                  fontSize: 16,
                  color: Constants
                      .kitGradients[
                  28]),),
              GestureDetector(
                child: Container(
                  width: 25.0,
                  height: 25.0,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(
                          color: Constants.kitGradients[36],
                          width: 2
                      )
                  ),
                  child: widget.selectionStatus == true?Padding(
                    padding: const EdgeInsets.all(3.0),
                    child: Container(
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Constants.kitGradients[36],
                      ),
                    ),
                  ):Container(),
                ),
                onTap: (){
                  setState(() {
                    widget.selectionStatus = true;
                  });
                  widget.selectionStatusValue(widget.selectionStatus);
                },
              ),
            ]
        ),
      ),
    );
  }
}
