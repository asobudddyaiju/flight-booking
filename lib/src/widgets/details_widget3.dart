import 'package:flightbooking/src/utils/constants.dart';
import 'package:flightbooking/src/utils/util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class DetailsWidget3 extends StatefulWidget {
  String layOverPlace, layOverDuration;
  DetailsWidget3({this.layOverDuration, this.layOverPlace});
  @override
  _DetailsWidget3State createState() => _DetailsWidget3State();
}

class _DetailsWidget3State extends State<DetailsWidget3> {
  @override
  void initState() {

    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth(context,dividedBy: 1.0),
      height: screenHeight(context,dividedBy: 12.0),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15.0),
          color: Constants.kitGradients[38]
      ),
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [

            Container(
             // color: Colors.green,
              width: screenWidth(context,dividedBy: 1.55),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    width: screenWidth(context,dividedBy: 15),
                    height: screenHeight(context,dividedBy: 25),
                   // color: Colors.greenAccent,
                    child: SvgPicture.asset('assets/icons/details_clock.svg'),
                  ),
                  SizedBox(width: screenWidth(context,dividedBy: 27),),
                  Container(
                    width: screenWidth(context,dividedBy: 1.9),
                    child: Text(
                      widget.layOverPlace,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          color: Constants.kitGradients[28],
                          fontFamily: 'ProximaNova',
                          fontSize: 18,
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal),
                    ),
                  ),
                ],
              ),
            ),


            Container(
              width: screenWidth(context,dividedBy: 3.9),
              //color: Colors.yellow,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text(
                    widget.layOverDuration,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      color: Constants.kitGradients[28],
                        fontFamily: 'ProximaNova',
                        fontSize: 18,
                        fontWeight: FontWeight.w700,
                        fontStyle: FontStyle.normal),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
