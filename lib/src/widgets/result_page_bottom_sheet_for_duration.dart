import 'package:flightbooking/src/utils/constants.dart';
import 'package:flightbooking/src/utils/object_factory.dart';
import 'package:flightbooking/src/utils/util.dart';
import 'package:flutter/material.dart';

class ResultPageBottomSheetForDuration extends StatefulWidget {
  List<int> durationDepartureArray = [];
  List<int> durationReturnArray = [];
  ValueChanged showSaveButtonValueChanged;
  ValueChanged departRangeValue;
  ValueChanged returnRangeValue;
  bool roundTripStatus;

  ResultPageBottomSheetForDuration(
      {this.durationDepartureArray,
      this.durationReturnArray,
      this.showSaveButtonValueChanged,
      this.departRangeValue,
      this.returnRangeValue,
      this.roundTripStatus});

  @override
  _ResultPageBottomSheetForDurationState createState() =>
      _ResultPageBottomSheetForDurationState();
}

class _ResultPageBottomSheetForDurationState
    extends State<ResultPageBottomSheetForDuration> {
  double _lowerValue;

  double _upperValue;

  double departRangeStartsAt;
  double returnRangeEndsAt;
  bool checkDepartureRangeIsLowerThanReturn;
  double variableForObjectFactoryDeparture;
  double variableForObjectFactoryReturn;
  double x;
  double y;

  @override
  void initState() {
    _upperValue = ObjectFactory().hiveBox.getFlightDuration() != null
        ? int.parse(ObjectFactory().hiveBox.getFlightDuration()).toDouble()
        : Duration(hours: 100).inSeconds.toDouble();
    // print("Flight Duration" + ObjectFactory().hiveBox.getFlightDuration());
    // print(_upperValue);
    // returnRangeEndsAt = widget.durationReturnArray.reduce(max).toDouble();
    // print("max" + ObjectFactory().hiveBox.getFlightDuration());

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          width: screenWidth(context, dividedBy: 1),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  // getDurationInHour(_lowerValue.round()),
                  "0hr",
                  style: TextStyle(
                      fontSize: 16.0,
                      fontFamily: 'AirbnbCerealAppRegular',
                      fontWeight: FontWeight.w700,
                      color: Constants.kitGradients[13]),
                ),
                Text(
                  getDurationInHour(_upperValue.round()),
                  style: TextStyle(
                      fontSize: 16.0,
                      fontFamily: 'AirbnbCerealAppRegular',
                      fontWeight: FontWeight.w700,
                      color: Constants.kitGradients[13]),
                ),
              ],
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0),
          child: SliderTheme(
              data: SliderTheme.of(context).copyWith(
                overlayColor: Constants.kitGradients[0],
                activeTickMarkColor: Constants.kitGradients[0],
                activeTrackColor: Constants.kitGradients[0],
                inactiveTrackColor: Colors.grey,
                //trackHeight: 8.0,
                thumbColor: Constants.kitGradients[0],
                valueIndicatorColor: Constants.kitGradients[0],
                showValueIndicator: false
                    ? ShowValueIndicator.always
                    : ShowValueIndicator.onlyForDiscrete,
              ),
              child: Slider(
                min: 0,
                max: Duration(hours: 100).inSeconds.toDouble(),
                divisions: 100,
                value: _upperValue,
                onChanged: (value) {
                  widget.showSaveButtonValueChanged(true);
                  setState(() {
                    _upperValue = value;
                  });
                  widget.returnRangeValue(value);
                },
              )
              // frs.RangeSlider(
              //   divisions: 100,
              //   min: 0,
              //   max: Duration(hours: 100).inSeconds.toDouble(),
              //   lowerValue: 0,
              //   upperValue: _upperValue,
              //   showValueIndicator: true,
              //   onChanged: (double newLowerValue, double newUpperValue) {
              //     setState(() {
              //       // _lowerValue = departRangeStartsAt;
              //       _upperValue = newUpperValue;
              //     });
              //     print("timo" + _upperValue.toString());
              //   },
              //   onChangeStart: (double startLowerValue, double startUpperValue) {
              //     widget.showSaveButtonValueChanged(true);
              //   },
              //   onChangeEnd: (double newLowerValue, double newUpperValue) {
              //     widget.showSaveButtonValueChanged(true);
              //     widget.returnRangeValue(newUpperValue);
              //     // if (widget.roundTripStatus == true) {
              //     //   if (checkDepartureRangeIsLowerThanReturn == true) {
              //     //     // widget.departRangeValue(newLowerValue);
              //     //     widget.returnRangeValue(newUpperValue);
              //     //     ObjectFactory()
              //     //         .hiveBox
              //     //         .putDepartureRangeLowerThanReturnRange(value: true);
              //     //   } else {
              //     //     widget.departRangeValue(newUpperValue);
              //     //     widget.returnRangeValue(newLowerValue);
              //     //     ObjectFactory()
              //     //         .hiveBox
              //     //         .putDepartureRangeLowerThanReturnRange(value: false);
              //     //   }
              //     // } else {
              //     //   widget.departRangeValue(newLowerValue);
              //     //   widget.returnRangeValue(newUpperValue);
              //     //   ObjectFactory()
              //     //       .hiveBox
              //     //       .putDepartureRangeLowerThanReturnRange(value: true);
              //     // }
              //   },
              // ),
              ),
        ),
        SizedBox(
          height: screenHeight(context, dividedBy: 10.0),
        ),
      ],
    );
  }
}
