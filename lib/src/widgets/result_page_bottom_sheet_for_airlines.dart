import 'package:flightbooking/src/utils/constants.dart';
import 'package:flightbooking/src/utils/flight_names.dart';
import 'package:flightbooking/src/utils/object_factory.dart';
import 'package:flightbooking/src/utils/util.dart';
import 'package:flightbooking/src/widgets/result_page_bottom_sheet_for_airlines_list_view.dart';
import 'package:flutter/material.dart';

class ResultPageBottomSheetForAirlines extends StatefulWidget {
  List<String> departureAirlinesArray;
  List<String> returnAirlinesArray;
  ValueChanged showSaveButtonValueChanged;

  ResultPageBottomSheetForAirlines(
      {this.departureAirlinesArray,
        this.returnAirlinesArray,
        this.showSaveButtonValueChanged});

  @override
  _ResultPageBottomSheetForAirlinesState createState() =>
      _ResultPageBottomSheetForAirlinesState();
}

class _ResultPageBottomSheetForAirlinesState
    extends State<ResultPageBottomSheetForAirlines> {
  ScrollController _controller = ScrollController();
  List<dynamic> combinedAirlinesArray;
  List<dynamic> combinedAirlinesArrayDummy;
  String selectedFlightsCodesString = "removeFromString";
  List<bool> airlineSelectionStatusArray = [];
  bool selectAllButtonStatus;

  @override
  void initState() {
    setState(() {
      ObjectFactory().hiveBox.getSelectAllButtonStatus() == null?selectAllButtonStatus = true:ObjectFactory().hiveBox.getSelectAllButtonStatus();
      combinedAirlinesArray = (List.from(widget.departureAirlinesArray)
        ..addAll(widget.returnAirlinesArray))
          .toSet()
          .toList();
      combinedAirlinesArrayDummy = (List.from(widget.departureAirlinesArray)
        ..addAll(widget.returnAirlinesArray))
          .toSet()
          .toList();
    });

    for (int i = 0; i < combinedAirlinesArray.length; i++) {
      setState(() {
        airlineSelectionStatusArray.add(true);
      });
    }
    // print("airlineSelectionStatusArray is " +
    //     airlineSelectionStatusArray.toString());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        GestureDetector(
          onTap: () {
            widget.showSaveButtonValueChanged(true);

            if(selectAllButtonStatus == false){
              setState((){
                selectAllButtonStatus = true;
                selectedFlightsCodesString = "";
                airlineSelectionStatusArray.clear();
              });
              for (int i = 0; i < combinedAirlinesArray.length; i++) {
                setState(() {
                  selectedFlightsCodesString = selectedFlightsCodesString +
                      combinedAirlinesArray[i].toString() +
                      ",";
                });
              }
              setState(() {
                combinedAirlinesArrayDummy = (List.from(widget.departureAirlinesArray)
                  ..addAll(widget.returnAirlinesArray))
                    .toSet()
                    .toList();
              });

              ObjectFactory()
                  .hiveBox
                  .putAirlineFilterSearch(value: selectedFlightsCodesString);

              for (int i = 0; i < combinedAirlinesArray.length; i++) {
                setState(() {
                  airlineSelectionStatusArray.add(true);
                });
              }

            }
            ObjectFactory().hiveBox.putSelectAllButtonStatus(value: selectAllButtonStatus);
            print("combinedAirlinesArrayDummy is  " +
                combinedAirlinesArrayDummy.toString());
            print("airlineSelectionStatusArray is  " +
                airlineSelectionStatusArray.toString());
            print(
                "selectedFlightsCodesString is  " + selectedFlightsCodesString);
          },
          child: Container(
            color: Colors.transparent,
            width: screenWidth(context, dividedBy: 1.1),
            height: screenHeight(context, dividedBy: 18),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  'Select all',
                  style: TextStyle(
                      fontSize: 18.0,
                      fontFamily: 'ProximaNova',
                      fontWeight: FontWeight.w400,
                      color: Constants.kitGradients[28]),
                ),    Container(
                  width: 25,
                  height: 25,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20.0),
                      border: Border.all(
                          color: Constants.kitGradients[14],
                          width: 2.0
                      )
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(3.0),
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20.0),
                        color: selectAllButtonStatus == true?Constants.kitGradients[14]:Constants.kitGradients[3],
                      ),),
                  ),
                )
              ],
            ),
          ),
        ),
        Divider(),
        Expanded(
          child: ListView.builder(
              shrinkWrap: true,
              itemCount: combinedAirlinesArray.length,
              controller: _controller,
              scrollDirection: Axis.vertical,
              itemBuilder: (BuildContext context, int index) {
                return ResultPageBottomSheetForAirlinesListViewWidget(
                  airlineName: getFlightName(
                      code: combinedAirlinesArray[index].toString()),
                  onTap: () {
                    widget.showSaveButtonValueChanged(true);

                    setState(() {
                      selectAllButtonStatus = false;
                      selectedFlightsCodesString = "";
                    });

                    if (combinedAirlinesArrayDummy[index] ==
                        "removeFromString") {
                      setState(() {
                        combinedAirlinesArrayDummy.insert(
                            index, combinedAirlinesArray[index]);
                        combinedAirlinesArrayDummy.removeAt((index + 1));
                      });
                    } else {
                      setState(() {
                        combinedAirlinesArrayDummy.insert(
                            index, "removeFromString");
                        combinedAirlinesArrayDummy.removeAt((index + 1));
                      });
                    }

                    for (int i = 0; i < combinedAirlinesArray.length; i++) {
                      if (combinedAirlinesArrayDummy[i] != "removeFromString") {
                        setState(() {
                          selectedFlightsCodesString =
                              selectedFlightsCodesString +
                                  combinedAirlinesArrayDummy[i].toString() +
                                  ",";
                        });
                      }
                    }

                    ObjectFactory().hiveBox.putAirlineFilterSearch(
                        value: selectedFlightsCodesString);

                    if (airlineSelectionStatusArray[index] == true) {
                      setState(() {
                        airlineSelectionStatusArray[index] = false;
                      });
                    } else {
                      setState(() {
                        airlineSelectionStatusArray[index] = true;
                      });
                    }

                    ObjectFactory().hiveBox.putSelectAllButtonStatus(value: selectAllButtonStatus);
                    // print("combinedAirlinesArrayDummy is  " +
                    //     combinedAirlinesArrayDummy.toString());
                    //  print("airlineSelectionStatusArray is  " +
                    //      airlineSelectionStatusArray.toString());
                    // print(
                    //     "selectedFlightsCodesString is  " + selectedFlightsCodesString);
                  },
                  index:index,
                  buttonClickStatus: airlineSelectionStatusArray,

                );
              }),
        ),
      ],
    );
  }
}
