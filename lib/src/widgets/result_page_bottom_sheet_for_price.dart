import 'package:flightbooking/src/utils/constants.dart';
import 'package:flightbooking/src/utils/currency_symbol.dart';
import 'package:flightbooking/src/utils/object_factory.dart';
import 'package:flightbooking/src/utils/util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_range_slider/flutter_range_slider.dart' as frs;

class ResultPageBottomSheetForPrice extends StatefulWidget {
  final List<int> priceTotalArray;
  ValueChanged showSaveButtonValueChanged;
  ValueChanged priceRangeLowerValue;
  ValueChanged priceRangeUpperValue;

  ResultPageBottomSheetForPrice(
      {this.priceTotalArray,
      this.showSaveButtonValueChanged,
      this.priceRangeLowerValue,
      this.priceRangeUpperValue});

  @override
  _ResultPageBottomSheetForPriceState createState() =>
      _ResultPageBottomSheetForPriceState();
}

class _ResultPageBottomSheetForPriceState
    extends State<ResultPageBottomSheetForPrice> {
  double _lowerValue = 20.0;
  double _upperValue = 80.0;
  double sliderRangeStartsAt;
  double sliderRangeEndsAt;
  double variableForDividing;

  @override
  void initState() {
    _lowerValue =
        ObjectFactory().hiveBox.getSelectedPriceRangeLowerForFilter() != null
            ? ObjectFactory().hiveBox.getSelectedPriceRangeLowerForFilter()
            : 0;
    _upperValue =
        ObjectFactory().hiveBox.getSelectedPriceRangeUpperForFilter() != null
            ? ObjectFactory()
                .hiveBox
                .getSelectedPriceRangeUpperForFilter()
                .toDouble()
            : 99999;
    print("nin" +
        ObjectFactory()
            .hiveBox
            .getSelectedPriceRangeUpperForFilter()
            .toString());
    print("min" +
        ObjectFactory()
            .hiveBox
            .getSelectedPriceRangeLowerForFilter()
            .toString());
    print("nix" + _lowerValue.toString());
    print("mix" + _upperValue.toString());

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          width: screenWidth(context, dividedBy: 1),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  (ObjectFactory().hiveBox.hiveGet(key: "currency_name") != null
                          ? (getCurrencySymbol(
                              code: ObjectFactory()
                                  .hiveBox
                                  .hiveGet(key: "currency_name")
                                  .split(" ")
                                  .first))
                          : ("\$")) +
                      _lowerValue.round().toString(),
                  style: TextStyle(
                      fontSize: 16.0,
                      fontFamily: 'AirbnbCerealAppRegular',
                      fontWeight: FontWeight.w700,
                      color: Constants.kitGradients[13]),
                ),
                Text(
                  (ObjectFactory().hiveBox.hiveGet(key: "currency_name") != null
                          ? (getCurrencySymbol(
                              code: ObjectFactory()
                                  .hiveBox
                                  .hiveGet(key: "currency_name")
                                  .split(" ")
                                  .first))
                          : ("\$")) +
                      _upperValue.round().toString(),
                  style: TextStyle(
                      fontSize: 16.0,
                      fontFamily: 'AirbnbCerealAppRegular',
                      fontWeight: FontWeight.w700,
                      color: Constants.kitGradients[13]),
                ),
              ],
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0),
          child: SliderTheme(
            data: SliderTheme.of(context).copyWith(
              overlayColor: Constants.kitGradients[0],
              activeTickMarkColor: Constants.kitGradients[0],
              activeTrackColor: Constants.kitGradients[0],
              inactiveTrackColor: Colors.grey,
              //trackHeight: 8.0,
              thumbColor: Constants.kitGradients[0],
              valueIndicatorColor: Constants.kitGradients[0],
              // showValueIndicator: false
              //     ? ShowValueIndicator.always
              //     : ShowValueIndicator.onlyForDiscrete,
            ),
            child: frs.RangeSlider(
              min: 0,
              max: 99999,
              divisions: 100,
              lowerValue: _lowerValue,
              upperValue: _upperValue,
              // showValueIndicator: true,
              valueIndicatorMaxDecimals: 1,
              onChanged: (double newLowerValue, double newUpperValue) {
                setState(() {
                  _lowerValue = newLowerValue;
                  _upperValue = newUpperValue;
                });
              },
              onChangeStart: (double startLowerValue, double startUpperValue) {
                widget.showSaveButtonValueChanged(true);
              },
              onChangeEnd: (double newLowerValue, double newUpperValue) {
                widget.showSaveButtonValueChanged(true);
                widget.priceRangeLowerValue(_lowerValue);
                widget.priceRangeUpperValue(_upperValue);
              },
            ),
          ),
        ),
        SizedBox(
          height: screenHeight(context, dividedBy: 10.0),
        ),
      ],
    );
  }
}
