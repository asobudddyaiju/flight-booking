import 'package:flightbooking/src/utils/constants.dart';
import 'package:flightbooking/src/utils/util.dart';
import 'package:flutter/material.dart';

class ResultPageBottomSheetForCheapest extends StatefulWidget {
  Function onTapCheapest, onTapQuickest;
  bool tickCheapest, tickQuickest;
  ResultPageBottomSheetForCheapest({this.onTapCheapest, this.onTapQuickest, this.tickCheapest, this.tickQuickest});
  @override
  _ResultPageBottomSheetForCheapestState createState() => _ResultPageBottomSheetForCheapestState();
}

class _ResultPageBottomSheetForCheapestState extends State<ResultPageBottomSheetForCheapest> {

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        GestureDetector(
          onTap: widget.onTapCheapest,
          child: Container(
            color: Colors.transparent,
            width: screenWidth(context, dividedBy: 1.1),
            height: screenHeight(context, dividedBy: 18),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  'Cheapest',
                  style: TextStyle(
                      fontSize: 18.0,
                      fontFamily: 'ProximaNova',
                      fontWeight: FontWeight.w400,
                      color: Constants.kitGradients[28]),
                ),
                Container(
                  width: 25,
                  height: 25,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20.0),
                      border: Border.all(
                          color: Constants.kitGradients[14],
                          width: 2.0
                      )
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(3.0),
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20.0),
                        color: widget.tickCheapest == true?Constants.kitGradients[14]:Constants.kitGradients[3],
                      ),),
                  ),
                )
              ],
            ),
          ),
        ),
        Divider(),
        GestureDetector(
          onTap: widget.onTapQuickest,
          child: Container(
            color: Colors.transparent,
            width: screenWidth(context, dividedBy: 1.1),
            height: screenHeight(context, dividedBy: 18),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  'Quickest',
                  style: TextStyle(
                      fontSize: 18.0,
                      fontFamily: 'ProximaNova',
                      fontWeight: FontWeight.w400,
                      color: Constants.kitGradients[28]),
                ),
              Container(
                width: 25,
                height: 25,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20.0),
                    border: Border.all(
                        color: Constants.kitGradients[14],
                        width: 2.0
                    )
                ),
                child: Padding(
                  padding: const EdgeInsets.all(3.0),
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20.0),
                      color: widget.tickQuickest == true?Constants.kitGradients[14]:Constants.kitGradients[3],
                    ),),
                ),
              )
              ],
            ),
          ),
        ),
        SizedBox(
          height: screenHeight(context, dividedBy: 20),
        )
      ],
    );
  }
}
