import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flightbooking/localization/locale.dart';
import 'package:flightbooking/src/screens/destination_selection_from.dart';
import 'package:flightbooking/src/utils/constants.dart';
import 'package:flightbooking/src/utils/util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class HomeScreenDestination extends StatefulWidget {
  String destinationFrom, destinationTo, destinationFromName, destinationToName;
  String tempForSwap, tempNameForSwap;
  ValueChanged fromOnChanged, toOnChanged, fromNameOnChanged, toNameOnChanged;
  HomeScreenDestination({this.destinationFrom, this.destinationTo,this.destinationFromName, this.destinationToName, this.fromOnChanged, this.toOnChanged, this.fromNameOnChanged, this.toNameOnChanged});

  @override
  _HomeScreenDestinationState createState() => _HomeScreenDestinationState();
}

class _HomeScreenDestinationState extends State<HomeScreenDestination> {
  @override
  Widget build(BuildContext context) {
    return Container(
        width: screenWidth(context, dividedBy: 1.2),
        height: screenHeight(context, dividedBy: 7.5),
        child: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Container(
                width: screenWidth(context,dividedBy: 3.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Center(
                      child: Text(
                        getTranslated(context, "From"),
                        style: TextStyle(
                          fontFamily: 'ProximaNova',
                          color: Constants.kitGradients[28],
                          fontSize: 12,
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal,
                        ),
                      ),
                    ),
                    Center(
                      child: GestureDetector(
                        child: Text(
                          widget.destinationFrom,
                          overflow:TextOverflow.ellipsis,
                          style: TextStyle(
                              fontFamily: 'ProximaNovaBold',
                              color: Constants.kitGradients[28],
                              fontSize: 48,
                              fontWeight: FontWeight.w900,
                              fontStyle: FontStyle.normal,
                          ),
                        ),
                        onTap: () {
                          FirebaseAnalytics().logEvent(name:"clicked_destination_from_selection_widget",parameters: null);
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      DestinationFrom(destinationTo: false,)));

                        },
                      ),
                    ),
                    Center(
                      child: Text(
                        widget.destinationFromName,
                        overflow:TextOverflow.ellipsis,
                        style: TextStyle(
                            fontFamily: 'ProximaNova',
                            color: Constants.kitGradients[16],
                            fontSize: 10,
                            fontWeight: FontWeight.w400,
                            fontStyle: FontStyle.normal),
                      ),
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 0, top: 2),
                child: Container(
                  height: 37.0,
                  width: 37.0,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Constants.kitGradients[17],
                  ),
                  child: GestureDetector(
                      onTap: () {
                        FirebaseAnalytics().logEvent(name:"swapped_destination",parameters: null);
                       setState(() {
                         widget.tempForSwap = widget.destinationFrom;
                         widget.destinationFrom = widget.destinationTo;
                         widget.destinationTo = widget.tempForSwap;
                         widget.tempNameForSwap = widget.destinationFromName;
                         widget.destinationFromName = widget.destinationToName;
                         widget.destinationToName = widget.tempNameForSwap;
                       });
                       widget.fromOnChanged(widget.destinationFrom);
                       widget.toOnChanged(widget.destinationTo);
                       widget.fromNameOnChanged(widget.destinationFromName);
                       widget.toNameOnChanged(widget.destinationToName);
                      },
                      child: SvgPicture.asset('assets/icons/home_screen_arrows.svg')),
                ),
              ),
              Container(
                width: screenWidth(context,dividedBy: 3.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Center(
                      child: Text(
                        getTranslated(context, "To"),
                        style: TextStyle(
                            fontFamily: 'ProximaNova',
                            color: Constants.kitGradients[28],
                            fontSize: 12,
                            fontWeight: FontWeight.w400,
                            fontStyle: FontStyle.normal),
                      ),
                    ),
                    Center(
                      child: GestureDetector(
                        child: Text(
                          widget.destinationTo,
                          overflow:TextOverflow.ellipsis,
                          style: TextStyle(
                              fontFamily: 'ProximaNovaBold',
                              color: Constants.kitGradients[28],
                              fontSize: 48,
                              fontWeight: FontWeight.w900,
                              fontStyle: FontStyle.normal),
                        ),
                        onTap: () {
                          FirebaseAnalytics().logEvent(name: "clicked_destination_selection_to_widget",parameters: null);
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      DestinationFrom(destinationTo: true,)));
                        },
                      ),
                    ),
                    Center(
                      child: Text(
                       widget.destinationToName,
                        overflow:TextOverflow.ellipsis,
                        style: TextStyle(
                            fontFamily: 'ProximaNova',
                            color: Constants.kitGradients[16],
                            fontSize: 10,
                            fontWeight: FontWeight.w400,
                            fontStyle: FontStyle.normal),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ));
  }
}
