import 'package:flightbooking/src/utils/constants.dart';
import 'package:flightbooking/src/utils/util.dart';
import 'package:flutter/material.dart';

class ResultPageBottomSheetForAirlinesListViewWidget extends StatefulWidget {
 final Function onTap;
 final String airlineName;
 List<bool> buttonClickStatus;
 int index;


  ResultPageBottomSheetForAirlinesListViewWidget({this.onTap, this.airlineName, this.buttonClickStatus, this.index});
  @override
  _ResultPageBottomSheetForAirlinesListViewWidgetState createState() => _ResultPageBottomSheetForAirlinesListViewWidgetState();
}

class _ResultPageBottomSheetForAirlinesListViewWidgetState extends State<ResultPageBottomSheetForAirlinesListViewWidget> {

  @override
  Widget build(BuildContext context) {
    return   GestureDetector(
      onTap: widget.onTap,
      child: Column(
        children: [
          Container(
            color: Colors.transparent,
            width: screenWidth(context, dividedBy: 1.1),
            height: screenHeight(context, dividedBy: 18),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  widget.airlineName,
                  style: TextStyle(
                      fontSize: 18.0,
                      fontFamily: 'ProximaNova',
                      fontWeight: FontWeight.w400,
                      color: Constants.kitGradients[28]),
                ),
                Container(
                  width: 25,
                  height: 25,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20.0),
                      border: Border.all(
                          color: Constants.kitGradients[14],
                          width: 2.0
                      )
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(3.0),
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20.0),
                        color: widget.buttonClickStatus[widget.index] == true?Constants.kitGradients[14]:Constants.kitGradients[3],
                      ),),
                  ),
                )
              ],
            ),
          ),
      Divider(),
        ],
      ),
    );
  }
}
