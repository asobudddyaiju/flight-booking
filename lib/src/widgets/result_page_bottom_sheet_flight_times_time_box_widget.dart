import 'package:flightbooking/src/utils/constants.dart';
import 'package:flightbooking/src/utils/util.dart';
import 'package:flutter/material.dart';

class ResultPageBottomSheetFlightTimesTimeBoxWidget extends StatefulWidget {
 final String time;
 final Function onTap;
 bool onClickStatus;

  ResultPageBottomSheetFlightTimesTimeBoxWidget({this.time, this.onTap, this.onClickStatus});
  @override
  _ResultPageBottomSheetFlightTimesTimeBoxWidgetState createState() => _ResultPageBottomSheetFlightTimesTimeBoxWidgetState();
}

class _ResultPageBottomSheetFlightTimesTimeBoxWidgetState extends State<ResultPageBottomSheetFlightTimesTimeBoxWidget> {
  @override
  Widget build(BuildContext context) {
    return   GestureDetector(
      onTap: widget.onTap,
      child: Container(
        width: screenWidth(context, dividedBy: 2.5),
        height: screenHeight(context, dividedBy: 18),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(6.0),
          color: widget.onClickStatus ==false?Constants.kitGradients[33]:Constants.kitGradients[9],
        ),
        child: Center(
            child: Text(
              widget.time,
              style: TextStyle(
                  fontSize: 17.0,
                  fontFamily: 'ProximaNova',
                  fontWeight: FontWeight.w400,
                  color: widget.onClickStatus ==false?Constants.kitGradients[28]:Constants.kitGradients[3],),
            )),
      ),
    );
  }
}
