import 'package:flightbooking/src/screens/destination_selection_from.dart';
import 'package:flightbooking/src/screens/home_screen.dart';
import 'package:flightbooking/src/utils/constants.dart';
import 'package:flightbooking/src/utils/util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class ResultAppBarWidget extends StatefulWidget {
  final String dateTimePassengers, routeFrom, routeTo;

  ResultAppBarWidget({this.dateTimePassengers, this.routeFrom, this.routeTo});

  @override
  _ResultAppBarWidgetState createState() => _ResultAppBarWidgetState();
}

class _ResultAppBarWidgetState extends State<ResultAppBarWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth(context, dividedBy: 1),
      height: screenHeight(context, dividedBy: 8),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            GestureDetector(
              child: Container(
                  width: screenWidth(context, dividedBy: 8),
                  height: screenHeight(context, dividedBy: 25),
                  color: Colors.transparent,
                  child: Padding(
                    padding: const EdgeInsets.all(2.0),
                    child: SvgPicture.asset(
                      'assets/icons/back.svg',
                      color: Constants.kitGradients[28],
                    ),
                  )),
              onTap: () {
                Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(builder: (context) => HomeScreen()),
                        (route) => false);
              },
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  width: screenWidth(context, dividedBy: 1.4),
                  child: Center(
                    child: FittedBox(
                      fit: BoxFit.fitWidth,
                      child: Text(
                        widget.routeFrom + " - "+ widget.routeTo,
                        overflow: TextOverflow.clip,
                        style: TextStyle(
                          fontFamily: 'ProximaNovaBold',
                          color: Constants.kitGradients[28],
                          fontSize: 16,
                          fontWeight: FontWeight.w700,
                          fontStyle: FontStyle.normal,
                        ),
                      ),
                    ),
                  ),
                ),
                Text(
                  widget.dateTimePassengers,
                  style: TextStyle(
                    fontFamily: 'ProximaNova',
                    color: Constants.kitGradients[29],
                    fontSize: 12,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,
                  ),
                ),
              ],
            ),
            GestureDetector(
              child: Container(
                child: Padding(
                  padding: const EdgeInsets.all(2.0),
                  child: SvgPicture.asset('assets/icons/edit.svg',
                      color: Constants.kitGradients[28]),
                ),
                width: screenWidth(context, dividedBy: 10),
                height: screenHeight(context, dividedBy: 28),
              ),
              onTap: () {
                Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(builder: (context) => HomeScreen()),
                        (route) => false);
              },
            ),
          ],
        ),
      ),
    );
  }
}
