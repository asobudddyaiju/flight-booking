import 'package:flightbooking/src/utils/constants.dart';
import 'package:flightbooking/src/utils/util.dart';
import 'package:flutter/material.dart';

class DetailsWidget1 extends StatefulWidget {
  String route, duration;
  Color backgroundColor;
  DetailsWidget1({this.route, this.duration, this.backgroundColor});
  @override
  _DetailsWidget1State createState() => _DetailsWidget1State();
}

class _DetailsWidget1State extends State<DetailsWidget1> {
  @override
  Widget build(BuildContext context) {
    return  Container(
      width: screenWidth(context,dividedBy: 1.0),
      height: screenHeight(context,dividedBy: 9.0),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15.0),
          color: widget.backgroundColor
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 18.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              widget.route,
              style: TextStyle(
                  color: Constants.kitGradients[38],
                  fontFamily: 'ProximaNova',
                  fontSize: 20,
                  fontWeight: FontWeight.w700,
                  fontStyle: FontStyle.normal),
            ),
            Text(
             widget.duration,
              style: TextStyle(
                  color: Constants.kitGradients[38],
                  fontFamily: 'ProximaNova',
                  fontSize: 15,
                  fontWeight: FontWeight.w400,
                  fontStyle: FontStyle.normal),
            ),
          ],
        ),
      ),
    );
  }
}
