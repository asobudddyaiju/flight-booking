import 'package:flightbooking/src/utils/util.dart';
import 'package:flutter/material.dart';

import 'details_widget3.dart';
import 'details_widget_2.dart';

class DetailsListView1 extends StatefulWidget {
  final String title1, title2, title3, toTime,toAirport, toAirportCode, toDate,toPlace, fromTime, fromAirport, fromAirportCode, fromDate,fromPlace, layOverDuration, layOverPlace, airline;
  bool showWidget;
  DetailsListView1({this.layOverDuration,this.toAirport,this.toPlace,this.toTime,this.toDate,this.fromAirport,this.fromDate,this.fromPlace,this.fromTime,this.title2,this.title1,this.title3,this.layOverPlace, this.airline, this.toAirportCode, this.fromAirportCode});
  @override
  _DetailsListView1State createState() => _DetailsListView1State();
}

class _DetailsListView1State extends State<DetailsListView1> {
  @override
  void initState() {
    if(widget.layOverPlace.trim()== "dontShowWidget") {
      setState(() {
        widget.showWidget = false;
      });
    }else{
      setState(() {
        widget.showWidget = true;
      });
    }
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        DetailsWidget2(
          title1: widget.title1,
          title2: widget.title2,
          title3: widget.title3,
          toTime: widget.toTime,
          toAirport: widget.toAirport,
          toDate: widget.toDate,
          toPlace: widget.toPlace,
          fromTime: widget.fromTime,
          fromAirport:widget.fromAirport,
          fromDate: widget.fromDate,
          fromPlace: widget.fromPlace,
          airline: widget.airline,
          fromAirportCode: widget.fromAirportCode,
          toAirportCode: widget.toAirportCode,
        ),
        SizedBox(
          height: screenHeight(context, dividedBy: 105),
        ),
        widget.showWidget == true?DetailsWidget3(
          layOverPlace: 'Layover in '+widget.layOverPlace,
          layOverDuration: widget.layOverDuration,
        ):Container(),
        SizedBox(
          height: screenHeight(context, dividedBy: 105),
        ),
      ],
    );
  }
}
