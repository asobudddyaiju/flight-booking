import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flightbooking/localization/locale.dart';
import 'package:flightbooking/src/utils/constants.dart';
import 'package:flightbooking/src/utils/object_factory.dart';
import 'package:flightbooking/src/utils/util.dart';
import 'package:flightbooking/src/widgets/sliding_switch.dart';
import 'package:flutter/material.dart';

class HomeScreenSlideSwitch extends StatefulWidget {
  bool roundTripStatus;
  ValueChanged tripValue;
  HomeScreenSlideSwitch({this.roundTripStatus,this.tripValue});
  @override
  _HomeScreenSlideSwitchState createState() => _HomeScreenSlideSwitchState();
}

class _HomeScreenSlideSwitchState extends State<HomeScreenSlideSwitch> {
  @override
  Widget build(BuildContext context) {
    return SlidingSwitch(
      value: widget.roundTripStatus,
      width: screenWidth(context, dividedBy: 1.2),
      onChanged: (bool value) {
        FirebaseAnalytics().logEvent(name: "sliding_switch_trip_status",parameters: null);
        widget.tripValue(value);
        setState(() {
          widget.roundTripStatus=value;
        });
        ObjectFactory().hiveBox.hivePut(key: 'roundTripStatus',value: widget.roundTripStatus.toString());
      },
      height: screenHeight(context, dividedBy: 14),
      animationDuration: const Duration(milliseconds: 300),
      onTap: () {},
      onDoubleTap: () {},
      onSwipe: () {},
      textOff:getTranslated(context, "One_way"),
      textOn: getTranslated(context, "Round_trip"),
      colorOn: Constants.kitGradients[3],
      colorOff: Constants.kitGradients[3],
      background: const Color(0xffe4e5eb),
      buttonColor: Constants.kitGradients[14],
      inactiveColor: Constants.kitGradients[28],
    );
  }
}
