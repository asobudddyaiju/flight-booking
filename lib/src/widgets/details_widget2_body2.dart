import 'package:flightbooking/src/utils/util.dart';
import 'package:flightbooking/src/widgets/details_widget2_body2_A.dart';
import 'package:flutter/material.dart';

import 'details_widget2_body2_icon.dart';
class DetailsWidget2Body2 extends StatefulWidget {
  String fromTime, toTime, fromDate, toDate, fromAirport,fromAirportCode, fromPlace,toPlace, toAirport, toAirportCode;
  DetailsWidget2Body2({this.fromAirport, this.toAirport, this.toTime,this.toDate, this.fromDate, this.fromTime, this.fromPlace, this.toPlace, this.toAirportCode, this.fromAirportCode});
  @override
  _DetailsWidget2Body2State createState() => _DetailsWidget2Body2State();
}

class _DetailsWidget2Body2State extends State<DetailsWidget2Body2> {
  @override
  Widget build(BuildContext context) {
    return   Container(
      // height: screenHeight(context, dividedBy: 4),
      width: screenWidth(context, dividedBy: 1.0),
      //color: Colors.yellow,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(
            width: screenWidth(context,dividedBy: 20),
          ),
          DetailsWidget2Body2Icon(),
          SizedBox(
            width: screenWidth(context,dividedBy: 30),
          ),
          Container(
            width: screenWidth(context, dividedBy: 1.3),
           // color: Colors.orange,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height:
                  screenHeight(context, dividedBy: 300),
                ),
               DetailsWidget2Body2A(time:  widget.fromTime, date: widget.fromDate, place: widget.fromPlace, Airport: widget.fromAirport,airportCode: widget.fromAirportCode,),
                SizedBox(
                  height:
                  screenHeight(context, dividedBy: 17),
                ),
               DetailsWidget2Body2A(time:  widget.toTime, date: widget.toDate, place: widget.toPlace, Airport: widget.toAirport,airportCode: widget.toAirportCode,),
              ],
            ),
          )
        ],
      ),
    );
  }
}
