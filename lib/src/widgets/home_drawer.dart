import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flightbooking/auth_service/firebase_auth.dart';
import 'package:flightbooking/localization/locale.dart';
import 'package:flightbooking/src/screens/about_us.dart';
import 'package:flightbooking/src/screens/currency.dart';
import 'package:flightbooking/src/utils/constants.dart';
import 'package:flightbooking/src/utils/util.dart';
import 'package:flutter/material.dart';
import '../screens/home_screen.dart';
import '../utils/object_factory.dart';
class HomeDrawer extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _HomeDrawerState();
  }
}

class _HomeDrawerState extends State<HomeDrawer> {
  String buttonName = "Sign in";
  String photpurl;
  String displayName;

  @override
  void initState() {
    if (ObjectFactory().hiveBox.hiveGet(key: 'email') != null) {
      setState(() {
        photpurl = ObjectFactory().hiveBox.hiveGet(key: 'photourl');
        displayName = ObjectFactory().hiveBox.hiveGet(key: 'displayname');
        buttonName = 'Sign out';
      });
    } else {
      setState(() {
        buttonName = 'Sign in';
      });
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Drawer(
      child: ListView(
        children: <Widget>[
          Container(
            color: Constants.kitGradients[14],
            width: screenWidth(context, dividedBy: 1),
            height: screenHeight(context, dividedBy: 6),
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 15.0),
              child: Row(
                children: [
                  Container(
                    width: 38.0,
                    height: 40.24,
                    decoration: BoxDecoration(
                        color: Colors.black.withOpacity(.05),
                        shape: BoxShape.circle,
                        image: DecorationImage(
                            image:
                            ObjectFactory().hiveBox.hiveGet(key: 'email') !=
                                null
                                ? NetworkImage(photpurl)
                                : AssetImage(
                              'assets/images/user_profile.png',
                            ),
                            fit: BoxFit.fill),
                        border: Border.all(
                            color: Constants.kitGradients[3], width: 1)),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 15.0),
                    child: Text(
                        ObjectFactory().hiveBox.hiveGet(key: 'displayname') !=
                            null
                            ? displayName
                            : getTranslated(context, "username"),
                        style: TextStyle(
                            color: Constants.kitGradients[3],
                            fontWeight: FontWeight.w500,
                            fontSize: 18.0,
                            fontStyle: FontStyle.normal,
                            fontFamily: 'Poppins')),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 135),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: GestureDetector(
              onTap: () {
                FirebaseAnalytics()
                    .logEvent(name: "clicked_about_us", parameters: null);

                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => AboutUs()));
              },
              child: ListTile(
                title: Text(
                  getTranslated(context, "About_us"),
                  style: TextStyle(
                      color: Constants.kitGradients[19],
                      fontWeight: FontWeight.w400,
                      fontSize: 14.0,
                      fontStyle: FontStyle.normal),
                ),
              ),
            ),
          ),
          Container(
            width: screenWidth(context, dividedBy: 1),
            height: 1,
            color: Colors.grey.withOpacity(.10),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: GestureDetector(
              onTap: () {
                FirebaseAnalytics().logEvent(
                    name: "clicked_terms_and_conditions", parameters: null);
              },
              child: ListTile(
                title: Text(
                  getTranslated(context, "Terms_and_conditions"),
                  style: TextStyle(
                      color: Constants.kitGradients[19],
                      fontWeight: FontWeight.w400,
                      fontSize: 14.0,
                      fontStyle: FontStyle.normal),
                ),
              ),
            ),
          ),
          Container(
            width: screenWidth(context, dividedBy: 1),
            height: 1,
            color: Colors.grey.withOpacity(.10),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: GestureDetector(
              onTap: () {
                FirebaseAnalytics()
                    .logEvent(name: "clicked_privacy_policy", parameters: null);
              },
              child: ListTile(
                title: Text(
                  getTranslated(context, 'Privacy_policy'),
                  style: TextStyle(
                      color: Constants.kitGradients[19],
                      fontWeight: FontWeight.w400,
                      fontSize: 14.0,
                      fontStyle: FontStyle.normal),
                ),
              ),
            ),
          ),
          Container(
            width: screenWidth(context, dividedBy: 1),
            height: 1,
            color: Colors.grey.withOpacity(.10),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: GestureDetector(
              onTap: () async {
                if (ObjectFactory().hiveBox.hiveGet(key: 'email') == null) {
                  FirebaseAnalytics()
                      .logEvent(name: "clicked_sign_in", parameters: null);
                  String status = await Auth().signInWithGoogle();
                  if (status.trim() == "Success") {
                    FirebaseAnalytics().logEvent(
                        name: "signed_in_user",
                        parameters: {
                          "user email":
                          ObjectFactory().hiveBox.hiveGet(key: "email")
                        });
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => HomeScreen()));
                  }
                } else {
                  FirebaseAnalytics()
                      .logEvent(name: "clicked_sign_out", parameters: null);
                  Auth().Signout();
                  ObjectFactory().hiveBox.hivePut(key: "photourl", value: null);
                  ObjectFactory().hiveBox.hivePut(key: "email", value: null);
                  ObjectFactory()
                      .hiveBox
                      .hivePut(key: "displayname", value: null);
                  setState(() {
                    displayName = getTranslated(context, "username");
                    buttonName = 'Sign in';
                  });
                }
              },
              child: ListTile(
                title: Text(
                  getTranslated(context, buttonName),
                  style: TextStyle(
                      color: Constants.kitGradients[19],
                      fontWeight: FontWeight.w400,
                      fontSize: 14.0,
                      fontStyle: FontStyle.normal),
                ),
              ),
            ),
          ),
          Container(
            width: screenWidth(context, dividedBy: 1),
            height: 1,
            color: Colors.grey.withOpacity(.10),
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 3.5),
          ),
          Container(
            width: screenWidth(context, dividedBy: 1),
            height: 1,
            color: Colors.grey.withOpacity(.10),
          ),
          // Column(
          //   mainAxisAlignment: MainAxisAlignment.start,
          //   crossAxisAlignment: CrossAxisAlignment.start,
          //   children: [
          //     GestureDetector(
          //       onTap: () {
          //         FirebaseAnalytics().logEvent(name:"clicked_country",parameters: null);
          //         Navigator.push(context,
          //             MaterialPageRoute(builder: (context) => Country()));
          //       },
          //       child: Padding(
          //         padding: EdgeInsets.symmetric(
          //             vertical: screenHeight(context, dividedBy: 100),
          //             horizontal: screenWidth(context, dividedBy: 20)),
          //         child: Text(
          //           getTranslated(context, 'language'),
          //           style: TextStyle(
          //               fontFamily: 'ProximaNova',
          //               fontSize: 16,
          //               fontWeight: FontWeight.w400,
          //               color: Constants.kitGradients[20]),
          //         ),
          //       ),
          //     ),
          //     Padding(
          //         padding: EdgeInsets.symmetric(
          //             // vertical: screenHeight(context, dividedBy: 100),
          //             horizontal: screenWidth(context, dividedBy: 20)),
          //         child: GestureDetector(
          //             onTap: () {
          //               FirebaseAnalytics().logEvent(name:"clicked_country",parameters: null);
          //               Navigator.push(context,
          //                   MaterialPageRoute(builder: (context) => Country()));
          //             },
          //             child: Text('English',
          //                 style: TextStyle(
          //                     fontFamily: 'ProximaNova',
          //                     fontSize: 14,
          //                     fontWeight: FontWeight.w400,
          //                     fontStyle: FontStyle.normal,
          //                     color: Constants.kitGradients[14]))))
          //   ],
          // ),
          SizedBox(
            height: screenHeight(context, dividedBy: 75),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              GestureDetector(
                onTap: () {
                  FirebaseAnalytics()
                      .logEvent(name: "clicked_currency", parameters: null);
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => Currency()));
                },
                child: Padding(
                  padding: EdgeInsets.symmetric(
                      vertical: screenHeight(context, dividedBy: 100),
                      horizontal: screenWidth(context, dividedBy: 20)),
                  child: Text(
                    getTranslated(context, 'Currency'),
                    style: TextStyle(
                        fontFamily: 'ProximaNova',
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                        color: Constants.kitGradients[20]),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(
                  // vertical: screenHeight(context, dividedBy: 100),
                    horizontal: screenWidth(context, dividedBy: 20)),
                child: GestureDetector(
                    onTap: () {
                      FirebaseAnalytics()
                          .logEvent(name: "clicked_currency", parameters: null);
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => Currency()));
                    },
                    child: Text(
                        ObjectFactory().hiveBox.hiveGet(key: "currency_name") !=
                            null
                            ? ObjectFactory()
                            .hiveBox
                            .hiveGet(key: "currency_name")
                            .split(" ")
                            .first
                            : "USD",
                        overflow: TextOverflow.visible,
                        style: TextStyle(
                            fontFamily: 'ProximaNova',
                            fontSize: 14,
                            fontWeight: FontWeight.w400,
                            fontStyle: FontStyle.normal,
                            color: Constants.kitGradients[14]))),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
