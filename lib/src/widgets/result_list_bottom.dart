import 'package:flightbooking/localization/locale.dart';
import 'package:flightbooking/src/utils/constants.dart';
import 'package:flightbooking/src/utils/currency_symbol.dart';
import 'package:flightbooking/src/utils/flight_names.dart';
import 'package:flightbooking/src/utils/object_factory.dart';
import 'package:flightbooking/src/utils/util.dart';
import 'package:flightbooking/src/widgets/result_list_bottom1.dart';
import 'package:flutter/material.dart';

class ResultListBottom extends StatefulWidget {
  final bool roundTripStatus;
  final String offersFound,
      price,
      tripDurationDepart,
      tripDurationReturn,
      departTime,
      arrivalTime,
      departTimeReturn,
      arrivalTimeReturn,
      departPort,
      arrivalPort,
      departPortReturn,
      arrivalPortReturn,
      airlineDeparture,
      airlineReturn;
  final int departStop, returnStop;
  final Function onTap;

  ResultListBottom(
      {this.roundTripStatus,
      this.offersFound,
      this.onTap,
      this.price,
      this.tripDurationDepart,
      this.tripDurationReturn,
      this.departStop,
      this.returnStop,
      this.arrivalTime,
      this.departTime,
      this.arrivalTimeReturn,
      this.departTimeReturn,
      this.departPort,
      this.arrivalPort,
      this.departPortReturn,
      this.arrivalPortReturn,
      this.airlineDeparture,
      this.airlineReturn});

  @override
  _ResultListBottomState createState() => _ResultListBottomState();
}

class _ResultListBottomState extends State<ResultListBottom> {
  String stopDepart, stopReturn;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 5.0),
      child: GestureDetector(
        child: Container(
          width: screenWidth(context, dividedBy: 1),
          decoration: BoxDecoration(
            color: Constants.kitGradients[3],
            borderRadius: BorderRadius.circular(15.0),
          ),
          child: Padding(
            padding: const EdgeInsets.all(6.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ResultListBottom1(
                  callFrom2: false,
                  dateTime: widget.departTime + " - " + widget.arrivalTime,
                  route1: widget.departPort +
                      " - " +
                      widget.arrivalPort +
                      ", " +
                      getFlightName(code: widget.airlineDeparture),
                  stops: widget.departStop > 1
                      ? widget.departStop.toString() + " Stops"
                      : widget.departStop.toString() + " Stop",
                  tripDuration: widget.tripDurationDepart,
                  airline: widget.airlineDeparture,
                ),
                widget.roundTripStatus == true
                    ? ResultListBottom1(
                        callFrom2: true,
                        dateTime: widget.departTimeReturn +
                            " - " +
                            widget.arrivalTimeReturn,
                        route1: widget.departPortReturn +
                            " - " +
                            widget.arrivalPortReturn +
                            ", " +
                            getFlightName(code: widget.airlineReturn),
                        stops: widget.returnStop > 1
                            ? widget.returnStop.toString() +
                                " " +
                                getTranslated(context, "Stops")
                            : widget.returnStop.toString() +
                                " " +
                                getTranslated(context, "Stop"),
                        tripDuration: widget.tripDurationReturn,
                        airline: widget.airlineReturn,
                      )
                    : Container(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Container(
                      //color: Colors.yellow,
                      width: screenWidth(context, dividedBy: 1.15),
                      height: screenHeight(context, dividedBy: 28),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            widget.offersFound,
                            style: TextStyle(
                              fontFamily: 'ProximaNova',
                              color: Constants.kitGradients[29],
                              fontSize: 12,
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.normal,
                            ),
                          ),
                          Text(
                            (ObjectFactory()
                                        .hiveBox
                                        .hiveGet(key: "currency_name")
                                         !=
                                    null
                                ? (getCurrencySymbol(
                                    code: ObjectFactory()
                                        .hiveBox
                                        .hiveGet(key: "currency_name")
                                        .split(" ")
                                        .first))
                                : ("\$")) + widget.price,
                            style: TextStyle(
                              fontFamily: 'ProximaNova',
                              color: Constants.kitGradients[32],
                              fontSize: 20,
                              fontWeight: FontWeight.w700,
                              fontStyle: FontStyle.normal,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
        onTap: widget.onTap,
      ),
    );
  }
}
