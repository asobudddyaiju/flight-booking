import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flightbooking/src/models/search_response.dart';
import 'package:flightbooking/src/utils/constants.dart';
import 'package:flightbooking/src/utils/object_factory.dart';
import 'package:flightbooking/src/utils/urls.dart';
import 'package:flightbooking/src/utils/util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';

class PaymentWebView extends StatefulWidget {
  final bool roundTripStatus;
  final int dataIndex, departStop, returnStop;
  final SearchResponse searchResponse;

  PaymentWebView(
      {this.roundTripStatus,
      this.dataIndex,
      this.searchResponse,
      this.returnStop,
      this.departStop});

  @override
  _PaymentWebViewState createState() => _PaymentWebViewState();
}

class _PaymentWebViewState extends State<PaymentWebView> {
  InAppWebViewController webView;
  String url;
  double progress = 0;
  int position;

  @override
  void initState() {
    setState(() {
      url = Urls.bookingUrl +
          "token=" +
          widget.searchResponse.data[widget.dataIndex].bookingToken +
          "&currency=" +
          ((ObjectFactory().hiveBox.hiveGet(key: "currency_name")) != null
              ? (ObjectFactory()
                  .hiveBox
                  .hiveGet(key: "currency_name")
                  .split(" ")
                  .first)
              : ("USD")) +
          "&locale=en" +
          "&passengers=" +
          ObjectFactory().hiveBox.hiveGetInt(key: "adultCount").toString() +
          "-0-" +
          ObjectFactory().hiveBox.hiveGetInt(key: "childrenCount").toString() +
          "-" +
          ObjectFactory().hiveBox.hiveGetInt(key: "infantCount").toString() +
          "&price=" +
          widget.searchResponse.data[widget.dataIndex].price.toString()+
      "affilid=jasudeeplinkdemo";
      position = 1;
    });
    super.initState();
  }

  Future<void> _backwardApp(BuildContext context) async {
    bool status = await webView.canGoBack();
    if (status == true) {
      FirebaseAnalytics()
          .logEvent(name: "back_to_webview_previous_page", parameters: null);
      webView.goBack();
      return Future.value(false);
    } else {
      FirebaseAnalytics()
          .logEvent(name: "back_to_home_screen_from_webview", parameters: null);
      // Navigator.pop(context);
      showAlert(context, "Exit Payment");
    }
  }

  @override
  Widget build(BuildContext context) {
    FirebaseAnalytics().logEvent(name: "webview_page", parameters: null);
    return WillPopScope(
      onWillPop: () => _backwardApp(context),
      child: SafeArea(
        top: true,
        child: Scaffold(
          resizeToAvoidBottomPadding: false,
          body: IndexedStack(
            index: position,
            children: [
              SafeArea(
                child: InAppWebView(
                  initialUrl: url,
                  initialHeaders: {},
                  initialOptions: InAppWebViewGroupOptions(
                      crossPlatform: InAppWebViewOptions(
                    debuggingEnabled: true,
                  )),
                  onWebViewCreated: (InAppWebViewController controller) {
                    webView = controller;
                  },
                  onLoadStart: (InAppWebViewController controller, String url) {
                    print(url);
                    setState(() {
                      this.url = url;
                    });
                  },
                  onLoadStop:
                      (InAppWebViewController controller, String url) async {
                    setState(() {
                      this.url = url;
                      position = 0;
                    });
                  },
                  onProgressChanged:
                      (InAppWebViewController controller, int progress) {
                    setState(() {
                      this.progress = progress / 100;
                    });
                  },
                ),
              ),
              Container(
                  child: Center(
                      child: new CircularPercentIndicator(
                radius: 120.0,
                lineWidth: 13.0,
                animation: true,
                percent:
                    progress == 0 ? (progress / 1) : ((progress - 0.1) / 1),
                animateFromLastPercent: true,
                center: new Text(
                  progress == 0
                      ? "0.0"
                      : "${((progress * 100) - 10.0).toString()}%",
                  style: new TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20.0,
                      color: Constants.kitGradients[4]),
                ),
                footer: new Text(
                  'loading' + '...',
                  style: new TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 17.0,
                      color: Constants.kitGradients[4]),
                ),
                circularStrokeCap: CircularStrokeCap.round,
                progressColor: Constants.kitGradients[14],
              ))),
            ],
          ),
        ),
      ),
    );
  }

}
