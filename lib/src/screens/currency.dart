import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flightbooking/hive/hive.dart';
import 'package:flightbooking/localization/locale.dart';
import 'package:flightbooking/src/utils/constants.dart';
import 'package:flightbooking/src/utils/object_factory.dart';
import 'package:flightbooking/src/utils/util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import 'home_screen.dart';

class Currency extends StatefulWidget {
  @override
  _CurrencyState createState() => _CurrencyState();
}

class _CurrencyState extends State<Currency> {
  TextEditingController _controller = TextEditingController();
  List<String> currencies = [
    "AED United Arab Emirates Dirham (AED)",
    "ALL Albanian Lek (ALL)",
    "AMD Armenian Dram (AMD)",
    "ARS Argentine Peso (ARS)",
    "AUD Australian Dollar (A\u0024)",
    "AZN Azerbaijani Manat (AZN)",
    "BAM Bosnia-Herzegovina Convertible Mark (BAM)",
    "BDT Bangladeshi Taka (BDT)",
    "BHD Bahraini Dinar (BHD)",
    "BGN Bulgarian Lev (BGN)",
    "BRL Brazil Real (R\u0024)",
    "CAD Canadian Dollar (C\u0024)",
    "CLP Chilean Peso (CLP)",
    "COP Colombian Peso (COP)",
    "CHF Swiss Francs (CHF)",
    "CNY Chinese yuan (CN\u00a5)",
    "CZK Czech koruna (CZK)",
    "HRK Croatian Kuna (HRK)",
    "DKK Danish Krone (DKK)",
    "DZD Algerian Dinar (DZD)",
    "EGP Egyptian Pound (EGP)",
    "GBP British Pound (£)",
    "GEL Georgian Lari (GEL)",
    "GHS Ghanaian Cedi (GHS)",
    "HTG Haitian Gourde (HTG)",
    "EUR Euro (\u20AC)",
    "HUF Hungarian Forint (HUF)",
    "HKD Hong Kong Dollar (HK\u0024)",
    "IQD Iraqi Dinar (IQD)",
    "IRR Iranian Rial (IRR)",
    "JOD Jordanian Dinar (JOD)",
    "ISK Iceland Krona (ISK)",
    "INR Indian Rupee (\u20B9)",
    "IDR Indonesian Rupiah (IDR)",
    "ILS Israeli New Shekel (₪)",
    "JPY Japanese Yen (\u00a5)",
    "KES Kenyan Shilling (KES)",
    "KGS Kyrgystani Som (KGS)",
    "KWD Kuwaiti Dinar (KWD)",
    "KZT Kazakhstani Tenge (KZT)",
    "LKR Sri Lankan Rupee (LKR)",
    "LYD Libyan Dinar (LYD)",
    "MNT Mongolian Tugrik (MNT)",
    "MUR Mauritian Rupee (MUR)",
    "MXN Mexican Peso (MX\u0024)",
    "KRW South Korean Won (\u20a9)",
    "LTL Lita (LTL)",
    "MYR Malaysian Ringgit (MYR)",
    "MZN Mozambican Metical (MZN)",
    "NGN Nigerian Naira (NGN)",
    "NOK Norwegian Krone (NOK)",
    "NPR Nepalese Rupee (NPR)",
    "NZD New Zealand Dollar (NZ\u0024)",
    "OMR Omani Rial (OMR)",
    "PEN Peruvian Sol (PEN)",
    "PHP Philippine Piso (PHP)",
    "PKR Pakistani Rupee (PKR)",
    "PLN Polish Zloty (PLN)",
    "QAR Qatari Rial (QAR)",
    "RON Romanian New Lei (RON)",
    "RUB Russian Rouble (RUB)",
    "RSD Serbian Dinar (RSD)",
    "SAR Saudi riyal (SAR)",
    "SGD Singapore Dollar (SGD)",
    "SEK Swedish Krona (SEK)",
    "TJS Tajikistani Somoni (TJS)",
    "TND Tunisian Dinar (TND)",
    "THB Thai Baht (THB)",
    "TRY Turkish Lira (TL)",
    "TWD Taiwan dollar (NT\u0024)",
    "UAH Ukrainian Hryvnia (UAH)",
    "USD U.S. Dollar (\u0024)",
    "UZS Uzbekistani Som (UZS)",
    "VND Vietnamese Dong (₫)",
    "XOF West African CFA Franc (CFA)",
    "ZAR South African Rand (ZAR)",
    "ZMW Zambian Kwacha (ZMW)"
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Constants.kitGradients[3],
      body: Column(
        children: [
          Container(
            width: screenWidth(context, dividedBy: 1),
            height: screenHeight(context, dividedBy: 5),
            color: Constants.kitGradients[14],
            child: Column(
              children: [
                SizedBox(
                  height: screenHeight(context, dividedBy: 15),
                ),
                Row(
                  children: [
                    SizedBox(
                      width: screenWidth(context, dividedBy: 20),
                    ),
                    GestureDetector(
                        onTap: () {
                          Navigator.pushAndRemoveUntil(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => HomeScreen()),
                              (route) => false);
                        },
                        child: Container(
                            width: screenWidth(context, dividedBy: 10),
                            height: screenHeight(context, dividedBy: 26),
                            child: Icon(
                              Icons.clear,
                              color: Constants.kitGradients[3],
                            ))),
                    SizedBox(
                      width: screenWidth(context, dividedBy: 50),
                    ),
                    RichText(
                      text: TextSpan(
                        text: getTranslated(context, "Currency"),
                        style: TextStyle(
                            fontFamily: 'Roboto',
                            color: Constants.kitGradients[3],
                            fontSize: 20,
                            fontWeight: FontWeight.w300,
                            fontStyle: FontStyle.normal),
                        children: <TextSpan>[
                          TextSpan(
                            text:   (ObjectFactory()
                                .hiveBox
                                .hiveGet(key: "currency_name")
                                !=
                                null
                                ? (" ("+
                                (ObjectFactory()
                                .hiveBox
                                .hiveGet(key: "currency_name")
                                .split(" ")
                                .first)
                                +")"
                            )
                                : ("USD")),
                            style: TextStyle(
                                fontFamily: 'Roboto',
                                color: Constants.kitGradients[3],
                                fontSize: 16,
                                fontWeight: FontWeight.w300,
                                fontStyle: FontStyle.normal),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 60),
                ),
                Container(
                  width: screenWidth(context, dividedBy: 1.1),
                  height: screenHeight(context, dividedBy: 15),
                  decoration: BoxDecoration(
                      color: Constants.kitGradients[3],
                      borderRadius: BorderRadius.circular(5)),
                  child: Padding(
                    padding: EdgeInsets.only(left: 10.0, right: 10.0),
                    child: Row(
                      children: [
                        Container(
                          width: screenWidth(context, dividedBy: 1.3),
                          child: TextField(
                            maxLines: 1,
                            decoration: InputDecoration(
                              hintText: getTranslated(context, "Search_currency"),
                              hintStyle: TextStyle(
                                  color: Constants.kitGradients[21],
                                  fontStyle: FontStyle.normal,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 14.0,
                                  fontFamily: 'Roboto'),
                              border: InputBorder.none,
                            ),
                            controller: _controller,
                          ),
                        ),
                        GestureDetector(
                          child: Icon(Icons.clear,
                              color: Constants.kitGradients[14]),
                          onTap: () {
                            _controller.clear();
                          },
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
          Expanded(
              child: ListView.builder(
                  itemCount: currencies.length,
                  itemBuilder: (BuildContext ctxt, int Index) {
                    return GestureDetector(
                      child: Container(
                        width: screenWidth(context, dividedBy: 1),
                        height: screenHeight(context, dividedBy: 12),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Row(
                              children: [
                                SizedBox(
                                  width: screenWidth(context, dividedBy: 30),
                                ),
                                Container(
                                  width: screenWidth(context,dividedBy: 11),
                                  margin: const EdgeInsets.all(5),
                                  padding: const EdgeInsets.all(0),
                                  decoration: BoxDecoration(
                                      border: Border.all(color: Constants.kitGradients[14])),
                                  child: Center(
                                    child: Text(
                                      currencies[Index].split(' ').first,
                                      style: TextStyle(
                                        color: Constants.kitGradients[14],
                                        fontSize: screenWidth(context, dividedBy: 30),
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: screenWidth(context, dividedBy: 50),
                                ),
                                Text(
                                  currencies[Index].substring(4,currencies[Index].length),
                                  style: TextStyle(
                                      fontFamily: 'ProximaNova',
                                      fontStyle: FontStyle.normal,
                                      fontWeight: FontWeight.w700,
                                      fontSize: 14,
                                      color: Constants.kitGradients[22]),
                                ),
                              ],
                            ),
                            Container(
                              height: 0.5,
                              width: screenWidth(context, dividedBy: 1),
                              color: Constants.kitGradients[21],
                            )
                          ],
                        ),
                      ),
                      onTap: (){
                        FirebaseAnalytics().logEvent(name: "selected_currency", parameters:{"currency":currencies[Index].toString()});
                        ObjectFactory().hiveBox.hivePut(key: "currency_name",value: currencies[Index]);
                        Navigator.pushAndRemoveUntil(
                            context,
                            MaterialPageRoute(builder: (context) => HomeScreen()),
                                (route) => false);
                      },
                    );
                  }))
        ],
      ),
    );
  }
}
