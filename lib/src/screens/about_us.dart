import 'package:flightbooking/src/screens/home_screen.dart';
import 'package:flightbooking/src/utils/constants.dart';
import 'package:flightbooking/src/utils/util.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';

class AboutUs extends StatefulWidget {
  @override
  _AboutUsState createState() => _AboutUsState();
}

class _AboutUsState extends State<AboutUs> {
  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        statusBarColor: Constants.kitGradients[3],
      ),
      child: Scaffold(
        backgroundColor: Constants.kitGradients[3],
        body: Stack(
          children: [
            Positioned(
              top: 0.0,
              left: 0.0,
              right: 0.0,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: screenHeight(context, dividedBy: 47),
                  ),
                  IconButton(
                      color: Constants.kitGradients[3],
                      icon: Icon(
                        Icons.clear,
                        color: Constants.kitGradients[32],
                      ),
                      onPressed: () {
                        Navigator.pushAndRemoveUntil(
                            context,
                            MaterialPageRoute(builder: (context) => HomeScreen()),
                                (route) => false);
                      }),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 45),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SvgPicture.asset(
                            'assets/images/about_us_icon1.svg',
                            width: screenWidth(context, dividedBy: 1.5),
                            height: screenHeight(context, dividedBy: 11),
                          ),
                          SizedBox(
                            height: screenHeight(context, dividedBy: 67),
                          ),
                          Text(
                            'cheap flights',
                            style: TextStyle(
                                fontSize: 16.0,
                                fontWeight: FontWeight.w400,
                                color: Constants.kitGradients[28],
                                fontFamily: 'ModernaFamily'),
                          ),
                        ],
                      )
                    ],
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 17),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        left: screenWidth(context, dividedBy: 20)),
                    child: Text(
                      'About us',
                      style: TextStyle(
                          fontSize: 24.0,
                          fontWeight: FontWeight.w700,
                          color: Constants.kitGradients[28],
                          fontFamily: 'ProximaNova'),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        left: screenWidth(context, dividedBy: 20)),
                    child: Container(
                      width: screenWidth(context, dividedBy: 15),
                      height: 5.0,
                      color: Constants.kitGradients[40],
                    ),
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 35),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        left: screenWidth(context, dividedBy: 20),
                        right: screenWidth(context, dividedBy: 35)),
                    child: Container(
                      width: screenWidth(context, dividedBy: 1),
                      child: Text(
                        Constants.ABOUT_US_NOTE,
                        overflow: TextOverflow.visible,
                        style: TextStyle(
                            fontSize: 12.0,
                            fontWeight: FontWeight.w700,
                            color: Constants.kitGradients[22],
                            fontFamily: 'ProximaNova'),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Positioned(
              bottom: 10.0,
              left: 0.0,
              right: 0.0,
              child: Container(
                width: screenWidth(context, dividedBy: 1),
                height: screenHeight(context, dividedBy: 3),
                child: SvgPicture.asset(
                  'assets/images/about_us_icon2.svg',
                  fit: BoxFit.fill,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
