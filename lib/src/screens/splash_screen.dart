import 'dart:async';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flightbooking/localization/locale.dart';
import 'package:flightbooking/src/utils/constants.dart';
import 'package:flightbooking/src/utils/util.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shimmer/shimmer.dart';
import 'home_screen.dart';

class Splash extends StatefulWidget {
  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> with SingleTickerProviderStateMixin {
  Timer _timerControl;


  void startTimer(){
    _timerControl = Timer.periodic(const Duration(seconds: 3),(timer){
      _timerControl.cancel();
      Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context)=>HomeScreen()), (route) => false);
    });
  }
  @override
  void initState() {
    startTimer();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    FirebaseAnalytics().logEvent(name: "Splash_Screen", parameters: null);
    return AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle(
          statusBarColor: Colors.white,
        ),
        child: Scaffold(
            backgroundColor: Constants.kitGradients[3],
            body: Builder(
                builder: (context) => SafeArea(
                    top: true,
                    left: true,
                    bottom: true,
                    child: Stack(
                      children: [
                        Positioned(
                          top: screenHeight(context, dividedBy: 4),
                          left: screenWidth(context, dividedBy: 5),
                          child: Image.asset(
                            "assets/images/richie.png",
                            // fit: BoxFit.fill,
                          ),
                        ),
                        Positioned(
                          top: screenHeight(context, dividedBy: 3.5),
                          right: screenWidth(context, dividedBy: 125),
                          child: Image.asset(
                            "assets/images/susy.png",
                            // fit: BoxFit.fill,
                          ),
                        ),
                        Positioned(
                          top: screenHeight(context, dividedBy: 1.5),
                          left: screenWidth(context, dividedBy: 4),
                          child: Shimmer.fromColors(
                              child: Text(
                                getTranslated(context, "cheap_flights"),
                                style: TextStyle(
                                    fontFamily: 'ProximaNova',
                                    fontSize: 30,
                                    fontWeight: FontWeight.w300,
                                    fontStyle: FontStyle.normal),
                              ),
                              baseColor: Constants.kitGradients[16],
                              highlightColor: Constants.kitGradients[17]),
                        ),
                      ],
                    )))));
  }
}
