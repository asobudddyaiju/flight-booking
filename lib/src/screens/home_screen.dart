import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flightbooking/localization/locale.dart';
import 'package:flightbooking/src/screens/result.dart';
import 'package:flightbooking/src/utils/constants.dart';
import 'package:flightbooking/src/utils/get_month_alpha.dart';
import 'package:flightbooking/src/utils/get_week_days_for_result_app_bar.dart';
import 'package:flightbooking/src/utils/util.dart';
import 'package:flightbooking/src/widgets/home_date_selection.dart';
import 'package:flightbooking/src/widgets/home_drawer.dart';
import 'package:flightbooking/src/widgets/home_screen_bottom_sheet.dart';
import 'package:flightbooking/src/widgets/home_screen_destination.dart';
import 'package:flightbooking/src/widgets/home_screen_passenger_selection.dart';
import 'package:flightbooking/src/widgets/home_screen_slide_switch.dart';
import 'package:flightbooking/src/widgets/home_screen_submit_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../utils/object_factory.dart';
import '../utils/util.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();
  String destinationFrom;
  String destinationFromName;
  String destinationTo;
  String destinationToName;
  String departureDate;
  String departureMonth;
  String departureDateNumber;
  String departureDateOneWay;
  String departureMonthOneWay;
  String departureDateNumberOneWay;
  String arrivalDate;
  String arrivalMonth;
  String arrivalDateNumber;
  String arrivalYear;
  String departureYear;
  String departureYearOneWay;
  String classSelected;
  String classForApiCall;
  int classSelect = 0;
  int passengerCount = 0;
  int adultCount = 0;
  int childrenCount = 0;
  int infantCount = 0;
  int listViewCount;
  bool roundTripStatus;
  String roundTrip;

  @override
  void initState() {
    destinationToCheck();
    destinationFromCheck();
    checkInDateCheck();
    checkOutDateCheck();
    roundTripStatusCheck();
    classSelectedCheck();
    passengerCountCheck();
    ObjectFactory().hiveBox.putSortByForFilter(value: "price");
    ObjectFactory().hiveBox.putStopsNumbers(value: null);
    ObjectFactory().hiveBox.putFlightDuration(value: null);
    ObjectFactory().hiveBox.putDepartureRangeStart(value: null);
    ObjectFactory().hiveBox.putReturnRangeEnd(value: null);
    ObjectFactory().hiveBox.putDepartureFromTime(value: null);
    ObjectFactory().hiveBox.putDepartureToTime(value: null);
    ObjectFactory().hiveBox.putDepartureToTime(value: null);
    ObjectFactory().hiveBox.putDepartureFromTime(value: null);
    ObjectFactory().hiveBox.putDepartureFlightTimeFrom(value: null);
    ObjectFactory().hiveBox.putDepartureFlightTimeTo(value: null);
    ObjectFactory().hiveBox.putReturnFlightTimeTo(value: null);
    ObjectFactory().hiveBox.putReturnFlightTimeTo(value: null);
    ObjectFactory().hiveBox.putDepartureFlightTime1(value: null);
    ObjectFactory().hiveBox.putDepartureFlightTime2(value: null);
    ObjectFactory().hiveBox.putDepartureFlightTime3(value: null);
    ObjectFactory().hiveBox.putDepartureFlightTime4(value: null);
    ObjectFactory().hiveBox.putReturnFlightTime1(value: null);
    ObjectFactory().hiveBox.putReturnFlightTime2(value: null);
    ObjectFactory().hiveBox.putReturnFlightTime3(value: null);
    ObjectFactory().hiveBox.putReturnFlightTime4(value: null);
    ObjectFactory().hiveBox.putDepartureFlightTimeFrom(value: null);
    ObjectFactory().hiveBox.putDepartureFlightTimeTo(value: null);
    ObjectFactory().hiveBox.putReturnFlightTimeFrom(value: null);
    ObjectFactory().hiveBox.putReturnFlightTimeTo(value: null);
    ObjectFactory().hiveBox.putAirlineFilterSearch(value: null);
    ObjectFactory().hiveBox.putSelectAllButtonStatus(value: null);
    ObjectFactory().hiveBox.putSelectedPriceRangeLowerForFilter(value: null);
    ObjectFactory().hiveBox.putSelectedPriceRangeUpperForFilter(value: null);
    ObjectFactory().hiveBox.putPriceTotalArrayMin(value: null);
    ObjectFactory().hiveBox.putPriceTotalArrayMax(value: null);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    FirebaseAnalytics().logEvent(name: "home_screen", parameters: null);
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        statusBarColor: Constants.kitGradients[14],
      ),
      child: Scaffold(
        resizeToAvoidBottomPadding: false,
        backgroundColor: Colors.white.withOpacity(0.98),
        key: _globalKey,
        drawer: HomeDrawer(),
        body: Stack(
          children: [
            Positioned(
              top: 0,
              left: 0,
              child: Container(
                width: screenWidth(context, dividedBy: 1),
                height: screenHeight(context, dividedBy: 2.2),
                color: Constants.kitGradients[14],
              ),
            ),
            Positioned(
                top: screenHeight(context, dividedBy: 16),
                left: screenWidth(context, dividedBy: 25),
                child: GestureDetector(
                    onTap: () {
                      FirebaseAnalytics().logEvent(
                          name: "clicked_drawer_from_home_screen",
                          parameters: null);
                      _globalKey.currentState.openDrawer();
                    },
                    child: Image.asset(
                      'assets/icons/home_screen_menu_button.png',
                      height: screenHeight(context, dividedBy: 20),
                      width: screenWidth(context, dividedBy: 10),
                    ))),
            Positioned(
              top: screenHeight(context, dividedBy: 8.0),
              left: screenWidth(context, dividedBy: 4.5),
              child: Image.asset(
                'assets/images/homescreen_image.png',
                height: screenHeight(context, dividedBy: 3.5),
                width: screenWidth(context, dividedBy: 1.5),
              ),
            ),
            Positioned(
              top: screenHeight(context, dividedBy: 2.45),
              left: screenWidth(context, dividedBy: 12),
              child: Container(
                width: screenWidth(context, dividedBy: 1.2),
                height: screenHeight(context, dividedBy: 12),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(31.0),
                  color: Constants.kitGradients[3],
                ),
                child: Padding(
                    padding: const EdgeInsets.all(2.0),
                    child: HomeScreenSlideSwitch(
                      roundTripStatus: roundTripStatus,
                      tripValue: (value) {
                        setState(() {
                          roundTripStatus = value;
                          roundTrip = value == false ? "oneway" : "round";
                        });
                      },
                    )),
              ),
            ),
            Positioned(
              top: screenHeight(context, dividedBy: 2.0),
              left: screenWidth(context, dividedBy: 12),
              child: HomeScreenDestination(
                destinationFrom: destinationFrom,
                destinationToName: destinationToName,
                destinationTo: destinationTo,
                destinationFromName: destinationFromName,
                fromOnChanged: (value) {
                  setState(() {
                    destinationFrom = value;
                  });
                },
                fromNameOnChanged: (value) {
                  setState(() {
                    destinationFromName = value;
                  });
                },
                toOnChanged: (value) {
                  setState(() {
                    destinationTo = value;
                  });
                },
                toNameOnChanged: (value) {
                  setState(() {
                    destinationToName = value;
                  });
                },
              ),
            ),
            Positioned(
                top: screenHeight(context, dividedBy: 1.55),
                left: screenWidth(context, dividedBy: 12),
                child: DateSelectionWidget(
                  roundTripStatus: roundTripStatus,
                  oneWayDepartDate: departureDateOneWay,
                  oneWayDepartMonth: departureMonthOneWay,
                  oneWayDepartYear: departureYearOneWay,
                  departDate: departureDate,
                  departureMonth: departureMonth,
                  departYear: departureYear,
                  arrivalYear: arrivalYear,
                  arrivalDate: arrivalDate,
                  arrivalMonth: arrivalMonth,
                )),
            Positioned(
              top: screenHeight(context, dividedBy: 1.3),
              left: screenWidth(context, dividedBy: 12),
              child: HomeScreenPassengerSelection(
                travelClass: classSelected,
                numberOfPassengers: passengerCount > 0
                    ? passengerCount.toString() +
                        ' ' +
                        getTranslated(context,
                            (passengerCount == 1 ? "passenger" : "passengers"))
                    : "select passengers",
                onTap: () {
                  FirebaseAnalytics()
                      .logEvent(name: "opened_bottom_sheet", parameters: null);
                  displayBottomSheet(context);
                },
              ),
            ),
            Positioned(
                top: screenHeight(context, dividedBy: 1.1),
                left: screenWidth(context, dividedBy: 12),
                child: HomeScreenSubmitButton(
                  buttonName: getTranslated(context, "Search"),
                  onTap: () async {
                    FirebaseAnalytics().logEvent(
                        name: "clicked_search_button", parameters: null);
                    bool result = await DataConnectionChecker().hasConnection;
                    if (result == true &&
                        ObjectFactory()
                                .hiveBox
                                .hiveGet(key: 'destinationFrom') !=
                            null &&
                        ObjectFactory().hiveBox.hiveGet(key: 'destinationTo') !=
                            null) {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ResultPage(
                                  flyFrom: destinationFrom,
                                  flyTo: destinationTo,
                                  destinationFromName: destinationFromName,
                                  destinationToName: destinationToName,
                                  routeFrom: destinationFromName,
                                  routeTo: destinationToName,
                                  dateFrom: roundTripStatus == true
                                      ? departureDateNumber +
                                          "/" +
                                          (ObjectFactory().hiveBox.hiveGet(
                                                      key: 'checkInMonth') !=
                                                  null
                                              ? ObjectFactory()
                                                  .hiveBox
                                                  .hiveGet(key: 'checkInMonth')
                                                  .toString()
                                              : DateTime.now()
                                                  .month
                                                  .toString()) +
                                          "/" +
                                          departureYear
                                      : departureDateNumberOneWay +
                                          "/" +
                                          (ObjectFactory()
                                                      .hiveBox
                                                      .getOneWayMonth() !=
                                                  null
                                              ? ObjectFactory()
                                                  .hiveBox
                                                  .getOneWayMonth()
                                              : DateTime.now()
                                                  .month
                                                  .toString()) +
                                          "/" +
                                          departureYearOneWay,
                                  dateTo: roundTripStatus == true
                                      ? departureDateNumber +
                                          "/" +
                                          (ObjectFactory().hiveBox.hiveGet(
                                                      key: 'checkInMonth') !=
                                                  null
                                              ? ObjectFactory()
                                                  .hiveBox
                                                  .hiveGet(key: 'checkInMonth')
                                                  .toString()
                                              : DateTime.now()
                                                  .month
                                                  .toString()) +
                                          "/" +
                                          departureYear
                                      : departureDateNumberOneWay +
                                          "/" +
                                          (ObjectFactory()
                                                      .hiveBox
                                                      .getOneWayMonth() !=
                                                  null
                                              ? ObjectFactory()
                                                  .hiveBox
                                                  .getOneWayMonth()
                                                  .toString()
                                              : DateTime.now()
                                                  .month
                                                  .toString()) +
                                          "/" +
                                          departureYearOneWay,
                                  flightType: roundTrip,
                                  adults: adultCount,
                                  children: childrenCount,
                                  infants: infantCount,
                                  selectedCabins: classForApiCall,
                                  returnFrom: arrivalDateNumber +
                                      "/" +
                                      (ObjectFactory().hiveBox.hiveGet(
                                                  key: 'checkOutMonth') !=
                                              null
                                          ? ObjectFactory()
                                              .hiveBox
                                              .hiveGet(key: 'checkOutMonth')
                                              .toString()
                                          : DateTime.now().month.toString()) +
                                      "/" +
                                      arrivalYear,
                                  returnTo: arrivalDateNumber +
                                      "/" +
                                      (ObjectFactory().hiveBox.hiveGet(
                                                  key: 'checkOutMonth') !=
                                              null
                                          ? ObjectFactory()
                                              .hiveBox
                                              .hiveGet(key: 'checkOutMonth')
                                              .toString()
                                          : DateTime.now().month.toString()) +
                                      "/" +
                                      arrivalYear,
                                  passengerCount: passengerCount.toString(),
                                  roundTripStatus: roundTripStatus,
                                  departureDate: roundTripStatus == true
                                      ? departureDate
                                      : departureDateOneWay,
                                  dateTimePassengersDayInAlphabet: getTranslated(
                                      context,
                                      roundTripStatus == true
                                          ? (ObjectFactory().hiveBox.hiveGet(
                                                          key:
                                                              'checkInDateWeekDay') !=
                                                      null
                                                  ? GetDayForResultAppBar()
                                                      .getWeekDayForResultAppBar(
                                                          ObjectFactory()
                                                              .hiveBox
                                                              .hiveGet(
                                                                  key:
                                                                      'checkInDateWeekDay')
                                                              .toString())
                                                      .toString()
                                                  : GetDayForResultAppBar()
                                                      .getWeekDayForResultAppBar(
                                                          DateTime.now()
                                                              .weekday
                                                              .toString()))
                                              .toString()
                                          : (ObjectFactory().hiveBox.getOneWayDate() != null
                                                  ? GetDayForResultAppBar()
                                                      .getWeekDayForResultAppBar(
                                                          ObjectFactory()
                                                              .hiveBox
                                                              .getOneWayDate()
                                                              .toString())
                                                      .toString()
                                                  : GetDayForResultAppBar()
                                                      .getWeekDayForResultAppBar(
                                                          DateTime.now().weekday.toString()))
                                              .toString()),
                                  dateTimePassengersDayReturnInAlphabet: getTranslated(
                                      context,
                                      (ObjectFactory().hiveBox.hiveGet(
                                                      key:
                                                          'checkOutDateWeekDay') !=
                                                  null
                                              ? GetDayForResultAppBar()
                                                  .getWeekDayForResultAppBar(
                                                      ObjectFactory()
                                                          .hiveBox
                                                          .hiveGet(
                                                              key:
                                                                  'checkOutDateWeekDay')
                                                          .toString())
                                                  .toString()
                                              : GetDayForResultAppBar()
                                                  .getWeekDayForResultAppBar(
                                                      DateTime.now()
                                                          .add(
                                                              Duration(days: 1))
                                                          .weekday
                                                          .toString()))
                                          .toString()),
                                  dateTimePassengers: roundTripStatus == true
                                      ? departureDate.toString() +
                                          " " +
                                          departureMonth
                                      : departureDateOneWay.toString() +
                                          " " +
                                          departureMonthOneWay,
                                  dateTimeReturnPassengers:
                                      arrivalDate.toString() +
                                          " " +
                                          arrivalMonth,
                                  departureCode: destinationFrom,
                                  destinationCode: destinationTo,
                                )),
                      );
                    } else {
                      result == false
                          ? FirebaseAnalytics().logEvent(
                              name: "no_internet_connection", parameters: null)
                          : FirebaseAnalytics().logEvent(
                              name: "empty_departure_arrival_fields",
                              parameters: null);
                      showToastLong(result == false
                          ? getTranslated(
                              context, "Check_your_Internet_Connection")
                          : getTranslated(
                              context, "Please_choose_departure_and_arrival"));
                    }
                  },
                ))
          ],
        ),
      ),
    );
  }

  void checkInDateCheck() {
    if (ObjectFactory().hiveBox.getOneWayDay() != null &&
        (DateTime.parse(ObjectFactory().hiveBox.getOneWayYear() +
                "-" +
                getFormattedToTwoDigitMonthAndDate(
                    ObjectFactory().hiveBox.getOneWayMonth()) +
                "-" +
                getFormattedToTwoDigitMonthAndDate(
                    ObjectFactory().hiveBox.getOneWayDay()) +
                "T" +
                getFormattedToTwoDigitMonthAndDate(
                    DateTime.now().hour.toString()) +
                ":" +
                getFormattedToTwoDigitMonthAndDate(
                    DateTime.now().minute.toString()) +
                ":" +
                "00.000Z")
            .isAfter(DateTime.now()))) {
      setState(() {
        departureDateOneWay = ObjectFactory().hiveBox.getOneWayDay();
        departureMonthOneWay = GetMonthName()
            .getMonthAlpha(ObjectFactory().hiveBox.getOneWayMonth());
        departureYearOneWay = ObjectFactory().hiveBox.getOneWayYear();
        departureDateNumberOneWay = ObjectFactory().hiveBox.getOneWayDay();
      });
    } else {
      departureDateOneWay = DateTime.now().day.toString();
      departureMonthOneWay =
          GetMonthName().getMonthAlpha(DateTime.now().month.toString());
      departureYearOneWay = DateTime.now().year.toString();
      departureDateNumberOneWay = DateTime.now().day.toString();
    }
    if (ObjectFactory().hiveBox.hiveGet(key: 'checkInDate') != null &&
        (DateTime.parse(ObjectFactory().hiveBox.hiveGet(key: 'checkInYear') +
                "-" +
                getFormattedToTwoDigitMonthAndDate(
                    ObjectFactory().hiveBox.hiveGet(key: 'checkInMonth')) +
                "-" +
                getFormattedToTwoDigitMonthAndDate(
                    ObjectFactory().hiveBox.hiveGet(key: 'checkInDate')) +
                "T" +
                getFormattedToTwoDigitMonthAndDate(
                    DateTime.now().hour.toString()) +
                ":" +
                getFormattedToTwoDigitMonthAndDate(
                    DateTime.now().minute.toString()) +
                ":" +
                "00.000Z")
            .isAfter(DateTime.now()))) {
      setState(() {
        departureDate = ObjectFactory().hiveBox.hiveGet(key: 'checkInDate');

        departureMonth = GetMonthName().getMonthAlpha(
            ObjectFactory().hiveBox.hiveGet(key: 'checkInMonth'));

        departureYear = ObjectFactory().hiveBox.hiveGet(key: 'checkInYear');

        departureDateNumber =
            ObjectFactory().hiveBox.hiveGet(key: 'checkInDate');
      });
    } else {
      setState(() {
        departureDate = DateTime.now().day.toString();
        departureMonth =
            GetMonthName().getMonthAlpha(DateTime.now().month.toString());
        departureYear = DateTime.now().year.toString();
        departureDateNumber = DateTime.now().day.toString();
      });
    }
    //print("departure date is "+departureDateNumber.toString()+"/"+ObjectFactory().hiveBox.hiveGet(key: 'checkInMonth').toString()+"/"+departureYear.toString());
  }

  void checkOutDateCheck() {
    if (ObjectFactory().hiveBox.hiveGet(key: 'checkOutDate') != null &&
        (DateTime.parse(ObjectFactory().hiveBox.hiveGet(key: 'checkOutYear') +
                "-" +
                getFormattedToTwoDigitMonthAndDate(
                    ObjectFactory().hiveBox.hiveGet(key: 'checkOutMonth')) +
                "-" +
                getFormattedToTwoDigitMonthAndDate(
                    ObjectFactory().hiveBox.hiveGet(key: 'checkOutDate')) +
                "T" +
                getFormattedToTwoDigitMonthAndDate(
                    DateTime.now().hour.toString()) +
                ":" +
                getFormattedToTwoDigitMonthAndDate(
                    DateTime.now().minute.toString()) +
                ":" +
                "00.000Z")
            .isAfter(DateTime.now()))) {
      setState(() {
        arrivalDate = ObjectFactory().hiveBox.hiveGet(key: 'checkOutDate');
        arrivalMonth = GetMonthName()
            .getMonthAlpha(
                ObjectFactory().hiveBox.hiveGet(key: 'checkOutMonth'))
            .toString();
        arrivalYear = ObjectFactory().hiveBox.hiveGet(key: 'checkOutYear');
        arrivalDateNumber =
            ObjectFactory().hiveBox.hiveGet(key: 'checkOutDate');
      });
      if (ObjectFactory().hiveBox.hiveGet(key: 'roundTripStatus') == "true") {
        setState(() {
          arrivalDate = ObjectFactory().hiveBox.hiveGet(key: 'checkOutDate');
          arrivalMonth = GetMonthName()
              .getMonthAlpha(
                  ObjectFactory().hiveBox.hiveGet(key: 'checkOutMonth'))
              .toString();
          arrivalYear = ObjectFactory().hiveBox.hiveGet(key: 'checkOutYear');
          arrivalDateNumber =
              ObjectFactory().hiveBox.hiveGet(key: 'checkOutDate');
        });
      }
    } else {
      setState(() {
        arrivalDate = DateTime.now().add(Duration(days: 1)).day.toString();
        arrivalMonth =
            GetMonthName().getMonthAlpha(DateTime.now().month.toString());
        arrivalYear = DateTime.now().year.toString();
        arrivalDateNumber =
            DateTime.now().add(Duration(days: 1)).day.toString();
      });
    }
  }

  void roundTripStatusCheck() {
    if (ObjectFactory().hiveBox.hiveGet(key: 'roundTripStatus') != null) {
      if (ObjectFactory().hiveBox.hiveGet(key: 'roundTripStatus') == "true") {
        setState(() {
          roundTripStatus = true;
        });
      } else if (ObjectFactory().hiveBox.hiveGet(key: 'roundTripStatus') ==
          "false") {
        setState(() {
          roundTripStatus = false;
        });
      }
    } else {
      setState(() {
        roundTripStatus = true;
        ObjectFactory().hiveBox.hivePut(key: 'roundTripStatus', value: "true");
      });
    }
    roundTripStatus == true
        ? setState(() {
            roundTrip = "round";
          })
        : setState(() {
            roundTrip = "oneway";
          });
  }

  void destinationFromCheck() {
    if (ObjectFactory().hiveBox.hiveGet(key: 'destinationFrom') != null) {
      setState(() {
        destinationFrom =
            ObjectFactory().hiveBox.hiveGet(key: 'destinationFrom');
        destinationFromName =
            ObjectFactory().hiveBox.hiveGet(key: 'destinationFromName');
      });
    } else {
      ObjectFactory().hiveBox.hivePut(key: 'destinationFrom', value: "NYC");
      ObjectFactory()
          .hiveBox
          .hivePut(key: 'destinationFromName', value: "New York");
      setState(() {
        destinationFrom =
            ObjectFactory().hiveBox.hiveGet(key: 'destinationFrom');
        destinationFromName =
            ObjectFactory().hiveBox.hiveGet(key: 'destinationFromName');
      });
    }
  }

  void destinationToCheck() {
    if (ObjectFactory().hiveBox.hiveGet(key: 'destinationTo') != null) {
      setState(() {
        destinationTo = ObjectFactory().hiveBox.hiveGet(key: 'destinationTo');
        destinationToName =
            ObjectFactory().hiveBox.hiveGet(key: 'destinationToName');
      });
    } else {
      ObjectFactory().hiveBox.hivePut(key: 'destinationTo', value: "LON");
      ObjectFactory()
          .hiveBox
          .hivePut(key: 'destinationToName', value: "London");

      setState(() {
        destinationTo = ObjectFactory().hiveBox.hiveGet(key: 'destinationTo');
        destinationToName =
            ObjectFactory().hiveBox.hiveGet(key: 'destinationToName');
      });
    }
  }

  void displayBottomSheet(context) {
    showModalBottomSheet(
      isScrollControlled: true,
      isDismissible: true,
      context: context,
      builder: (BuildContext context) {
        return HomeBottomSheet();
      },
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(25.0),
      ),
    );
  }

  void passengerCountCheck() {
    ObjectFactory().hiveBox.hiveGetInt(key: "adultCount") != null
        ? setState(() {
            adultCount = ObjectFactory().hiveBox.hiveGetInt(key: "adultCount");
          })
        : setState(() {
            adultCount = 1;
          });
    ObjectFactory().hiveBox.hiveGetInt(key: "childrenCount") != null
        ? setState(() {
            childrenCount =
                ObjectFactory().hiveBox.hiveGetInt(key: "childrenCount");
          })
        : setState(() {
            childrenCount = 0;
          });
    ObjectFactory().hiveBox.hiveGetInt(key: "infantCount") != null
        ? setState(() {
            infantCount =
                ObjectFactory().hiveBox.hiveGetInt(key: "infantCount");
          })
        : setState(() {
            infantCount = 0;
          });

    int sum = adultCount + childrenCount + infantCount;
    setState(() {
      passengerCount = sum;
    });
    ObjectFactory()
        .hiveBox
        .hivePutInt(key: "totalPassengerCount", value: passengerCount);
    print('Total passenger count is ' +
        ObjectFactory()
            .hiveBox
            .hiveGetInt(key: "totalPassengerCount")
            .toString());
  }

  void classSelectedCheck() {
    if ((ObjectFactory().hiveBox.hiveGetBool(key: "economy") == null) &&
        (ObjectFactory().hiveBox.hiveGetBool(key: "premiumEconomy") == null) &&
        (ObjectFactory().hiveBox.hiveGetBool(key: "business") == null) &&
        (ObjectFactory().hiveBox.hiveGetBool(key: "firstClass") == null)) {
      classSelect = 1;
      classSelected = "Economy";
      classForApiCall = "M";
    }

    if (ObjectFactory().hiveBox.hiveGetBool(key: "economy") == true) {
      setState(() {
        classSelect = 1;
        classSelected = "Economy";
        classForApiCall = "M";
      });
    } else if (ObjectFactory().hiveBox.hiveGetBool(key: "premiumEconomy") ==
        true) {
      setState(() {
        classSelect = 2;
        classSelected = "Premium Economy";
        classForApiCall = "W";
      });
    } else if (ObjectFactory().hiveBox.hiveGetBool(key: "business") == true) {
      setState(() {
        classSelect = 3;
        classSelected = "Business";
        classForApiCall = "C";
      });
    } else if (ObjectFactory().hiveBox.hiveGetBool(key: "firstClass") == true) {
      setState(() {
        classSelect = 4;
        classSelected = "First class";
        classForApiCall = "F";
      });
    }
  }
}
