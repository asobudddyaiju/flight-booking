import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flightbooking/localization/locale.dart';
import 'package:flightbooking/src/models/search_response.dart';
import 'package:flightbooking/src/utils/constants.dart';
import 'package:flightbooking/src/utils/currency_symbol.dart';
import 'package:flightbooking/src/utils/flight_names.dart';
import 'package:flightbooking/src/utils/get_month_alpha.dart';
import 'package:flightbooking/src/utils/get_weekdays.dart';
import 'package:flightbooking/src/utils/object_factory.dart';
import 'package:flightbooking/src/utils/urls.dart';
import 'package:flightbooking/src/utils/util.dart';
import 'package:flightbooking/src/widgets/details_app_bar_widget.dart';
import 'package:flightbooking/src/widgets/details_bottom_nav.dart';
import 'package:flightbooking/src/widgets/details_list_view1.dart';
import 'package:flightbooking/src/widgets/details_list_view2.dart';
import 'package:flightbooking/src/widgets/details_widget4.dart';
import 'package:flightbooking/src/widgets/details_widget_1.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:flutter_custom_tabs/flutter_custom_tabs.dart';



class DetailsPage extends StatefulWidget {
  final bool roundTripStatus;
  final int dataIndex, departStop, returnStop;
  final SearchResponse searchResponse;


  DetailsPage(
      {this.roundTripStatus,
      this.dataIndex,
      this.searchResponse,
      this.returnStop,
      this.departStop});

  @override
  _DetailsPageState createState() => _DetailsPageState();
}

class _DetailsPageState extends State<DetailsPage> {
  ScrollController _controller = ScrollController();
  String roundTrip;
  bool roundTripStatus, noticeStatus, bookingButtonLoading;
  int length;
  int departureStopsDetail = 0, returnStopsDetail = 0;
  List<int> returnStopsArrayDetail = [];
  List<int> departStopsArrayDetail = [];
  String url;

  @override
  void initState() {

    setState(() {
      length = lengthDepart();
      bookingButtonLoading = false;

      url = Urls.bookingUrl +
          "token=" +
          widget.searchResponse.data[widget.dataIndex].bookingToken +
          "&currency=" +
          ((ObjectFactory().hiveBox.hiveGet(key: "currency_name")) != null
              ? (ObjectFactory()
              .hiveBox
              .hiveGet(key: "currency_name")
              .split(" ")
              .first)
              : ("USD")) +
          "&locale=en" +
          "&passengers=" +
          ObjectFactory().hiveBox.hiveGetInt(key: "adultCount").toString() +
          "-0-" +
          ObjectFactory().hiveBox.hiveGetInt(key: "childrenCount").toString() +
          "-" +
          ObjectFactory().hiveBox.hiveGetInt(key: "infantCount").toString() +
          "&price=" +
          widget.searchResponse.data[widget.dataIndex].price.toString();

    });


    super.initState();
  }

  @override
  void dispose() {
   setState(() {
     bookingButtonLoading = false;
   });
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    FirebaseAnalytics().logEvent(name: "details_page", parameters: null);
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        statusBarColor: Constants.kitGradients[14],
      ),
      child: Scaffold(
        resizeToAvoidBottomPadding: false,
        backgroundColor: Constants.kitGradients[27],
        appBar: AppBar(
          leading: Container(),
          backgroundColor: Constants.kitGradients[3],
          actions: [
            DetailsAppBarWidget(
              title: getTranslated(context, "Flight_Details"),
            ),
          ],
        ),
        bottomNavigationBar: DetailsBottomNavigation(
          bookingButtonLoading: bookingButtonLoading,
          title: "Price",
          price:  (ObjectFactory()
              .hiveBox
              .hiveGet(key: "currency_name")
              !=
              null
              ? (getCurrencySymbol(
              code: ObjectFactory()
                  .hiveBox
                  .hiveGet(key: "currency_name")
                  .split(" ")
                  .first))
              : ("\$"))+
                  widget.searchResponse.data[widget.dataIndex].price.toString(),
          onTap: () {
            setState(() {
              bookingButtonLoading = true;
            });
            FirebaseAnalytics()
                .logEvent(name: "proceed_to_payment", parameters: {
              "price":
                  widget.searchResponse.data[widget.dataIndex].price.toString()
            });

            _launchURL(context);
            //_launchURL();

            // Navigator.push(
            //     context,
            //     MaterialPageRoute(
            //         builder: (context) => PaymentWebView(
            //               dataIndex: widget.dataIndex,
            //               searchResponse: widget.searchResponse,
            //             )));

          },
        ),
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 7.0),
          child: SingleChildScrollView(
            controller: _controller,
            scrollDirection: Axis.vertical,
            child: Column(
              children: [
                SizedBox(
                  height: screenHeight(context, dividedBy: 105),
                ),
                DetailsWidget1(
                  route: widget.searchResponse.data[widget.dataIndex].cityFrom +
                      " - " +
                      widget.searchResponse.data[widget.dataIndex].cityTo,
                  duration: getTranslated(context, "Flight_duration_:") +
                      getDurationInHour(widget.searchResponse
                          .data[widget.dataIndex].duration.departure) +
                      ", " +
                      (widget.departStop == 0?("Direct Flight"):(widget.departStop.toString()+
                          " " +
                          getTranslated(context, "stop:"))) ,
                  backgroundColor: Constants.kitGradients[14],
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 105),
                ),
                ListView.builder(
                    itemCount: listViewCount(widget.departStop),
                    scrollDirection: Axis.vertical,
                    controller: _controller,
                    shrinkWrap: true,
                    itemBuilder: (BuildContext context, index) {
                      return DetailsListView1(
                        title1: getFlightName(
                            code: widget.searchResponse.data[widget.dataIndex]
                                .route[index].airline),
                        title2: getTranslated(context, "Operated_by_:") +
                            widget.searchResponse.data[widget.dataIndex]
                                .route[index].operatingCarrier,
                        title3: getTranslated(context, "Flight:") +
                            widget.searchResponse.data[widget.dataIndex]
                                .route[index].airline +
                            " - " +
                            widget.searchResponse.data[widget.dataIndex]
                                .route[index].flightNo
                                .toString(),
                        toTime: getTime(widget.searchResponse
                            .data[widget.dataIndex].route[index].localArrival
                            .toString()),
                        // toAirport: 'Bangalore International Airport (BLR)',
                        toAirport: widget.searchResponse.data[widget.dataIndex]
                            .route[index].cityTo,
                        toAirportCode: widget.searchResponse
                            .data[widget.dataIndex].route[index].flyTo,
                        fromAirport: widget.searchResponse
                            .data[widget.dataIndex].route[index].cityFrom,
                        fromAirportCode: widget.searchResponse
                            .data[widget.dataIndex].route[index].flyFrom,
                        fromDate: GetDay().getWeekDay(widget
                                .searchResponse
                                .data[widget.dataIndex]
                                .route[index]
                                .localDeparture
                                .weekday
                                .toString())[1] +
                            " " +
                            GetMonthName().getMonthAlpha(widget
                                .searchResponse
                                .data[widget.dataIndex]
                                .route[index]
                                .localDeparture
                                .month
                                .toString()) +
                            ", " +
                            widget.searchResponse.data[widget.dataIndex]
                                .route[index].localDeparture.day
                                .toString(),
                        toDate: GetDay().getWeekDay(widget
                                .searchResponse
                                .data[widget.dataIndex]
                                .route[index]
                                .localArrival
                                .weekday
                                .toString())[1] +
                            " " +
                            GetMonthName().getMonthAlpha(widget
                                .searchResponse
                                .data[widget.dataIndex]
                                .route[index]
                                .localArrival
                                .month
                                .toString()) +
                            ", " +
                            widget.searchResponse.data[widget.dataIndex]
                                .route[index].localArrival.day
                                .toString(),
                        toPlace: widget.searchResponse.data[widget.dataIndex]
                            .route[index].cityTo,
                        fromTime: getTime(widget.searchResponse
                            .data[widget.dataIndex].route[index].localDeparture
                            .toString()),
                        fromPlace: widget.searchResponse.data[widget.dataIndex]
                            .route[index].cityFrom,
                        layOverDuration: ((index + 1) <
                                widget.searchResponse.data[widget.dataIndex]
                                    .route.length)
                            ? getDifferenceInTime([
                                widget.searchResponse.data[widget.dataIndex]
                                    .route[index].localArrival,
                                widget.searchResponse.data[widget.dataIndex]
                                    .route[index + 1].localDeparture,
                              ])
                            : "dontShowWidget",
                        layOverPlace: widget.searchResponse
                                    .data[widget.dataIndex].route[index].cityTo
                                    .toString() !=
                                widget.searchResponse.data[widget.dataIndex]
                                    .cityTo
                                    .toString()
                            ? widget.searchResponse.data[widget.dataIndex]
                                .route[index].cityTo
                            : "dontShowWidget",
                        airline: widget.searchResponse.data[widget.dataIndex]
                            .route[index].airline,
                      );
                    }),
                SizedBox(
                  height: screenHeight(context, dividedBy: 105),
                ),
                widget.roundTripStatus == true
                    ? DetailsWidget1(
                        route: widget
                                .searchResponse.data[widget.dataIndex].cityTo +
                            " - " +
                            widget
                                .searchResponse.data[widget.dataIndex].cityFrom,
                        duration: getTranslated(context, "Flight_duration_:") +
                            getDurationInHour(widget
                                .searchResponse
                                .data[widget.dataIndex]
                                .duration
                                .durationReturn) +
                            ", " +
                            (widget.returnStop == 0?("Direct Flight"):(widget.returnStop.toString()+
                                getTranslated(context, "stop:"))) ,
                        backgroundColor: Constants.kitGradients[28],
                      )
                    : Container(),
                SizedBox(
                  height: screenHeight(context, dividedBy: 105),
                ),
                widget.roundTripStatus == true
                    ? ListView.builder(
                        itemCount: listViewCount(widget.returnStop),
                        scrollDirection: Axis.vertical,
                        controller: _controller,
                        shrinkWrap: true,
                        itemBuilder: (BuildContext context, index) {
                          return DetailsListView2(
                            roundTripStatus: widget.roundTripStatus,
                            title1: getFlightName(
                                code: widget
                                    .searchResponse
                                    .data[widget.dataIndex]
                                    .route[index + length]
                                    .airline),
                            title2: getTranslated(context, "Operated_by_:") +
                                widget.searchResponse.data[widget.dataIndex]
                                    .route[index + length].operatingCarrier,
                            title3: getTranslated(context, "Flight:") +
                                widget.searchResponse.data[widget.dataIndex]
                                    .route[index + length].airline +
                                " - " +
                                widget.searchResponse.data[widget.dataIndex]
                                    .route[index + length].flightNo
                                    .toString(),
                            toTime: getTime(widget
                                .searchResponse
                                .data[widget.dataIndex]
                                .route[index + length]
                                .localArrival
                                .toString()),
                            fromAirport: widget
                                .searchResponse
                                .data[widget.dataIndex]
                                .route[index + length]
                                .cityFrom,
                            fromAirportCode: widget
                                .searchResponse
                                .data[widget.dataIndex]
                                .route[index + length]
                                .flyFrom,
                            toAirport: widget
                                .searchResponse
                                .data[widget.dataIndex]
                                .route[index + length]
                                .cityTo,
                            toAirportCode: widget
                                .searchResponse
                                .data[widget.dataIndex]
                                .route[index + length]
                                .flyTo,
                            toPlace: widget
                                .searchResponse
                                .data[widget.dataIndex]
                                .route[index + length]
                                .cityTo,
                            fromTime: getTime(widget
                                .searchResponse
                                .data[widget.dataIndex]
                                .route[index + length]
                                .localDeparture
                                .toString()),
                            fromDate: GetDay().getWeekDay(widget
                                    .searchResponse
                                    .data[widget.dataIndex]
                                    .route[index + lengthDepart()]
                                    .localDeparture
                                    .weekday
                                    .toString())[1] +
                                " " +
                                GetMonthName().getMonthAlpha(widget
                                    .searchResponse
                                    .data[widget.dataIndex]
                                    .route[index + lengthDepart()]
                                    .localDeparture
                                    .month
                                    .toString()) +
                                ", " +
                                widget
                                    .searchResponse
                                    .data[widget.dataIndex]
                                    .route[index + lengthDepart()]
                                    .localDeparture
                                    .day
                                    .toString(),
                            toDate: GetDay().getWeekDay(widget
                                    .searchResponse
                                    .data[widget.dataIndex]
                                    .route[index + lengthDepart()]
                                    .localArrival
                                    .weekday
                                    .toString())[1] +
                                " " +
                                GetMonthName().getMonthAlpha(widget
                                    .searchResponse
                                    .data[widget.dataIndex]
                                    .route[index + lengthDepart()]
                                    .localArrival
                                    .month
                                    .toString()) +
                                ", " +
                                widget
                                    .searchResponse
                                    .data[widget.dataIndex]
                                    .route[index + lengthDepart()]
                                    .localArrival
                                    .day
                                    .toString(),
                            fromPlace: widget
                                .searchResponse
                                .data[widget.dataIndex]
                                .route[index + length]
                                .cityFrom,
                            layOverDuration: ((index + lengthDepart() + 1) <
                                    widget.searchResponse.data[widget.dataIndex]
                                        .route.length)
                                ? getDifferenceInTime([
                                    widget
                                        .searchResponse
                                        .data[widget.dataIndex]
                                        .route[index + lengthDepart()]
                                        .localArrival,
                                    widget
                                        .searchResponse
                                        .data[widget.dataIndex]
                                        .route[((index + lengthDepart()) + 1)]
                                        .localDeparture
                                  ])
                                : "dontShowWidget",
                            layOverPlace: (index + lengthDepart()) <=
                                    widget.searchResponse.data[widget.dataIndex]
                                        .route.length
                                ? widget.searchResponse.data[widget.dataIndex]
                                    .route[index + lengthDepart()].cityTo
                                : "",
                            airline: widget
                                .searchResponse
                                .data[widget.dataIndex]
                                .route[index + length]
                                .airline,
                          );
                        },
                      )
                    : Container(),
                SizedBox(
                  height: screenHeight(context, dividedBy: 105),
                ),
                noticeStatus == true
                    ? DetailsWidget4(
                        notice:
                            'You are arriving at La guardia but returning from Newark International airport',
                      )
                    : Container(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  int lengthDepart() {
    int length = 0;
    for (int x = 0;
        x < widget.searchResponse.data[widget.dataIndex].route.length;
        x++) {
      if (widget.searchResponse.data[widget.dataIndex].route[x].routeReturn ==
          0) {
        length = length + 1;
      }
    }

    return length;
  }

  void _launchURL(BuildContext context) async {
    try {
      await launch(
        url,
        option: new CustomTabsOption(
          toolbarColor: Theme.of(context).primaryColor,
          enableDefaultShare: true,
          enableUrlBarHiding: true,
          showPageTitle: true,
          animation: new CustomTabsAnimation.slideIn(),
          // or user defined animation.
          //   animation: new CustomTabsAnimation(
          //   startEnter: 'slide_up',
          //   startExit: 'android:anim/fade_out',
          //   endEnter: 'android:anim/fade_in',
          //   endExit: 'slide_down',
          // ),
          extraCustomTabs: <String>[
            // ref. https://play.google.com/store/apps/details?id=org.mozilla.firefox
            'org.mozilla.firefox',
            // ref. https://play.google.com/store/apps/details?id=com.microsoft.emmx
            'com.microsoft.emmx',
          ],
        ),
      );
      setState(() {
        bookingButtonLoading = false;
      });
    } catch (e) {
      // An exception is thrown if browser app is not installed on Android device.
      debugPrint(e.toString());
      setState(() {
        bookingButtonLoading = false;
      });
      showToast("Something went wrong!");
    }
  }

}
