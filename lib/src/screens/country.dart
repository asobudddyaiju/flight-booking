import 'package:flightbooking/localization/locale.dart';
import 'package:flightbooking/src/screens/home_screen.dart';
import 'package:flightbooking/src/utils/constants.dart';
import 'package:flightbooking/src/utils/util.dart';
import 'package:flutter/material.dart';

class Country extends StatefulWidget {
  @override
  _CountryState createState() => _CountryState();
}

class _CountryState extends State<Country> {
  TextEditingController _controller = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Constants.kitGradients[3],
      body: Column(
        children: [
          Container(
            width: screenWidth(context, dividedBy: 1),
            height: screenHeight(context, dividedBy: 5),
            color: Constants.kitGradients[14],
            child: Column(
              children: [
                SizedBox(
                  height: screenHeight(context, dividedBy: 15),
                ),
                Row(
                  children: [
                    SizedBox(
                      width: screenWidth(context, dividedBy: 20),
                    ),
                    GestureDetector(
                        onTap: () {
                          Navigator.pushAndRemoveUntil(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => HomeScreen()),
                              (route) => false);
                        },
                        child: Container(
                          width: screenWidth(context, dividedBy: 10),
                          height: screenHeight(context, dividedBy: 26),
                          child: Icon(
                            Icons.clear,
                            color: Constants.kitGradients[3],
                          ),
                        )),
                    SizedBox(
                      width: screenWidth(context, dividedBy: 50),
                    ),
                    RichText(
                      text: TextSpan(
                        text: getTranslated(context, "Country"),
                        style: TextStyle(
                            fontFamily: 'Roboto',
                            color: Constants.kitGradients[3],
                            fontSize: 20,
                            fontWeight: FontWeight.w300,
                            fontStyle: FontStyle.normal),
                        children: <TextSpan>[
                          TextSpan(
                            text: '(United States)',
                            style: TextStyle(
                                fontFamily: 'Roboto',
                                color: Constants.kitGradients[3],
                                fontSize: 16,
                                fontWeight: FontWeight.w300,
                                fontStyle: FontStyle.normal),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 60),
                ),
                Container(
                  width: screenWidth(context, dividedBy: 1.1),
                  height: screenHeight(context, dividedBy: 15),
                  decoration: BoxDecoration(
                      color: Constants.kitGradients[3],
                      borderRadius: BorderRadius.circular(5)),
                  child: Padding(
                    padding: EdgeInsets.only(left: 10.0, right: 10.0),
                    child: Row(
                      children: [
                        Container(
                          width: screenWidth(context, dividedBy: 1.3),
                          child: TextField(
                            maxLines: 1,
                            decoration: InputDecoration(
                              hintText: getTranslated(context, "Search_country"),
                              hintStyle: TextStyle(
                                  color: Constants.kitGradients[21],
                                  fontStyle: FontStyle.normal,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 14.0,
                                  fontFamily: 'Roboto'),
                              border: InputBorder.none,
                            ),
                            controller: _controller,
                          ),
                        ),
                        GestureDetector(
                          child: Icon(Icons.clear,
                              color: Constants.kitGradients[14]),
                          onTap: () {
                            _controller.clear();
                          },
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
          Expanded(
              child: ListView.builder(
                  itemCount: 10,
                  itemBuilder: (BuildContext ctxt, int Index) {
                    return Container(
                      width: screenWidth(context, dividedBy: 1),
                      height: screenHeight(context, dividedBy: 12),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Row(
                            children: [
                              SizedBox(
                                width: screenWidth(context, dividedBy: 30),
                              ),
                              Text(
                                'United States',
                                style: TextStyle(
                                    fontFamily: 'ProximaNova',
                                    fontStyle: FontStyle.normal,
                                    fontWeight: FontWeight.w700,
                                    fontSize: 14,
                                    color: Constants.kitGradients[22]),
                              ),
                            ],
                          ),
                          Container(
                            height: 0.5,
                            width: screenWidth(context, dividedBy: 1),
                            color: Constants.kitGradients[21],
                          )
                        ],
                      ),
                    );
                  }))
        ],
      ),
    );
  }
}
