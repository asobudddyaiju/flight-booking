import 'package:flightbooking/src/models/nearby_airport_request.dart';
import 'package:flightbooking/src/models/search_request.dart';
import 'package:flightbooking/src/models/state.dart';
import 'package:flightbooking/src/resources/api_providers/user_api_provider.dart';

import '../../../models/state.dart';

/// Repository is an intermediary class between network and data
class Repository {
  final userApiProvider = UserApiProvider();

  Future<State> sampleCall() => userApiProvider.sampleCall();

  Future<State> destination({String searchKey}) => userApiProvider.destinationFromCall(searchKey: searchKey);
  Future<State> searchFlights({SearchRequest searchReq})=> userApiProvider.searchFlights(searchReq: searchReq);
  Future<State> searchNearbyAirports({NearByAirportsRequest nearByAirportsRequest})=> userApiProvider.searchNearbyAirports(nearByAirportsRequest: nearByAirportsRequest);
}
