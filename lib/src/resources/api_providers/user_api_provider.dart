
import 'package:flightbooking/src/models/destination_from_response.dart';
import 'package:flightbooking/src/models/nearby_airport_request.dart';
import 'package:flightbooking/src/models/nearby_airport_response.dart';
import 'package:flightbooking/src/models/search_request.dart';
import 'package:flightbooking/src/models/notification_list_response.dart';
import 'package:flightbooking/src/models/search_response.dart';
import 'package:flightbooking/src/models/state.dart';
import 'package:flightbooking/src/utils/object_factory.dart';

import '../../models/state.dart';
import '../../utils/object_factory.dart';
import '../../utils/object_factory.dart';

class UserApiProvider {
  
  Future<State> sampleCall() async {
    final response = await ObjectFactory().apiClient.sampleApiCall();
    print(response.toString());
    if (response.statusCode == 200) {
      return State<SampleResponseModel>.success(
          SampleResponseModel.fromJson(response.data));
    } else {
      return null;
    }
  }
  
  Future<State> destinationFromCall({String searchKey}) async {
    final response = await ObjectFactory().apiClient.destinationFrom(searchKey: searchKey);
    print(response.data.toString());
    if(response.statusCode == 200){
      return State<DestinationFromResponse>.success(DestinationFromResponse.fromJson(response.data));
    }else{
      return null;
    }
  }
  Future<State> searchFlights({SearchRequest searchReq}) async {
    final response = await ObjectFactory().apiClient.searchFlights(searchReq: searchReq);
    if(response.statusCode == 200 || true){
      return State<SearchResponse>.success(SearchResponse.fromJson(response.data),);
    }else{
      return null;
    }

  }
  Future<State> searchNearbyAirports({NearByAirportsRequest nearByAirportsRequest}) async {
    final response = await ObjectFactory().apiClient.searchNearByAirports(nearByAirportsRequest: nearByAirportsRequest);
    if(response.statusCode == 200 || true){
      return State<NearByAirportsResponse>.success(NearByAirportsResponse.fromJson(response.data),);
    }else{
      return null;
    }

  }
}
