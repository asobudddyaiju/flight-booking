
import 'package:flightbooking/src/models/currency.dart';
import 'package:flutter/foundation.dart';

String getCurrencySymbol({String code}) {
 String symbol;
  for (int i = 0; i < currencyJson.data.length; i++) {
    if(currencyJson.data[i].currency == code){
     symbol = currencyJson.data[i].symbol;
    }
  }
  return symbol;
}



CurrencyModel currencyJson = CurrencyModel.fromJson({"data":[
  {
    "currency": "USD",
    "symbol": "\$"
  },
  {
    "currency": "EUR",
    "symbol": "\€"
  },
  {
    "currency": "INR",
    "symbol": "\₹"
  },
  {
    "currency": "AED",
    "symbol": "AED"
  },
  {
    "currency": "ALL",
    "symbol": "ALL"
  },
  {
    "currency": "AMD",
    "symbol": "AMD"
  },
  {
    "currency": "ARS",
    "symbol": "ARS"
  },
  {
    "currency": "AUD",
    "symbol": "\$"
  },
  {
    "currency": "AZN",
    "symbol": "AZN"
  },
  {
    "currency": "BAM",
    "symbol": "BAM"
  },
  {
    "currency": "BDT",
    "symbol": "BDT"
  },
  {
    "currency": "BGN",
    "symbol": "BGN"
  },
  {
    "currency": "BHD",
    "symbol": "BHD"
  },
  {
    "currency": "BRL",
    "symbol": "\$"
  },
  {
    "currency": "CAD",
    "symbol": "\$"
  },
  {
    "currency": "CHF",
    "symbol": "CHF"
  },
  {
    "currency": "CLP",
    "symbol": "CLP"
  },
  {
    "currency": "CNY",
    "symbol": "\¥"
  },
  {
    "currency": "COP",
    "symbol": "COP"
  },
  {
    "currency": "CZK",
    "symbol": "CZK"
  },
  {
    "currency": "DKK",
    "symbol": "DKK"
  },
  {
    "currency": "DZD",
    "symbol": "DZD"
  },
  {
    "currency": "EGP",
    "symbol": "EGP"
  },
  {
    "currency": "GBP",
    "symbol": "\£"
  },
  {
    "currency": "GEL",
    "symbol": "GEL"
  },
  {
    "currency": "GHS",
    "symbol": "GHS"
  },
  {
    "currency": "HKD",
    "symbol": "\$"
  },
  {
    "currency": "HRK",
    "symbol": "HRK"
  },
  {
    "currency": "HTG",
    "symbol": "HTG"
  },
  {
    "currency": "HUF",
    "symbol": "HUF"
  },
  {
    "currency": "IDR",
    "symbol": "IDR"
  },
  {
    "currency": "ILS",
    "symbol": "\₪"
  },
  {
    "currency": "IQD",
    "symbol": "IQD"
  },
  {
    "currency": "IRR",
    "symbol": "IRR"
  },
  {
    "currency": "ISK",
    "symbol": "ISK"
  },
  {
    "currency": "JOD",
    "symbol": "JOD"
  },
  {
    "currency": "JPY",
    "symbol": "\¥"
  },
  {
    "currency": "KES",
    "symbol": "KES"
  },
  {
    "currency": "KGS",
    "symbol": "KGS"
  },
  {
    "currency": "KRW",
    "symbol": "\₩"
  },
  {
    "currency": "KWD",
    "symbol": "KWD"
  },
  {
    "currency": "KZT",
    "symbol": "KZT"
  },
  {
    "currency": "LKR",
    "symbol": "LKR"
  },
  {
    "currency": "LYD",
    "symbol": "LYD"
  },
  {
    "currency": "MNT",
    "symbol": "MNT"
  },
  {
    "currency": "MUR",
    "symbol": "MUR"
  },
  {
    "currency": "MXN",
    "symbol": "\$"
  },
  {
    "currency": "MYR",
    "symbol": "MYR"
  },
  {
    "currency": "MZN",
    "symbol": "MZN"
  },
  {
    "currency": "NGN",
    "symbol": "NGN"
  },
  {
    "currency": "NOK",
    "symbol": "NOK"
  },
  {
    "currency": "NPR",
    "symbol": "NPR"
  },
  {
    "currency": "NZD",
    "symbol": "\$"
  },
  {
    "currency": "OMR",
    "symbol": "OMR"
  },
  {
    "currency": "PEN",
    "symbol": "PEN"
  },
  {
    "currency": "PHP",
    "symbol": "PHP"
  },
  {
    "currency": "PKR",
    "symbol": "PKR"
  },
  {
    "currency": "PLN",
    "symbol": "PLN"
  },
  {
    "currency": "QAR",
    "symbol": "QAR"
  },
  {
    "currency": "RON",
    "symbol": "RON"
  },
  {
    "currency": "RSD",
    "symbol": "RSD"
  },
  {
    "currency": "RUB",
    "symbol": "RUB"
  },
  {
    "currency": "SAR",
    "symbol": "SAR"
  },
  {
    "currency": "SEK",
    "symbol": "SEK"
  },
  {
    "currency": "SGD",
    "symbol": "SGD"
  },
  {
    "currency": "THB",
    "symbol": "THB"
  },
  {
    "currency": "TJS",
    "symbol": "TJS"
  },
  {
    "currency": "TND",
    "symbol": "TND"
  },
  {
    "currency": "TRY",
    "symbol": "TRY"
  },
  {
    "currency": "TWD",
    "symbol": "\$"
  },
  {
    "currency": "UAH",
    "symbol": "UAH"
  },
  {
    "currency": "UZS",
    "symbol": "UZS"
  },
  {
    "currency": "VND",
    "symbol": "\₫"
  },
  {
    "currency": "XOF",
    "symbol": "XOF"
  },
  {
    "currency": "ZAR",
    "symbol": "ZAR"
  },
  {
    "currency": "ZMW",
    "symbol": "ZMW"
  }
]});

