import 'dart:async';

import 'package:dio/dio.dart';
import 'package:flightbooking/src/models/nearby_airport_request.dart';
import 'package:flightbooking/src/models/search_request.dart';
import 'package:flightbooking/src/utils/urls.dart';
import 'package:flutter/material.dart';

class ApiClient {
  ApiClient() {
    initClient();
  }

//for api client testing only
  ApiClient.test({@required this.dio});

  Dio dio;

  initClient() async {
    dio = Dio();

    dio.interceptors.add(InterceptorsWrapper(
      onRequest: (RequestOptions reqOptions) {
        return reqOptions;
      },
      onError: (DioError dioError) {
        return dioError.response;
      },
    ));
  }

  /// Verify User
  Future<Response> sampleApiCall() {
    return dio.post("", data: {});
  }

  Future<Response> destinationFrom({String searchKey}) {
    dio.options.headers.addAll({
      "accept": "application/json",
      "apikey": "uKThuAKehWwXgNrKcb2ht3opcFQSke2W"
    });
    print(Urls.destinationUrl +
        "locale=en-US&location_types=airport&location_types=city&limit=10&active_only=true&term=$searchKey");
    return dio.get(Urls.destinationUrl +
        "locale=en-US&location_types=airport&location_types=city&limit=10&active_only=true&term=$searchKey");
  }

  Future<Response> searchFlights({SearchRequest searchReq}) {
    dio.options.headers.addAll({
      "accept": "application/json",
      "apikey": "s0IWICmAwHE5hnV_O7soRd9qHSGeNCsh"
    });
    print(Urls.flightsSearchUrl +
        "fly_from=${searchReq.flyFrom}&fly_to=${searchReq.flyTo}&dateFrom=${searchReq.dateFrom}&dateTo=${searchReq.dateTo}&curr=${searchReq.curr}&locale=${searchReq.locale}&selected_cabins=${searchReq.selectedCabins}" +
        (searchReq.flightType.toString() == "round"
            ? ("&return_from=${searchReq.returnFrom}&return_to=${searchReq.returnTo}")
            : "") +
        "&adults=${searchReq.adults}&children=${searchReq.children}&infants=${searchReq.infants}&sort=${searchReq.sort}&flight_type=${searchReq.flightType}" +
        (searchReq.selectAirlines.toString() != "noValue"
            ? ("&select_airlines=${searchReq.selectAirlines}")
            : "") +
        (searchReq.selectAirlines.toString() != "noValue"
            ? ("&select_airlines_exclude=${searchReq.selectAirlinesExclude}")
            : "") +
        (searchReq.maxStopovers.toString() == ""
            ? ""
            : ("&max_stopovers=${searchReq.maxStopovers}")) +
        (searchReq.maxFlyDuration == null
            ? ""
            : ("&max_fly_duration=${searchReq.maxFlyDuration}")) +
        (searchReq.dtimeFrom.toString() == ""
            ? ""
            : ("&dtime_from=${searchReq.dtimeFrom}")) +
        (searchReq.dtimeTo.toString() == ""
            ? ""
            : ("&dtime_to=${searchReq.dtimeTo}")) +
        (searchReq.retDtimeFrom.toString() == ""
            ? ""
            : ("&ret_dtime_from=${searchReq.retDtimeFrom}")) +
        (searchReq.retDtimeTo.toString() == ""
            ? ""
            : ("&ret_dtime_to=${searchReq.retDtimeTo}")) +
        (searchReq.priceFrom == null
            ? ""
            : ("&price_from=${searchReq.priceFrom}")) +
        (searchReq.priceTo == null ? "" : ("&price_to=${searchReq.priceTo}")));

    return dio.get(Urls.flightsSearchUrl +
        "fly_from=${searchReq.flyFrom}&fly_to=${searchReq.flyTo}&dateFrom=${searchReq.dateFrom}&dateTo=${searchReq.dateTo}&curr=${searchReq.curr}&locale=${searchReq.locale}&selected_cabins=${searchReq.selectedCabins}" +
        (searchReq.flightType.toString() == "round"
            ? ("&return_from=${searchReq.returnFrom}&return_to=${searchReq.returnTo}")
            : "") +
        "&adults=${searchReq.adults}&children=${searchReq.children}&infants=${searchReq.infants}&sort=${searchReq.sort}&flight_type=${searchReq.flightType}" +
        (searchReq.selectAirlines.toString() != "noValue"
            ? ("&select_airlines=${searchReq.selectAirlines}")
            : "") +
        (searchReq.selectAirlines.toString() != "noValue"
            ? ("&select_airlines_exclude=${searchReq.selectAirlinesExclude}")
            : "") +
        (searchReq.maxStopovers.toString() == ""
            ? ""
            : ("&max_stopovers=${searchReq.maxStopovers}")) +
        (searchReq.maxFlyDuration == null
            ? ""
            : ("&max_fly_duration=${searchReq.maxFlyDuration}")) +
        (searchReq.dtimeFrom.toString() == ""
            ? ""
            : ("&dtime_from=${searchReq.dtimeFrom}")) +
        (searchReq.dtimeTo.toString() == ""
            ? ""
            : ("&dtime_to=${searchReq.dtimeTo}")) +
        (searchReq.retDtimeFrom.toString() == ""
            ? ""
            : ("&ret_dtime_from=${searchReq.retDtimeFrom}")) +
        (searchReq.retDtimeTo.toString() == ""
            ? ""
            : ("&ret_dtime_to=${searchReq.retDtimeTo}")) +
        (searchReq.priceFrom == null
            ? ""
            : ("&price_from=${searchReq.priceFrom}")) +
        (searchReq.priceTo == null ? "" : ("&price_to=${searchReq.priceTo}")));
  }

  Future<Response> searchNearByAirports(
      {NearByAirportsRequest nearByAirportsRequest}) {
    dio.options.headers.addAll({
      "accept": "application/json",
      "apikey": "Cy3dNHq6-RZtP_Y2Q-UimxFB9TxZyMTr"
    });
    print(Urls.nearbySearchUrl +
        "lat=${nearByAirportsRequest.lat}&lon=${nearByAirportsRequest.lon}&radius=${nearByAirportsRequest.radius}&locale=${nearByAirportsRequest.locale}&location_types=${nearByAirportsRequest.locationTypes}&limit=${nearByAirportsRequest.limit}&sort=${nearByAirportsRequest.sort}");

    return dio.get(Urls.nearbySearchUrl +
        "lat=${nearByAirportsRequest.lat}&lon=${nearByAirportsRequest.lon}&radius=${nearByAirportsRequest.radius}&locale=${nearByAirportsRequest.locale}&location_types=${nearByAirportsRequest.locationTypes}&limit=${nearByAirportsRequest.limit}&sort=${nearByAirportsRequest.sort}");
  }
}
