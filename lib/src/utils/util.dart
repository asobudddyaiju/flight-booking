import 'dart:convert';

import 'package:flightbooking/src/utils/constants.dart';
import 'package:flightbooking/src/utils/object_factory.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;

///it contain common functions
class Utils {
  static String capitalize(String s) {
    if (s != null && s.isNotEmpty) {
      return s[0].toUpperCase() + s.substring(1);
    } else {
      return "";
    }
  }
}

Size screenSize(BuildContext context) {
  return MediaQuery.of(context).size;
}

double screenHeight(BuildContext context, {double dividedBy = 1}) {
  return screenSize(context).height / dividedBy;
}

double screenWidth(BuildContext context, {double dividedBy = 1}) {
  return screenSize(context).width / dividedBy;
}

///common toast
void showToast(String msg) {
  Fluttertoast.showToast(
    msg: msg,
    toastLength: Toast.LENGTH_SHORT,
    gravity: ToastGravity.BOTTOM,
  );
}

void showAlert(context, String msg) {
  // flutter defined function
  showDialog(
    context: context,
    builder: (BuildContext context) {
      // return object of type Dialog
      return AlertDialog(
        title: new Text(msg,
            style: TextStyle(
                color: Constants.kitGradients[28],
                fontFamily: 'ProximaNovaBold',
                fontSize: 20.0)),
//        content: new Text("Alert Dialog body"),
        actions: <Widget>[
          // usually buttons at the bottom of the dialog
          new FlatButton(
            child: new Text(
              "Cancel",
              style: TextStyle(
                  fontFamily: 'ProximaNova',
                  color: Constants.kitGradients[14],
                  fontSize: 15),
            ),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          new FlatButton(
            child: new Text(
              "OK",
              style: TextStyle(
                  fontFamily: 'ProximaNova',
                  color: Constants.kitGradients[14],
                  fontSize: 15),
            ),
            onPressed: () {
              Navigator.of(context).pop();
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}

///common toast
void showToastLong(String msg) {
  Fluttertoast.showToast(
    msg: msg,
    toastLength: Toast.LENGTH_LONG,
    gravity: ToastGravity.BOTTOM,
  );
}

void showToastConnection(String msg) {
  Fluttertoast.showToast(
    msg: msg,
    toastLength: Toast.LENGTH_LONG,
    gravity: ToastGravity.BOTTOM,
  );
}

int initialCheckIn(DateTime startDate, DateTime endDate) {
  DateTime now = new DateTime.now();
  DateTime startTime = DateTime(now.year, now.month, now.day);
  DateTime endTime = DateTime(startDate.year, startDate.month, startDate.day);
  int differenceCheckInDays = 0;
  if (startTime == endTime) {
    differenceCheckInDays = 0;
  } else {
    differenceCheckInDays = endTime.difference(startTime).inDays;
  }
  // print("diffin="+ differenceCheckInDays.toString());
  return differenceCheckInDays;
}

int initialCheckOut(DateTime startDate, DateTime endDate) {
  DateTime now = new DateTime.now();
  DateTime startTime = DateTime(now.year, now.month, now.day);
  if (endDate != null) {
    DateTime endTime = DateTime(endDate.year, endDate.month, endDate.day);
    int differenceCheckOutDays;
    if (startTime == endTime) {
      differenceCheckOutDays = null;
    } else {
      differenceCheckOutDays = endTime.difference(startTime).inDays;
    }
    // print("diffout=" + differenceCheckOutDays.toString());
    return differenceCheckOutDays;
  }
}

Future<String> getPublicIP() async {
  try {
    const url = 'https://api.ipify.org';
    var response = await http.get(url);
    String ip = response.body.toString();
    try {
      String url =
          'http://api.ipstack.com/$ip?access_key=d42ac7f928933d400d70d3a95590356b&format=1';
      var response = await http.get(url);
      // print(response.statusCode);
      //print(response.body);
      var data = json.decode(response.body);
      ObjectFactory()
          .hiveBox
          .hivePut(key: "country_name", value: data["country_name"].toString());
      ObjectFactory()
          .hiveBox
          .hivePut(key: "lat", value: data["latitude"].toString());
      ObjectFactory()
          .hiveBox
          .hivePut(key: "lon", value: data["longitude"].toString());
      // Hive.box('room').put('country', data["country_name"].toString());
      ObjectFactory().hiveBox.hivePut(
          key: "currency_name", value: data["currency"]["code"].toString());
      // Hive.box('room').put('currency', data["currency"]["code"].toString());
      // print( 'country='+ ObjectFactory().hiveBox.hiveGet(key:"country_name"));
      // print( 'currency='+ ObjectFactory().hiveBox.hiveGet(key:"currency_name"));
      return null;
    } catch (e) {
      print(e);
      return null;
    }
  } catch (e) {
    print(e);
    return null;
  }
}

String getDurationInHour(int seconds) {
  String duration = (seconds / 3600).toString();
  // print("given seconds is"+seconds.toString() );
  // print("duration in hr is"+duration.toString() );
  String A, B, C, D;
  if (duration.contains(".")) {
    A = duration.split(".").first + "hr";
    B = duration.split(".").last;
    if (B.length >= 2) {
      C = B.substring(0, 2);
    } else {
      C = B;
    }
    D = A + " " + C + "m";
  } else {
    D = duration + "hr";
  }
  return D;
}

int getStops(int index, List array) {
  int x;
  int r;
  x = index > 0 ? (array[index] - array[index - 1]) : array[index];
  {
    r = x - 1;
  }
  return r;
}

String getTime(String dateTime) {
  String x, y;
  x = dateTime.split(" ").last;
  y = x.substring(0, 5);
  return y;
}

String getDifferenceInTime(List<DateTime> dateTimes) {
  DateTime x = dateTimes[0];
  DateTime y = dateTimes[1];
  int z = y.difference(x).inSeconds;
  String z1 = getDurationInHour(z);
  return z1;
}

int listViewCount(int stops) {
  int count = 0;
  count = stops + 1;
  return count;
}

String getFormattedToTwoDigitMonthAndDate(String date) {
  if (date.length == 1) {
    return "0" + date.toString();
  } else {
    return date.toString();
  }
}
