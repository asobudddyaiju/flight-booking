class GetMonthName {
  List<String> monthList = ["Jan","Feb", "Mar", "Apr", "May", "Jun", "Jul","Aug","Sep","Oct","Nov","Dec"];

  String getMonthAlpha(String monthNumber) {
    String monthAlpha;
    switch (monthNumber) {
      case "1":
        {
          monthAlpha = monthList[0];

          break;
        }
      case "2":
        {
          monthAlpha = monthList[1];


          break;
        }
      case "3":
        {
          monthAlpha = monthList[2];


          break;
        }
      case "4":
        {
          monthAlpha = monthList[3];


          break;
        }
      case "5":
        {
          monthAlpha = monthList[4];


          break;
        }
      case "6":
        {
          monthAlpha = monthList[5];


          break;
        }
      case "7":
        {
          monthAlpha = monthList[6];


          break;
        }
      case "8":
        {
          monthAlpha = monthList[7];


          break;
        }
      case "9":
        {
          monthAlpha = monthList[8];


          break;
        }
      case "10":
        {
          monthAlpha = monthList[9];


          break;
        }
      case "11":
        {
          monthAlpha = monthList[10];


          break;
        }
      case "12":
        {
          monthAlpha = monthList[11];


          break;
        }

    }
    return monthAlpha;
  }
}
