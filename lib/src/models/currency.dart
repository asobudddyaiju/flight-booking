// To parse this JSON data, do
//
//     final currencyModel = currencyModelFromJson(jsonString);

import 'dart:convert';

CurrencyModel currencyModelFromJson(String str) => CurrencyModel.fromJson(json.decode(str));

String currencyModelToJson(CurrencyModel data) => json.encode(data.toJson());

class CurrencyModel {
  CurrencyModel({
    this.data,
  });

  final List<Datum> data;

  factory CurrencyModel.fromJson(Map<String, dynamic> json) => CurrencyModel(
    data: json["data"] == null ? null : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "data": data == null ? null : List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class Datum {
  Datum({
    this.currency,
    this.symbol,
  });

  final String currency;
  final String symbol;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    currency: json["currency"] == null ? null : json["currency"],
    symbol: json["symbol"] == null ? null : json["symbol"],
  );

  Map<String, dynamic> toJson() => {
    "currency": currency == null ? null : currency,
    "symbol": symbol == null ? null : symbol,
  };
}
