// To parse this JSON data, do
//
//     final nearByAirportsRequest = nearByAirportsRequestFromJson(jsonString);

import 'dart:convert';

NearByAirportsRequest nearByAirportsRequestFromJson(String str) => NearByAirportsRequest.fromJson(json.decode(str));

String nearByAirportsRequestToJson(NearByAirportsRequest data) => json.encode(data.toJson());

class NearByAirportsRequest {
  NearByAirportsRequest({
    this.lat,
    this.lon,
    this.radius,
    this.locale,
    this.locationTypes,
    this.limit,
    this.sort,
  });

  final String lat;
  final String lon;
  final String radius;
  final String locale;
  final String locationTypes;
  final int limit;
  final String sort;

  factory NearByAirportsRequest.fromJson(Map<String, dynamic> json) => NearByAirportsRequest(
    lat: json["lat"] == null ? null : json["lat"],
    lon: json["lon"] == null ? null : json["lon"],
    radius: json["radius"] == null ? null : json["radius"],
    locale: json["locale"] == null ? null : json["locale"],
    locationTypes: json["location_types"] == null ? null : json["location_types"],
    limit: json["Limit"] == null ? null : json["Limit"],
    sort: json["Sort"] == null ? null : json["Sort"],
  );

  Map<String, dynamic> toJson() => {
    "lat": lat == null ? null : lat,
    "lon": lon == null ? null : lon,
    "radius": radius == null ? null : radius,
    "locale": locale == null ? null : locale,
    "location_types": locationTypes == null ? null : locationTypes,
    "Limit": limit == null ? null : limit,
    "Sort": sort == null ? null : sort,
  };
}
