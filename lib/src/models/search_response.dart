// To parse this JSON data, do
//
//     final searchResponse = searchResponseFromJson(jsonString);

import 'dart:convert';

SearchResponse searchResponseFromJson(String str) =>
    SearchResponse.fromJson(json.decode(str));

String searchResponseToJson(SearchResponse data) => json.encode(data.toJson());

class SearchResponse {
  SearchResponse({
    this.searchId,
    this.data,
    this.connections,
    this.time,
    this.currency,
    this.currencyRate,
    this.fxRate,
    this.refresh,
    this.del,
    this.refTasks,
    this.searchParams,
    this.allStopoverAirports,
    this.allAirlines,
  });

  final String searchId;
  final List<Datum> data;
  final List<dynamic> connections;
  final int time;
  final String currency;
  final double currencyRate;
  final double fxRate;
  final List<dynamic> refresh;
  final int del;
  final List<dynamic> refTasks;
  final SearchParams searchParams;
  final List<dynamic> allStopoverAirports;
  final List<dynamic> allAirlines;

  factory SearchResponse.fromJson(Map<String, dynamic> json) => SearchResponse(
        searchId: json["search_id"] == null ? null : json["search_id"],
        data: json["data"] == null
            ? null
            : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
        connections: json["connections"] == null
            ? null
            : List<dynamic>.from(json["connections"].map((x) => x)),
        time: json["time"] == null ? null : json["time"],
        currency: json["currency"] == null ? null : json["currency"],
        currencyRate:
            json["currency_rate"] == null ? null : json["currency_rate"],
        fxRate: json["fx_rate"] == null ? null : json["fx_rate"],
        refresh: json["refresh"] == null
            ? null
            : List<dynamic>.from(json["refresh"].map((x) => x)),
        del: json["del"] == null ? null : json["del"],
        refTasks: json["ref_tasks"] == null
            ? null
            : List<dynamic>.from(json["ref_tasks"].map((x) => x)),
        searchParams: json["search_params"] == null
            ? null
            : SearchParams.fromJson(json["search_params"]),
        allStopoverAirports: json["all_stopover_airports"] == null
            ? null
            : List<dynamic>.from(json["all_stopover_airports"].map((x) => x)),
        allAirlines: json["all_airlines"] == null
            ? null
            : List<dynamic>.from(json["all_airlines"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "search_id": searchId == null ? null : searchId,
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
        "connections": connections == null
            ? null
            : List<dynamic>.from(connections.map((x) => x)),
        "time": time == null ? null : time,
        "currency": currency == null ? null : currency,
        "currency_rate": currencyRate == null ? null : currencyRate,
        "fx_rate": fxRate == null ? null : fxRate,
        "refresh":
            refresh == null ? null : List<dynamic>.from(refresh.map((x) => x)),
        "del": del == null ? null : del,
        "ref_tasks": refTasks == null
            ? null
            : List<dynamic>.from(refTasks.map((x) => x)),
        "search_params": searchParams == null ? null : searchParams.toJson(),
        "all_stopover_airports": allStopoverAirports == null
            ? null
            : List<dynamic>.from(allStopoverAirports.map((x) => x)),
        "all_airlines": allAirlines == null
            ? null
            : List<dynamic>.from(allAirlines.map((x) => x)),
      };
}

class Datum {
  Datum({
    this.id,
    this.nightsInDest,
    this.duration,
    this.flyFrom,
    this.cityFrom,
    this.cityCodeFrom,
    this.countryFrom,
    this.flyTo,
    this.cityTo,
    this.cityCodeTo,
    this.countryTo,
    this.distance,
    this.routes,
    this.airlines,
    this.pnrCount,
    this.hasAirportChange,
    this.technicalStops,
    this.price,
    this.bagsPrice,
    this.baglimit,
    this.availability,
    this.facilitatedBookingAvailable,
    this.conversion,
    this.quality,
    this.bookingToken,
    this.deepLink,
    this.trackingPixel,
    this.transfers,
    this.typeFlights,
    this.fare,
    this.priceDropdown,
    this.virtualInterlining,
    this.route,
    this.localArrival,
    this.utcArrival,
    this.localDeparture,
    this.utcDeparture,
  });

  final String id;
  final dynamic nightsInDest;
  final Duration duration;
  final String flyFrom;
  final String cityFrom;
  final String cityCodeFrom;
  final Country countryFrom;
  final String flyTo;
  final String cityTo;
  final String cityCodeTo;
  final Country countryTo;
  final double distance;
  final List<dynamic> routes;
  final List<dynamic> airlines;
  final int pnrCount;
  final bool hasAirportChange;
  final int technicalStops;
  final int price;
  final BagsPrice bagsPrice;
  final Baglimit baglimit;
  final Availability availability;
  final bool facilitatedBookingAvailable;
  final Conversion conversion;
  final double quality;
  final String bookingToken;
  final String deepLink;
  final String trackingPixel;
  final List<dynamic> transfers;
  final List<String> typeFlights;
  final Seats fare;
  final PriceDropdown priceDropdown;
  final bool virtualInterlining;
  final List<Route> route;
  final DateTime localArrival;
  final DateTime utcArrival;
  final DateTime localDeparture;
  final DateTime utcDeparture;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"] == null ? null : json["id"],
        nightsInDest: json["nightsInDest"],
        duration: json["duration"] == null
            ? null
            : Duration.fromJson(json["duration"]),
        flyFrom: json["flyFrom"] == null ? null : json["flyFrom"],
        cityFrom: json["cityFrom"] == null ? null : json["cityFrom"],
        cityCodeFrom:
            json["cityCodeFrom"] == null ? null : json["cityCodeFrom"],
        countryFrom: json["countryFrom"] == null
            ? null
            : Country.fromJson(json["countryFrom"]),
        flyTo: json["flyTo"] == null ? null : json["flyTo"],
        cityTo: json["cityTo"] == null ? null : json["cityTo"],
        cityCodeTo: json["cityCodeTo"] == null ? null : json["cityCodeTo"],
        countryTo: json["countryTo"] == null
            ? null
            : Country.fromJson(json["countryTo"]),
        distance: json["distance"] == null ? null : json["distance"].toDouble(),
        routes: json["routes"] == null
            ? null
            : List<dynamic>.from(json["routes"].map((x) => x)),
        airlines: json["airlines"] == null
            ? null
            : List<dynamic>.from(json["airlines"].map((x) => x)),
        pnrCount: json["pnr_count"] == null ? null : json["pnr_count"],
        hasAirportChange: json["has_airport_change"] == null
            ? null
            : json["has_airport_change"],
        technicalStops:
            json["technical_stops"] == null ? null : json["technical_stops"],
        price: json["price"] == null ? null : json["price"],
        bagsPrice: json["bags_price"] == null
            ? null
            : BagsPrice.fromJson(json["bags_price"]),
        baglimit: json["baglimit"] == null
            ? null
            : Baglimit.fromJson(json["baglimit"]),
        availability: json["availability"] == null
            ? null
            : Availability.fromJson(json["availability"]),
        facilitatedBookingAvailable:
            json["facilitated_booking_available"] == null
                ? null
                : json["facilitated_booking_available"],
        conversion: json["conversion"] == null
            ? null
            : Conversion.fromJson(json["conversion"]),
        quality: json["quality"] == null ? null : json["quality"].toDouble(),
        bookingToken:
            json["booking_token"] == null ? null : json["booking_token"],
        deepLink: json["deep_link"] == null ? null : json["deep_link"],
        trackingPixel:
            json["tracking_pixel"] == null ? null : json["tracking_pixel"],
        transfers: json["transfers"] == null
            ? null
            : List<dynamic>.from(json["transfers"].map((x) => x)),
        typeFlights: json["type_flights"] == null
            ? null
            : List<String>.from(json["type_flights"].map((x) => x)),
        fare: json["fare"] == null ? null : Seats.fromJson(json["fare"]),
        priceDropdown: json["price_dropdown"] == null
            ? null
            : PriceDropdown.fromJson(json["price_dropdown"]),
        virtualInterlining: json["virtual_interlining"] == null
            ? null
            : json["virtual_interlining"],
        route: json["route"] == null
            ? null
            : List<Route>.from(json["route"].map((x) => Route.fromJson(x))),
        localArrival: json["local_arrival"] == null
            ? null
            : DateTime.parse(json["local_arrival"]),
        utcArrival: json["utc_arrival"] == null
            ? null
            : DateTime.parse(json["utc_arrival"]),
        localDeparture: json["local_departure"] == null
            ? null
            : DateTime.parse(json["local_departure"]),
        utcDeparture: json["utc_departure"] == null
            ? null
            : DateTime.parse(json["utc_departure"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "nightsInDest": nightsInDest,
        "duration": duration == null ? null : duration.toJson(),
        "flyFrom": flyFrom == null ? null : flyFrom,
        "cityFrom": cityFrom == null ? null : cityFrom,
        "cityCodeFrom": cityCodeFrom == null ? null : cityCodeFrom,
        "countryFrom": countryFrom == null ? null : countryFrom.toJson(),
        "flyTo": flyTo == null ? null : flyTo,
        "cityTo": cityTo == null ? null : cityTo,
        "cityCodeTo": cityCodeTo == null ? null : cityCodeTo,
        "countryTo": countryTo == null ? null : countryTo.toJson(),
        "distance": distance == null ? null : distance,
        "routes":
            routes == null ? null : List<dynamic>.from(routes.map((x) => x)),
        "airlines": airlines == null
            ? null
            : List<dynamic>.from(airlines.map((x) => x)),
        "pnr_count": pnrCount == null ? null : pnrCount,
        "has_airport_change":
            hasAirportChange == null ? null : hasAirportChange,
        "technical_stops": technicalStops == null ? null : technicalStops,
        "price": price == null ? null : price,
        "bags_price": bagsPrice == null ? null : bagsPrice.toJson(),
        "baglimit": baglimit == null ? null : baglimit.toJson(),
        "availability": availability == null ? null : availability.toJson(),
        "facilitated_booking_available": facilitatedBookingAvailable == null
            ? null
            : facilitatedBookingAvailable,
        "conversion": conversion == null ? null : conversion.toJson(),
        "quality": quality == null ? null : quality,
        "booking_token": bookingToken == null ? null : bookingToken,
        "deep_link": deepLink == null ? null : deepLink,
        "tracking_pixel": trackingPixel == null ? null : trackingPixel,
        "transfers": transfers == null
            ? null
            : List<dynamic>.from(transfers.map((x) => x)),
        "type_flights": typeFlights == null
            ? null
            : List<dynamic>.from(typeFlights.map((x) => x)),
        "fare": fare == null ? null : fare.toJson(),
        "price_dropdown": priceDropdown == null ? null : priceDropdown.toJson(),
        "virtual_interlining":
            virtualInterlining == null ? null : virtualInterlining,
        "route": route == null
            ? null
            : List<dynamic>.from(route.map((x) => x.toJson())),
        "local_arrival":
            localArrival == null ? null : localArrival.toIso8601String(),
        "utc_arrival": utcArrival == null ? null : utcArrival.toIso8601String(),
        "local_departure":
            localDeparture == null ? null : localDeparture.toIso8601String(),
        "utc_departure":
            utcDeparture == null ? null : utcDeparture.toIso8601String(),
      };
}

class Availability {
  Availability({
    this.seats,
  });

  final int seats;

  factory Availability.fromJson(Map<String, dynamic> json) => Availability(
        seats: json["seats"] == null ? null : json["seats"],
      );

  Map<String, dynamic> toJson() => {
        "seats": seats == null ? null : seats,
      };
}

class Baglimit {
  Baglimit({
    this.handWidth,
    this.handHeight,
    this.handLength,
    this.handWeight,
    this.holdWidth,
    this.holdHeight,
    this.holdLength,
    this.holdDimensionsSum,
    this.holdWeight,
  });

  final int handWidth;
  final int handHeight;
  final int handLength;
  final int handWeight;
  final int holdWidth;
  final int holdHeight;
  final int holdLength;
  final int holdDimensionsSum;
  final int holdWeight;

  factory Baglimit.fromJson(Map<String, dynamic> json) => Baglimit(
        handWidth: json["hand_width"] == null ? null : json["hand_width"],
        handHeight: json["hand_height"] == null ? null : json["hand_height"],
        handLength: json["hand_length"] == null ? null : json["hand_length"],
        handWeight: json["hand_weight"] == null ? null : json["hand_weight"],
        holdWidth: json["hold_width"] == null ? null : json["hold_width"],
        holdHeight: json["hold_height"] == null ? null : json["hold_height"],
        holdLength: json["hold_length"] == null ? null : json["hold_length"],
        holdDimensionsSum: json["hold_dimensions_sum"] == null
            ? null
            : json["hold_dimensions_sum"],
        holdWeight: json["hold_weight"] == null ? null : json["hold_weight"],
      );

  Map<String, dynamic> toJson() => {
        "hand_width": handWidth == null ? null : handWidth,
        "hand_height": handHeight == null ? null : handHeight,
        "hand_length": handLength == null ? null : handLength,
        "hand_weight": handWeight == null ? null : handWeight,
        "hold_width": holdWidth == null ? null : holdWidth,
        "hold_height": holdHeight == null ? null : holdHeight,
        "hold_length": holdLength == null ? null : holdLength,
        "hold_dimensions_sum":
            holdDimensionsSum == null ? null : holdDimensionsSum,
        "hold_weight": holdWeight == null ? null : holdWeight,
      };
}

class BagsPrice {
  BagsPrice({
    this.the1,
  });

  final double the1;

  factory BagsPrice.fromJson(Map<String, dynamic> json) => BagsPrice(
        the1: json["1"] == null ? null : json["1"].toDouble(),
      );

  Map<String, dynamic> toJson() => {
        "1": the1 == null ? null : the1,
      };
}

class Conversion {
  Conversion({
    this.eur,
  });

  final int eur;

  factory Conversion.fromJson(Map<String, dynamic> json) => Conversion(
        eur: json["EUR"] == null ? null : json["EUR"],
      );

  Map<String, dynamic> toJson() => {
        "EUR": eur == null ? null : eur,
      };
}

class Country {
  Country({
    this.code,
    this.name,
  });

  final String code;
  final String name;

  factory Country.fromJson(Map<String, dynamic> json) => Country(
        code: json["code"] == null ? null : json["code"],
        name: json["name"] == null ? null : json["name"],
      );

  Map<String, dynamic> toJson() => {
        "code": code == null ? null : code,
        "name": name == null ? null : name,
      };
}

class Duration {
  Duration({
    this.departure,
    this.durationReturn,
    this.total,
  });

  final int departure;
  final int durationReturn;
  final int total;

  factory Duration.fromJson(Map<String, dynamic> json) => Duration(
        departure: json["departure"] == null ? null : json["departure"],
        durationReturn: json["return"] == null ? null : json["return"],
        total: json["total"] == null ? null : json["total"],
      );

  Map<String, dynamic> toJson() => {
        "departure": departure == null ? null : departure,
        "return": durationReturn == null ? null : durationReturn,
        "total": total == null ? null : total,
      };
}

class Seats {
  Seats({
    this.adults,
    this.children,
    this.infants,
    this.passengers,
  });

  final double adults;
  final double children;
  final double infants;
  final int passengers;

  factory Seats.fromJson(Map<String, dynamic> json) => Seats(
        adults: json["adults"] == null ? null : json["adults"].toDouble(),
        children: json["children"] == null ? null : json["children"].toDouble(),
        infants: json["infants"] == null ? null : json["infants"].toDouble(),
        passengers: json["passengers"] == null ? null : json["passengers"],
      );

  Map<String, dynamic> toJson() => {
        "adults": adults == null ? null : adults,
        "children": children == null ? null : children,
        "infants": infants == null ? null : infants,
        "passengers": passengers == null ? null : passengers,
      };
}

class PriceDropdown {
  PriceDropdown({
    this.baseFare,
    this.fees,
  });

  final double baseFare;
  final double fees;

  factory PriceDropdown.fromJson(Map<String, dynamic> json) => PriceDropdown(
        baseFare: json["base_fare"] == null ? null : json["base_fare"],
        fees: json["fees"] == null ? null : json["fees"],
      );

  Map<String, dynamic> toJson() => {
        "base_fare": baseFare == null ? null : baseFare,
        "fees": fees == null ? null : fees,
      };
}

class Route {
  Route({
    this.fareBasis,
    this.fareCategory,
    this.fareClasses,
    this.fareFamily,
    this.lastSeen,
    this.refreshTimestamp,
    this.routeReturn,
    this.bagsRecheckRequired,
    this.guarantee,
    this.id,
    this.combinationId,
    this.cityTo,
    this.cityFrom,
    this.cityCodeFrom,
    this.cityCodeTo,
    this.flyTo,
    this.flyFrom,
    this.airline,
    this.operatingCarrier,
    this.equipment,
    this.flightNo,
    this.vehicleType,
    this.operatingFlightNo,
    this.localArrival,
    this.utcArrival,
    this.localDeparture,
    this.utcDeparture,
  });

  final String fareBasis;
  final String fareCategory;
  final String fareClasses;
  final String fareFamily;
  final DateTime lastSeen;
  final DateTime refreshTimestamp;
  final int routeReturn;
  final bool bagsRecheckRequired;
  final bool guarantee;
  final String id;
  final String combinationId;
  final String cityTo;
  final String cityFrom;
  final String cityCodeFrom;
  final String cityCodeTo;
  final String flyTo;
  final String flyFrom;
  final String airline;
  final String operatingCarrier;
  final String equipment;
  final int flightNo;
  final String vehicleType;
  final String operatingFlightNo;
  final DateTime localArrival;
  final DateTime utcArrival;
  final DateTime localDeparture;
  final DateTime utcDeparture;

  factory Route.fromJson(Map<String, dynamic> json) => Route(
        fareBasis: json["fare_basis"] == null ? null : json["fare_basis"],
        fareCategory:
            json["fare_category"] == null ? null : json["fare_category"],
        fareClasses: json["fare_classes"] == null ? null : json["fare_classes"],
        fareFamily: json["fare_family"] == null ? null : json["fare_family"],
        lastSeen: json["last_seen"] == null
            ? null
            : DateTime.parse(json["last_seen"]),
        refreshTimestamp: json["refresh_timestamp"] == null
            ? null
            : DateTime.parse(json["refresh_timestamp"]),
        routeReturn: json["return"] == null ? null : json["return"],
        bagsRecheckRequired: json["bags_recheck_required"] == null
            ? null
            : json["bags_recheck_required"],
        guarantee: json["guarantee"] == null ? null : json["guarantee"],
        id: json["id"] == null ? null : json["id"],
        combinationId:
            json["combination_id"] == null ? null : json["combination_id"],
        cityTo: json["cityTo"] == null ? null : json["cityTo"],
        cityFrom: json["cityFrom"] == null ? null : json["cityFrom"],
        cityCodeFrom:
            json["cityCodeFrom"] == null ? null : json["cityCodeFrom"],
        cityCodeTo: json["cityCodeTo"] == null ? null : json["cityCodeTo"],
        flyTo: json["flyTo"] == null ? null : json["flyTo"],
        flyFrom: json["flyFrom"] == null ? null : json["flyFrom"],
        airline: json["airline"] == null ? null : json["airline"],
        operatingCarrier: json["operating_carrier"] == null
            ? null
            : json["operating_carrier"],
        equipment: json["equipment"] == null ? null : json["equipment"],
        flightNo: json["flight_no"] == null ? null : json["flight_no"],
        vehicleType: json["vehicle_type"] == null ? null : json["vehicle_type"],
        operatingFlightNo: json["operating_flight_no"] == null
            ? null
            : json["operating_flight_no"],
        localArrival: json["local_arrival"] == null
            ? null
            : DateTime.parse(json["local_arrival"]),
        utcArrival: json["utc_arrival"] == null
            ? null
            : DateTime.parse(json["utc_arrival"]),
        localDeparture: json["local_departure"] == null
            ? null
            : DateTime.parse(json["local_departure"]),
        utcDeparture: json["utc_departure"] == null
            ? null
            : DateTime.parse(json["utc_departure"]),
      );

  Map<String, dynamic> toJson() => {
        "fare_basis": fareBasis == null ? null : fareBasis,
        "fare_category": fareCategory == null ? null : fareCategory,
        "fare_classes": fareClasses == null ? null : fareClasses,
        "fare_family": fareFamily == null ? null : fareFamily,
        "last_seen": lastSeen == null ? null : lastSeen.toIso8601String(),
        "refresh_timestamp": refreshTimestamp == null
            ? null
            : refreshTimestamp.toIso8601String(),
        "return": routeReturn == null ? null : routeReturn,
        "bags_recheck_required":
            bagsRecheckRequired == null ? null : bagsRecheckRequired,
        "guarantee": guarantee == null ? null : guarantee,
        "id": id == null ? null : id,
        "combination_id": combinationId == null ? null : combinationId,
        "cityTo": cityTo == null ? null : cityTo,
        "cityFrom": cityFrom == null ? null : cityFrom,
        "cityCodeFrom": cityCodeFrom == null ? null : cityCodeFrom,
        "cityCodeTo": cityCodeTo == null ? null : cityCodeTo,
        "flyTo": flyTo == null ? null : flyTo,
        "flyFrom": flyFrom == null ? null : flyFrom,
        "airline": airline == null ? null : airline,
        "operating_carrier": operatingCarrier == null ? null : operatingCarrier,
        "equipment": equipment == null ? null : equipment,
        "flight_no": flightNo == null ? null : flightNo,
        "vehicle_type": vehicleType == null ? null : vehicleType,
        "operating_flight_no":
            operatingFlightNo == null ? null : operatingFlightNo,
        "local_arrival":
            localArrival == null ? null : localArrival.toIso8601String(),
        "utc_arrival": utcArrival == null ? null : utcArrival.toIso8601String(),
        "local_departure":
            localDeparture == null ? null : localDeparture.toIso8601String(),
        "utc_departure":
            utcDeparture == null ? null : utcDeparture.toIso8601String(),
      };
}

class SearchParams {
  SearchParams({
    this.flyFromType,
    this.toType,
    this.seats,
  });

  final String flyFromType;
  final String toType;
  final Seats seats;

  factory SearchParams.fromJson(Map<String, dynamic> json) => SearchParams(
        flyFromType: json["flyFrom_type"] == null ? null : json["flyFrom_type"],
        toType: json["to_type"] == null ? null : json["to_type"],
        seats: json["seats"] == null ? null : Seats.fromJson(json["seats"]),
      );

  Map<String, dynamic> toJson() => {
        "flyFrom_type": flyFromType == null ? null : flyFromType,
        "to_type": toType == null ? null : toType,
        "seats": seats == null ? null : seats.toJson(),
      };
}
