// To parse this JSON data, do
//
//     final nearByAirportsResponse = nearByAirportsResponseFromJson(jsonString);

import 'dart:convert';

NearByAirportsResponse nearByAirportsResponseFromJson(String str) => NearByAirportsResponse.fromJson(json.decode(str));

String nearByAirportsResponseToJson(NearByAirportsResponse data) => json.encode(data.toJson());

class NearByAirportsResponse {
  NearByAirportsResponse({
    this.locations,
    this.meta,
    this.lastRefresh,
    this.resultsRetrieved,
  });

  final List<LocationElement> locations;
  final Meta meta;
  final int lastRefresh;
  final int resultsRetrieved;

  factory NearByAirportsResponse.fromJson(Map<String, dynamic> json) => NearByAirportsResponse(
    locations: json["locations"] == null ? null : List<LocationElement>.from(json["locations"].map((x) => LocationElement.fromJson(x))),
    meta: json["meta"] == null ? null : Meta.fromJson(json["meta"]),
    lastRefresh: json["last_refresh"] == null ? null : json["last_refresh"],
    resultsRetrieved: json["results_retrieved"] == null ? null : json["results_retrieved"],
  );

  Map<String, dynamic> toJson() => {
    "locations": locations == null ? null : List<dynamic>.from(locations.map((x) => x.toJson())),
    "meta": meta == null ? null : meta.toJson(),
    "last_refresh": lastRefresh == null ? null : lastRefresh,
    "results_retrieved": resultsRetrieved == null ? null : resultsRetrieved,
  };
}

class LocationElement {
  LocationElement({
    this.id,
    this.intId,
    this.airportIntId,
    this.active,
    this.code,
    this.icao,
    this.name,
    this.slug,
    this.slugEn,
    this.alternativeNames,
    this.rank,
    this.globalRankDst,
    this.dstPopularityScore,
    this.timezone,
    this.city,
    this.location,
    this.alternativeDeparturePoints,
    this.tags,
    this.providers,
    this.special,
    this.touristRegion,
    this.carRentals,
    this.newGround,
    this.routingPriority,
    this.type,
  });

  final String id;
  final int intId;
  final int airportIntId;
  final bool active;
  final String code;
  final String icao;
  final String name;
  final String slug;
  final String slugEn;
  final List<dynamic> alternativeNames;
  final int rank;
  final int globalRankDst;
  final double dstPopularityScore;
  final String timezone;
  final City city;
  final LocationLocation location;
  final List<AlternativeDeparturePoint> alternativeDeparturePoints;
  final List<Tag> tags;
  final List<int> providers;
  final List<Special> special;
  final List<Special> touristRegion;
  final List<CarRental> carRentals;
  final bool newGround;
  final int routingPriority;
  final String type;

  factory LocationElement.fromJson(Map<String, dynamic> json) => LocationElement(
    id: json["id"] == null ? null : json["id"],
    intId: json["int_id"] == null ? null : json["int_id"],
    airportIntId: json["airport_int_id"] == null ? null : json["airport_int_id"],
    active: json["active"] == null ? null : json["active"],
    code: json["code"] == null ? null : json["code"],
    icao: json["icao"] == null ? null : json["icao"],
    name: json["name"] == null ? null : json["name"],
    slug: json["slug"] == null ? null : json["slug"],
    slugEn: json["slug_en"] == null ? null : json["slug_en"],
    alternativeNames: json["alternative_names"] == null ? null : List<dynamic>.from(json["alternative_names"].map((x) => x)),
    rank: json["rank"] == null ? null : json["rank"],
    globalRankDst: json["global_rank_dst"] == null ? null : json["global_rank_dst"],
    dstPopularityScore: json["dst_popularity_score"] == null ? null : json["dst_popularity_score"],
    timezone: json["timezone"] == null ? null : json["timezone"],
    city: json["city"] == null ? null : City.fromJson(json["city"]),
    location: json["location"] == null ? null : LocationLocation.fromJson(json["location"]),
    alternativeDeparturePoints: json["alternative_departure_points"] == null ? null : List<AlternativeDeparturePoint>.from(json["alternative_departure_points"].map((x) => AlternativeDeparturePoint.fromJson(x))),
    tags: json["tags"] == null ? null : List<Tag>.from(json["tags"].map((x) => Tag.fromJson(x))),
    providers: json["providers"] == null ? null : List<int>.from(json["providers"].map((x) => x)),
    special: json["special"] == null ? null : List<Special>.from(json["special"].map((x) => Special.fromJson(x))),
    touristRegion: json["tourist_region"] == null ? null : List<Special>.from(json["tourist_region"].map((x) => Special.fromJson(x))),
    carRentals: json["car_rentals"] == null ? null : List<CarRental>.from(json["car_rentals"].map((x) => CarRental.fromJson(x))),
    newGround: json["new_ground"] == null ? null : json["new_ground"],
    routingPriority: json["routing_priority"] == null ? null : json["routing_priority"],
    type: json["type"] == null ? null : json["type"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "int_id": intId == null ? null : intId,
    "airport_int_id": airportIntId == null ? null : airportIntId,
    "active": active == null ? null : active,
    "code": code == null ? null : code,
    "icao": icao == null ? null : icao,
    "name": name == null ? null : name,
    "slug": slug == null ? null : slug,
    "slug_en": slugEn == null ? null : slugEn,
    "alternative_names": alternativeNames == null ? null : List<dynamic>.from(alternativeNames.map((x) => x)),
    "rank": rank == null ? null : rank,
    "global_rank_dst": globalRankDst == null ? null : globalRankDst,
    "dst_popularity_score": dstPopularityScore == null ? null : dstPopularityScore,
    "timezone": timezone == null ? null : timezone,
    "city": city == null ? null : city.toJson(),
    "location": location == null ? null : location.toJson(),
    "alternative_departure_points": alternativeDeparturePoints == null ? null : List<dynamic>.from(alternativeDeparturePoints.map((x) => x.toJson())),
    "tags": tags == null ? null : List<dynamic>.from(tags.map((x) => x.toJson())),
    "providers": providers == null ? null : List<dynamic>.from(providers.map((x) => x)),
    "special": special == null ? null : List<dynamic>.from(special.map((x) => x.toJson())),
    "tourist_region": touristRegion == null ? null : List<dynamic>.from(touristRegion.map((x) => x.toJson())),
    "car_rentals": carRentals == null ? null : List<dynamic>.from(carRentals.map((x) => x.toJson())),
    "new_ground": newGround == null ? null : newGround,
    "routing_priority": routingPriority == null ? null : routingPriority,
    "type": type == null ? null : type,
  };
}

class AlternativeDeparturePoint {
  AlternativeDeparturePoint({
    this.id,
    this.distance,
    this.duration,
  });

  final String id;
  final double distance;
  final double duration;

  factory AlternativeDeparturePoint.fromJson(Map<String, dynamic> json) => AlternativeDeparturePoint(
    id: json["id"] == null ? null : json["id"],
    distance: json["distance"] == null ? null : json["distance"].toDouble(),
    duration: json["duration"] == null ? null : json["duration"].toDouble(),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "distance": distance == null ? null : distance,
    "duration": duration == null ? null : duration,
  };
}

class CarRental {
  CarRental({
    this.providerId,
    this.providersLocations,
  });

  final int providerId;
  final List<String> providersLocations;

  factory CarRental.fromJson(Map<String, dynamic> json) => CarRental(
    providerId: json["provider_id"] == null ? null : json["provider_id"],
    providersLocations: json["providers_locations"] == null ? null : List<String>.from(json["providers_locations"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "provider_id": providerId == null ? null : providerId,
    "providers_locations": providersLocations == null ? null : List<dynamic>.from(providersLocations.map((x) => x)),
  };
}

class City {
  City({
    this.id,
    this.name,
    this.code,
    this.slug,
    this.autonomousTerritory,
    this.country,
    this.subdivision,
    this.continent,
    this.region,
  });

  final String id;
  final String name;
  final String code;
  final String slug;
  final dynamic autonomousTerritory;
  final Special country;
  final dynamic subdivision;
  final Special continent;
  final Special region;

  factory City.fromJson(Map<String, dynamic> json) => City(
    id: json["id"] == null ? null : json["id"],
    name: json["name"] == null ? null : json["name"],
    code: json["code"] == null ? null : json["code"],
    slug: json["slug"] == null ? null : json["slug"],
    autonomousTerritory: json["autonomous_territory"],
    country: json["country"] == null ? null : Special.fromJson(json["country"]),
    subdivision: json["subdivision"],
    continent: json["continent"] == null ? null : Special.fromJson(json["continent"]),
    region: json["region"] == null ? null : Special.fromJson(json["region"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "name": name == null ? null : name,
    "code": code == null ? null : code,
    "slug": slug == null ? null : slug,
    "autonomous_territory": autonomousTerritory,
    "country": country == null ? null : country.toJson(),
    "subdivision": subdivision,
    "continent": continent == null ? null : continent.toJson(),
    "region": region == null ? null : region.toJson(),
  };
}

class Special {
  Special({
    this.id,
    this.name,
    this.slug,
    this.code,
  });

  final String id;
  final String name;
  final String slug;
  final Code code;

  factory Special.fromJson(Map<String, dynamic> json) => Special(
    id: json["id"] == null ? null : json["id"],
    name: json["name"] == null ? null : json["name"],
    slug: json["slug"] == null ? null : json["slug"],
    code: json["code"] == null ? null : codeValues.map[json["code"]],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "name": name == null ? null : name,
    "slug": slug == null ? null : slug,
    "code": code == null ? null : codeValues.reverse[code],
  };
}

enum Code { AS, IN }

final codeValues = EnumValues({
  "AS": Code.AS,
  "IN": Code.IN
});

class LocationLocation {
  LocationLocation({
    this.lat,
    this.lon,
  });

  final double lat;
  final double lon;

  factory LocationLocation.fromJson(Map<String, dynamic> json) => LocationLocation(
    lat: json["lat"] == null ? null : json["lat"].toDouble(),
    lon: json["lon"] == null ? null : json["lon"].toDouble(),
  );

  Map<String, dynamic> toJson() => {
    "lat": lat == null ? null : lat,
    "lon": lon == null ? null : lon,
  };
}

class Tag {
  Tag({
    this.tag,
    this.monthTo,
    this.monthFrom,
  });

  final String tag;
  final int monthTo;
  final int monthFrom;

  factory Tag.fromJson(Map<String, dynamic> json) => Tag(
    tag: json["tag"] == null ? null : json["tag"],
    monthTo: json["month_to"] == null ? null : json["month_to"],
    monthFrom: json["month_from"] == null ? null : json["month_from"],
  );

  Map<String, dynamic> toJson() => {
    "tag": tag == null ? null : tag,
    "month_to": monthTo == null ? null : monthTo,
    "month_from": monthFrom == null ? null : monthFrom,
  };
}

class Meta {
  Meta({
    this.locale,
  });

  final Locale locale;

  factory Meta.fromJson(Map<String, dynamic> json) => Meta(
    locale: json["locale"] == null ? null : Locale.fromJson(json["locale"]),
  );

  Map<String, dynamic> toJson() => {
    "locale": locale == null ? null : locale.toJson(),
  };
}

class Locale {
  Locale({
    this.code,
    this.status,
  });

  final String code;
  final String status;

  factory Locale.fromJson(Map<String, dynamic> json) => Locale(
    code: json["code"] == null ? null : json["code"],
    status: json["status"] == null ? null : json["status"],
  );

  Map<String, dynamic> toJson() => {
    "code": code == null ? null : code,
    "status": status == null ? null : status,
  };
}

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
