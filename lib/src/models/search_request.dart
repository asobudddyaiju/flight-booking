// To parse this JSON data, do
//
//     final searchRequest = searchRequestFromJson(jsonString);

import 'dart:convert';

SearchRequest searchRequestFromJson(String str) =>
    SearchRequest.fromJson(json.decode(str));

String searchRequestToJson(SearchRequest data) => json.encode(data.toJson());

class SearchRequest {
  SearchRequest({
    this.flyFrom,
    this.flyTo,
    this.dateFrom,
    this.dateTo,
    this.returnFrom,
    this.returnTo,
    this.maxFlyDuration,
    this.flightType,
    this.oneForCity,
    this.onePerDate,
    this.adults,
    this.children,
    this.infants,
    this.selectedCabins,
    this.curr,
    this.locale,
    this.priceFrom,
    this.priceTo,
    this.dtimeFrom,
    this.dtimeTo,
    this.atimeFrom,
    this.atimeTo,
    this.retDtimeFrom,
    this.retDtimeTo,
    this.retAtimeFrom,
    this.retAtimeTo,
    this.stopoverFrom,
    this.stopoverTo,
    this.maxStopovers,
    this.selectAirlines,
    this.selectAirlinesExclude,
    this.sort,
  });

  final String flyFrom;
  final String flyTo;
  final String dateFrom;
  final String dateTo;
  final String returnFrom;
  final String returnTo;
  final int maxFlyDuration;
  final String flightType;
  final int oneForCity;
  final int onePerDate;
  final int adults;
  final int children;
  final int infants;
  final String selectedCabins;
  final String curr;
  final String locale;
  final int priceFrom;
  final int priceTo;
  final String dtimeFrom;
  final String dtimeTo;
  final String atimeFrom;
  final String atimeTo;
  final String retDtimeFrom;
  final String retDtimeTo;
  final String retAtimeFrom;
  final String retAtimeTo;
  final String stopoverFrom;
  final String stopoverTo;
  final int maxStopovers;
  final String selectAirlines;
  final bool selectAirlinesExclude;
  final String sort;

  factory SearchRequest.fromJson(Map<String, dynamic> json) => SearchRequest(
        flyFrom: json["fly_from"] == null ? null : json["fly_from"],
        flyTo: json["fly_to"] == null ? null : json["fly_to"],
        dateFrom: json["date_from"] == null ? null : json["date_from"],
        dateTo: json["date_to"] == null ? null : json["date_to"],
        returnFrom: json["return_from"] == null ? null : json["return_from"],
        returnTo: json["return_to"] == null ? null : json["return_to"],
        maxFlyDuration:
            json["max_fly_duration"] == null ? null : json["max_fly_duration"],
        flightType: json["flight_type"] == null ? null : json["flight_type"],
        oneForCity: json["one_for_city"] == null ? null : json["one_for_city"],
        onePerDate: json["one_per_date"] == null ? null : json["one_per_date"],
        adults: json["adults"] == null ? null : json["adults"],
        children: json["children"] == null ? null : json["children"],
        infants: json["infants"] == null ? null : json["infants"],
        selectedCabins:
            json["selected_cabins"] == null ? null : json["selected_cabins"],
        curr: json["curr"] == null ? null : json["curr"],
        locale: json["locale"] == null ? null : json["locale"],
        priceFrom: json["price_from"] == null ? null : json["price_from"],
        priceTo: json["price_to"] == null ? null : json["price_to"],
        dtimeFrom: json["dtime_from"] == null ? null : json["dtime_from"],
        dtimeTo: json["dtime_to"] == null ? null : json["dtime_to"],
        atimeFrom: json["atime_from"] == null ? null : json["atime_from"],
        atimeTo: json["atime_to"] == null ? null : json["atime_to"],
        retDtimeFrom:
            json["ret_dtime_from"] == null ? null : json["ret_dtime_from"],
        retDtimeTo: json["ret_dtime_to"] == null ? null : json["ret_dtime_to"],
        retAtimeFrom:
            json["ret_atime_from"] == null ? null : json["ret_atime_from"],
        retAtimeTo: json["ret_atime_to"] == null ? null : json["ret_atime_to"],
        stopoverFrom:
            json["stopover_from"] == null ? null : json["stopover_from"],
        stopoverTo: json["stopover_to"] == null ? null : json["stopover_to"],
        maxStopovers:
            json["max_stopovers"] == null ? null : json["max_stopovers"],
        selectAirlines:
            json["select_airlines"] == null ? null : json["select_airlines"],
        selectAirlinesExclude: json["select_airlines_exclude"] == null
            ? null
            : json["select_airlines_exclude"],
        sort: json["sort"] == null ? null : json["sort"],
      );

  Map<String, dynamic> toJson() => {
        "fly_from": flyFrom == null ? null : flyFrom,
        "fly_to": flyTo == null ? null : flyTo,
        "date_from": dateFrom == null ? null : dateFrom,
        "date_to": dateTo == null ? null : dateTo,
        "return_from": returnFrom == null ? null : returnFrom,
        "return_to": returnTo == null ? null : returnTo,
        "max_fly_duration": maxFlyDuration == null ? null : maxFlyDuration,
        "flight_type": flightType == null ? null : flightType,
        "one_for_city": oneForCity == null ? null : oneForCity,
        "one_per_date": onePerDate == null ? null : onePerDate,
        "adults": adults == null ? null : adults,
        "children": children == null ? null : children,
        "infants": infants == null ? null : infants,
        "selected_cabins": selectedCabins == null ? null : selectedCabins,
        "curr": curr == null ? null : curr,
        "locale": locale == null ? null : locale,
        "price_from": priceFrom == null ? null : priceFrom,
        "price_to": priceTo == null ? null : priceTo,
        "dtime_from": dtimeFrom == null ? null : dtimeFrom,
        "dtime_to": dtimeTo == null ? null : dtimeTo,
        "atime_from": atimeFrom == null ? null : atimeFrom,
        "atime_to": atimeTo == null ? null : atimeTo,
        "ret_dtime_from": retDtimeFrom == null ? null : retDtimeFrom,
        "ret_dtime_to": retDtimeTo == null ? null : retDtimeTo,
        "ret_atime_from": retAtimeFrom == null ? null : retAtimeFrom,
        "ret_atime_to": retAtimeTo == null ? null : retAtimeTo,
        "stopover_from": stopoverFrom == null ? null : stopoverFrom,
        "stopover_to": stopoverTo == null ? null : stopoverTo,
        "max_stopovers": maxStopovers == null ? null : maxStopovers,
        "select_airlines": selectAirlines == null ? null : selectAirlines,
        "select_airlines_exclude":
            selectAirlinesExclude == null ? null : selectAirlinesExclude,
        "sort": sort == null ? null : sort,
      };
}
