import 'package:hive/hive.dart';

import '../src/utils/constants.dart';

class AppHive {
  static const String _TOKEN = "token";
  static const String _CUSTOMERNAME = 'custname';
  static const String _CURRENCY = 'currency';
  static const String _SORTBY = 'sortby';
  static const String _STOPNUMBERS = 'stopnumbers';

  static const String _DEPARTURERANGESTARTS = 'departrangestartkey';
  static const String _RETURNRANGEENDS = 'returnrangeendkey';
  static const String _DEPARTURERANGELOWERTHANRETURNRANGE =
      'departurerangelowerthanreturnrange';
  static const String _DEPARTUREFLIGHTTIMEFROM = 'departureflighttimefrom';
  static const String _DEPARTUREFLIGHTTIMETO = 'departureflighttimeto';
  static const String _RETURNFLIGHTTIMEFROM = 'returnflighttimefrom';
  static const String _RETURNFLIGHTTIMETO = 'returnflighttimeto';
  static const String _DEPARTUREFLIGHTTIMEFROM1 = 'departureflighttimefrom1';
  static const String _DEPARTUREFLIGHTTIMEFROM2 = 'departureflighttimefrom2';
  static const String _DEPARTUREFLIGHTTIMEFROM3 = 'departureflighttimefrom3';
  static const String _DEPARTUREFLIGHTTIMEFROM4 = 'departureflighttimefrom4';
  static const String _RETURNFLIGHTTIMEFROM1 = 'returnflighttimefrom1';
  static const String _RETURNFLIGHTTIMEFROM2 = 'returnflighttimefrom2';
  static const String _RETURNFLIGHTTIMEFROM3 = 'returnflighttimefrom3';
  static const String _RETURNFLIGHTTIMEFROM4 = 'returnflighttimefrom4';
  static const String _AIRLINESFILTERSEARCH = 'airlinesfiltersearch';
  static const String _SELECTALLBUTTONSTATUS = 'selectallbuttonstatus';
  static const String _SELECTEDPRICERANGELOWER = 'selectedpricerangelower';
  static const String _SELECTEDPRICERANGEUPPER = 'selectedpricerangeupper';
  static const String _PRICETOTALARRAYMIN = 'pricetotalarraymin';
  static const String _PRICETOTALARRAYMAX = 'pricetotalarraymax';
  static const String _PRICE_STARTS_AT = "pricestartsat";
  static const String _PRICE_ENDS_AT = "priceendssat";
  static const String _DEPARTURE_FROM_TIME = 'departurefromtime';
  static const String _DEPARTURE_TO_TIME = 'departuretotime';
  static const String _RETURN_FROM_TIME = 'returnfromtime';
  static const String _RETURN_TO_TIME = 'returntotime';
  static const String _NEARBYAIRPORTFIRST = 'nearbyairportfirst';
  static const String _LATITUDE = 'latitude';
  static const String _LONGITUDE = 'longitude';
  static const String _ONE_WAY_DATE = 'one way date';
  static const String _ONE_WAY_DAY = 'one way day';
  static const String _ONE_WAY_MONTH = 'one way month';
  static const String _ONE_WAY_YEAR = 'one way year';
  static const String _FLIGHT_DURATION = "flightduration";

  void hivePut({String key, String value}) async {
    await Hive.box(Constants.BOX_NAME).put(key, value);
  }

  void hivePutInt({String key, int value}) async {
    await Hive.box(Constants.BOX_NAME).put(key, value);
  }

  void hivePutDouble({String key, double value}) async {
    await Hive.box(Constants.BOX_NAME).put(key, value);
  }

  void hivePutBool({String key, bool value}) async {
    await Hive.box(Constants.BOX_NAME).put(key, value);
  }

  void hivePutIntAt({String key, int value, int index}) async {
    await Hive.box(Constants.BOX_NAME).putAt(index, value);
  }

  String hiveGet({String key}) {
    return Hive.box(Constants.BOX_NAME).get(key);
  }

  int hiveGetInt({String key}) {
    return Hive.box(Constants.BOX_NAME).get(key);
  }

  double hiveGetDouble({String key}) {
    return Hive.box(Constants.BOX_NAME).get(key);
  }

  bool hiveGetBool({String key}) {
    return Hive.box(Constants.BOX_NAME).get(key);
  }

  putToken({String token}) {
    return hivePut(key: _TOKEN, value: token);
  }

  getToken() {
    return hiveGet(key: _TOKEN);
  }

  void deleteToken() {
    putToken(token: "");
  }

  customerNamePut({String token}) {
    return hivePut(key: _CUSTOMERNAME, value: token);
  }

  getCustomerName() {
    return hiveGet(key: _CUSTOMERNAME);
  }

  putCurrency({String value}) {
    return hivePut(key: _CURRENCY, value: value);
  }

  getCurrency() {
    return hiveGet(key: _CURRENCY);
  }

  putSortByForFilter({String value}) {
    return hivePut(key: _SORTBY, value: value);
  }

  getSortByForFilter() {
    return hiveGet(key: _SORTBY);
  }

  putDepartureRangeStart({double value}) {
    return hivePutDouble(key: _DEPARTURERANGESTARTS, value: value);
  }

  putDepartureRangeLowerThanReturnRange({bool value}) {
    return hivePutBool(key: _DEPARTURERANGELOWERTHANRETURNRANGE, value: value);
  }

  getDepartureRangeLowerThanReturnRange() {
    return hiveGetBool(key: _DEPARTURERANGELOWERTHANRETURNRANGE);
  }

  putDepartureFlightTimeFrom({String value}) {
    return hivePut(key: _DEPARTUREFLIGHTTIMEFROM, value: value);
  }

  getDepartureFlightTimeFrom() {
    return hiveGet(key: _DEPARTUREFLIGHTTIMEFROM);
  }

  putDepartureFlightTimeTo({String value}) {
    return hivePut(key: _DEPARTUREFLIGHTTIMETO, value: value);
  }

  getDepartureFlightTimeTo() {
    return hiveGet(key: _DEPARTUREFLIGHTTIMETO);
  }

  putReturnFlightTimeFrom({String value}) {
    return hivePut(key: _RETURNFLIGHTTIMEFROM, value: value);
  }

  getReturnFlightTimeFrom() {
    return hiveGet(key: _RETURNFLIGHTTIMEFROM);
  }

  putReturnFlightTimeTo({String value}) {
    return hivePut(key: _RETURNFLIGHTTIMETO, value: value);
  }

  getReturnFlightTimeTo() {
    return hiveGet(key: _RETURNFLIGHTTIMETO);
  }

  putDepartureFlightTime1({bool value}) {
    return hivePutBool(key: _DEPARTUREFLIGHTTIMEFROM1, value: value);
  }

  getDepartureFlightTime1() {
    return hiveGetBool(key: _DEPARTUREFLIGHTTIMEFROM1);
  }

  putDepartureFlightTime2({bool value}) {
    return hivePutBool(key: _DEPARTUREFLIGHTTIMEFROM2, value: value);
  }

  getDepartureFlightTime2() {
    return hiveGetBool(key: _DEPARTUREFLIGHTTIMEFROM2);
  }

  putDepartureFlightTime3({bool value}) {
    return hivePutBool(key: _DEPARTUREFLIGHTTIMEFROM3, value: value);
  }

  getDepartureFlightTime3() {
    return hiveGetBool(key: _DEPARTUREFLIGHTTIMEFROM3);
  }

  putDepartureFlightTime4({bool value}) {
    return hivePutBool(key: _DEPARTUREFLIGHTTIMEFROM4, value: value);
  }

  getDepartureFlightTime4() {
    return hiveGetBool(key: _DEPARTUREFLIGHTTIMEFROM4);
  }

  putReturnFlightTime1({bool value}) {
    return hivePutBool(key: _RETURNFLIGHTTIMEFROM1, value: value);
  }

  getReturnFlightTime1() {
    return hiveGetBool(key: _RETURNFLIGHTTIMEFROM1);
  }

  putReturnFlightTime2({bool value}) {
    return hivePutBool(key: _RETURNFLIGHTTIMEFROM2, value: value);
  }

  getReturnFlightTime2() {
    return hiveGetBool(key: _RETURNFLIGHTTIMEFROM2);
  }

  putReturnFlightTime3({bool value}) {
    return hivePutBool(key: _RETURNFLIGHTTIMEFROM3, value: value);
  }

  getReturnFlightTime3() {
    return hiveGetBool(key: _RETURNFLIGHTTIMEFROM3);
  }

  putReturnFlightTime4({bool value}) {
    return hivePutBool(key: _RETURNFLIGHTTIMEFROM4, value: value);
  }

  getReturnFlightTime4() {
    return hiveGetBool(key: _RETURNFLIGHTTIMEFROM4);
  }

  putAirlineFilterSearch({String value}) {
    return hivePut(key: _AIRLINESFILTERSEARCH, value: value);
  }

  getAirlineFilterSearch() {
    return hiveGet(key: _AIRLINESFILTERSEARCH);
  }

  putSelectAllButtonStatus({bool value}) {
    return hivePutBool(key: _SELECTALLBUTTONSTATUS, value: value);
  }

  getSelectAllButtonStatus() {
    return hiveGetBool(key: _SELECTALLBUTTONSTATUS);
  }

  putSelectedPriceRangeLowerForFilter({double value}) {
    return hivePutDouble(key: _SELECTEDPRICERANGELOWER, value: value);
  }

  getSelectedPriceRangeLowerForFilter() {
    return hiveGetDouble(key: _SELECTEDPRICERANGELOWER);
  }

  putSelectedPriceRangeUpperForFilter({double value}) {
    return hivePutDouble(key: _SELECTEDPRICERANGEUPPER, value: value);
  }

  getSelectedPriceRangeUpperForFilter() {
    return hiveGetDouble(key: _SELECTEDPRICERANGEUPPER);
  }

  putPriceTotalArrayMin({int value}) {
    return hivePutInt(key: _PRICETOTALARRAYMIN, value: value);
  }

  putReturnRangeEnd({String value}) {
    return hiveGetDouble(key: _RETURNRANGEENDS);
  }

  putDepartureFromTime({String value}) {
    return hivePut(key: _DEPARTURE_FROM_TIME, value: value);
  }

  getDepartureFromTime() {
    return hiveGet(key: _DEPARTURE_FROM_TIME);
  }

  putDepartureToTime({String value}) {
    return hivePut(key: _DEPARTURE_TO_TIME, value: value);
  }

  getDepartureToTime() {
    return hiveGet(key: _DEPARTURE_TO_TIME);
  }

  putReturnToTime({String value}) {
    return hivePut(key: _RETURN_TO_TIME, value: value);
  }

  getReturnToTime() {
    return hiveGet(key: _RETURN_TO_TIME);
  }

  putReturnFromTime({String value}) {
    return hivePut(key: _RETURN_FROM_TIME, value: value);
  }

  getReturnFromTime() {
    return hiveGet(key: _RETURN_FROM_TIME);
  }

  getPriceTotalArrayMin() {
    return hiveGetInt(key: _PRICETOTALARRAYMIN);
  }

  putPriceTotalArrayMax({int value}) {
    return hivePutInt(key: _PRICETOTALARRAYMAX, value: value);
  }

  getPriceTotalArrayMax() {
    return hiveGetInt(key: _PRICETOTALARRAYMAX);
  }

  putNearbyAirport({String value}) {
    return hivePut(key: _NEARBYAIRPORTFIRST, value: value);
  }

  getNearbyAirport() {
    return hiveGet(key: _NEARBYAIRPORTFIRST);
  }

  putLatitude({double value}) {
    return hivePutDouble(key: _LATITUDE, value: value);
  }

  getLatitude() {
    return hiveGetDouble(key: _LATITUDE);
  }

  putLongitude({double value}) {
    return hivePutDouble(key: _LONGITUDE, value: value);
  }

  getLongitude() {
    return hiveGetDouble(key: _LONGITUDE);
  }

  putOneWayDate({String value}) {
    return hivePut(key: _ONE_WAY_DATE, value: value);
  }

  getOneWayDate() {
    return hiveGet(key: _ONE_WAY_DATE);
  }

  putOneWayDay({String value}) {
    return hivePut(key: _ONE_WAY_DAY, value: value);
  }

  getOneWayDay() {
    return hiveGet(key: _ONE_WAY_DAY);
  }

  putOneWayMonth({String value}) {
    return hivePut(key: _ONE_WAY_MONTH, value: value);
  }

  getOneWayMonth() {
    return hiveGet(key: _ONE_WAY_MONTH);
  }

  putOneWayYear({String value}) {
    return hivePut(key: _ONE_WAY_YEAR, value: value);
  }

  getOneWayYear() {
    return hiveGet(key: _ONE_WAY_YEAR);
  }

  putStopsNumbers({String value}) {
    return hivePut(key: _STOPNUMBERS, value: value);
  }

  getStopsNumbers() {
    return hiveGet(key: _STOPNUMBERS);
  }

  putFlightDuration({String value}) {
    return hivePut(key: _FLIGHT_DURATION, value: value);
  }

  getFlightDuration() {
    return hiveGet(key: _FLIGHT_DURATION);
  }

  void hiveClear() {
    Hive.box('box').clear();
  }

  AppHive();
}
