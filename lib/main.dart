import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart' as path_provider;

import 'app/app.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final appDocDir = await path_provider.getApplicationDocumentsDirectory();
  Hive.init(appDocDir.path);
  await Firebase.initializeApp();
  await Hive.openBox('box');
   await Hive.openBox('date');
   await Hive.openBox('calender');
   await Hive.openBox('code');
   await Hive.openBox('adult');
  runApp(MyApp());
}
