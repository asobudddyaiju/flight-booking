import 'package:flightbooking/localization/locale.dart';
import 'package:flightbooking/src/utils/constants.dart';
import 'package:flightbooking/src/utils/util.dart';
import 'package:flutter/material.dart';

class DetailsBottomNavigation extends StatefulWidget {
  final String title, price;
  final Function onTap;
  final bookingButtonLoading;
  DetailsBottomNavigation(
      {this.title, this.price, this.onTap, this.bookingButtonLoading});

  @override
  _DetailsBottomNavigationState createState() =>
      _DetailsBottomNavigationState();
}

class _DetailsBottomNavigationState extends State<DetailsBottomNavigation> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth(context, dividedBy: 1),
      height: screenHeight(context, dividedBy: 7.8),
      color: Constants.kitGradients[38],
      child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  widget.title,
                  style: TextStyle(
                      color: Constants.kitGradients[31],
                      fontFamily: 'ProximaNova',
                      fontSize: 16,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal),
                ),
                Text(
                  widget.price,
                  style: TextStyle(
                      color: Constants.kitGradients[28],
                      fontFamily: 'ProximaNova',
                      fontSize: 24,
                      fontWeight: FontWeight.w700,
                      fontStyle: FontStyle.normal),
                ),
              ],
            ),
            Container(
              width: screenWidth(context, dividedBy: 2.5),
              height: screenHeight(context, dividedBy: 15),
              child: RaisedButton(
                color: Constants.kitGradients[14],
                shape: StadiumBorder(),
                onPressed: widget.onTap,
                child: widget.bookingButtonLoading == false
                    ? Center(
                        child: Text(
                          getTranslated(context, "Book_now:"),
                          style: TextStyle(
                              color: Constants.kitGradients[3],
                              fontFamily: 'ProximaNova',
                              fontSize: 18,
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.normal),
                        ),
                      )
                    : Center(
                        child: CircularProgressIndicator(
                          valueColor: new AlwaysStoppedAnimation<Color>(
                              Constants.kitGradients[3]),
                        ),
                      ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
